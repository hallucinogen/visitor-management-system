package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.CardLevel;
import com.btj.vms.model.CardRoom;
import com.btj.vms.model.Level;
import com.btj.vms.model.Room;

/**
 * Servlet implementation class CardController
 */
@WebServlet("/administration/CardController")
public class CardController extends BaseController{
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CardController() {
        super();
    }

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		request.setAttribute("location", "administration");
		request.setAttribute("option", "card");
		String type	= request.getParameter("type");
		if(type == null) type = "level";
		request.setAttribute("type", type);
		
		if(mAction == null) {
			if(IOUtilities.isAjaxRequest(request)){
				// HACK: we won't be using this for a while
				String query = request.getParameter("q");
				PrintWriter out = response.getWriter();
			}else{
				if(type.equals("room")) {
					CardRoom[] cards = CardRoom.getSortedLimitedModel(0, 0, 0);
					request.setAttribute("cards", cards);
					request.setAttribute("currentPage", 1);
					request.setAttribute("currentMode", 0);
					request.setAttribute("currentColumn", 0);
					request.setAttribute("lastPage", CardLevel.getTotalPage());					
				}
				else{
					CardLevel[] cards = CardLevel.getSortedLimitedModel(0, 0, 0);
					request.setAttribute("cards", cards);
					request.setAttribute("currentPage", 1);
					request.setAttribute("currentMode", 0);
					request.setAttribute("currentColumn", 0);
					request.setAttribute("lastPage", CardLevel.getTotalPage());					
				}

				mRD = request.getRequestDispatcher("card/index.jsp");
				mRD.forward(request, response);
			}
		}
		else if(mAction.equals("update")) {
			if(type.equals("room")) {
				CardRoom card	= CardRoom.getCard(mID);
				if(card == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested card does not exist");
					return;
				}

				request.setAttribute("card", card);
			}
			else {
				CardLevel card	= CardLevel.getCard(mID);
				if(card == null){
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested card does not exist");
					return;
				}

				request.setAttribute("card", card);
			}
			mRD = request.getRequestDispatcher("card/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")) {
			if(type.equals("room")) {
				CardRoom card	= new CardRoom();
				request.setAttribute("card", card);
			}
			else {
				CardLevel card	= new CardLevel();
				request.setAttribute("card", card);
			}			
			mRD = request.getRequestDispatcher("card/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")) {
			if(type.equals("room")) {
				CardRoom card	= CardRoom.getCard(mID);
				if(card == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested card does not exist");
					return;
				}

				card.deleteOnDB();
			}
			else {
				CardLevel card	= CardLevel.getCard(mID);
				if(card == null){
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested card does not exist");
					return;
				}

				card.deleteOnDB();
			}
			response.sendRedirect("card?type="+type);
		}
		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
			int page   = Integer.valueOf((String) request.getParameter("page"));
			int mode   = Integer.valueOf((String) request.getParameter("mode"));
			int column = Integer.valueOf((String) request.getParameter("column"));
			if(type.equals("room")) {
				CardRoom[] cards = CardRoom.getSortedLimitedModel(column, (page-1)*20, mode);
				request.setAttribute("cards", cards);
				request.setAttribute("lastPage",  CardRoom.getTotalPage());			
			}
			else {
				CardLevel[] cards = CardLevel.getSortedLimitedModel(column, (page-1)*20, mode);
				request.setAttribute("cards", cards);
				request.setAttribute("lastPage",  CardLevel.getTotalPage());				
			}

			request.setAttribute("currentPage", page);
			request.setAttribute("currentMode", mode);
			request.setAttribute("currentColumn", column);

			mRD = request.getRequestDispatcher("card/index.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("get")) {
			PrintWriter out		= response.getWriter();

			if(type.equals("room")) {
				Room[] rooms	= Room.getAllRooms();
				
				out.print(";");
				for(int i=0,n=rooms.length;i<n;++i) {
	                out.print("<option value=\"" + rooms[i].getID() + "\">"); //<option value="BCHxxxx" 
	                out.print(rooms[i].getName()); //<option value="BCHxxxx">Jakarta
	                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
				}
			}
			else {
				Level[] levels	= Level.getAllLevels();
				
				out.print(";");
				for(int i=0,n=levels.length;i<n;++i) {
	                out.print("<option value=\"" + levels[i].getID() + "\">"); //<option value="BCHxxxx" 
	                out.print(levels[i].getPhysicalLevel()); //<option value="BCHxxxx">Jakarta
	                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
				}
			}
		}
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");
		String type	= request.getParameter("type");
		String id	= request.getParameter("id");
		
		if(cmd.equals("create")) {
			if(type.equals("room")) {
				String roomID	= request.getParameter("roomID");
				CardRoom card	= CardRoom.getCard(id);
				boolean isExist = (card != null);
				
				if(!isExist){
					card = new CardRoom();
				}

				card.setRoomID(roomID);
				
				if(isExist){
					card.editOnDB();
				} else {
					card.setID(id);
					card.addOnDB();
				}				
			}
			else {
				String levelID	= request.getParameter("levelID");
				CardLevel card	= CardLevel.getCard(id);
				boolean isExist = (card != null);
				
				if(!isExist){
					card = new CardLevel();
				}

				card.setLevelID(levelID);
				
				if(isExist){
					card.editOnDB();
				} else {
					card.setID(id);
					card.addOnDB();
				}				
			}
		} else if(cmd.equals("update")) {
			if(type.equals("room")) {
				String roomID	= request.getParameter("roomID");
				CardRoom card	= CardRoom.getCard(id);

				card.setRoomID(roomID);

				card.editOnDB();
			}
			else {
				String levelID	= request.getParameter("levelID");
				CardLevel card	= CardLevel.getCard(id);

				card.setLevelID(levelID);

				card.editOnDB();
			}
		}

		response.sendRedirect("card?type="+type);
	}

	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	}
}
