package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.User;
import com.btj.vms.model.Visitor;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/administration/UserController")
public class UserController extends BaseController{
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
    }

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		User user = null;
		if(mID != null){
			user = User.getUser(mID);
		}
		
		request.setAttribute("location", "administration");
		request.setAttribute("option", "user");
		
		if(mAction == null){
			if(IOUtilities.isAjaxRequest(request)){
				// HACK: we won't be using this for a while
				String query = request.getParameter("q");
				PrintWriter out = response.getWriter();
			}else{
				User[] users = User.getSortedLimitedModel(0, 0, 0);
				request.setAttribute("users", users);
				request.setAttribute("currentPage", 1);
				request.setAttribute("currentMode", 0);
				request.setAttribute("currentColumn", 0);
				request.setAttribute("lastPage", User.getTotalPage());

				mRD = request.getRequestDispatcher("user/index.jsp");
				mRD.forward(request, response);
			}
		}else if(mAction.equals("view")){
			if(user == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested user does not exist");
				return;
			}
			
			request.setAttribute("user", user);
			
			//Set requestDate and attendance
			String requestDate = IOUtilities.getTodayDateAsString("yyyy-MM-01"); //we just need the year and month, date must be at 01
			request.setAttribute("requestDate", requestDate);
			
			mRD = request.getRequestDispatcher("user/view.jsp");
			mRD.forward(request, response);
		}else if(mAction.equals("update")){
			if(user == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested user does not exist");
				return;
			}
			
			request.setAttribute("user", user);
			mRD = request.getRequestDispatcher("user/update.jsp");
			mRD.forward(request, response);
		}else if(mAction.equals("create")){
			request.setAttribute("user", new User());
			mRD = request.getRequestDispatcher("user/update.jsp");
			mRD.forward(request, response);
		}else if(mAction.equals("delete")){
			if(user == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested user does not exist");
				return;
			}
			
			user.deleteOnDB();
			Visitor visitor = Visitor.getVisitor(user.getUsername());
			visitor.deleteOnDB();
			response.sendRedirect("user");
		} else if((mAction.equals("page"))||(mAction.equals("sort"))) {
			int page   = Integer.valueOf((String) request.getParameter("page"));
			int mode   = Integer.valueOf((String) request.getParameter("mode"));
			int column = Integer.valueOf((String) request.getParameter("column"));
			User[] users = User.getSortedLimitedModel(column, (page-1)*20, mode);
			request.setAttribute("users", users);
			request.setAttribute("currentPage", page);
			request.setAttribute("currentMode", mode);
			request.setAttribute("currentColumn", column);
			request.setAttribute("lastPage",  User.getTotalPage());

			mRD = request.getRequestDispatcher("user/index.jsp");
			mRD.forward(request, response);
		}
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);
		
		String cmd	= request.getParameter("command");
		
		if(cmd.equals("create")) {
			doCreate(request, response);
		} else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}

	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);
		String username		= request.getParameter("username");
		User user			= User.getUser(username);
		Visitor visitor		= Visitor.getVisitor(username);
		boolean isUserExist = (user != null);
		
		if(!isUserExist){
			user	= new User();
			visitor	= new Visitor();
		}
		
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setDepartment(request.getParameter("department"));
		user.setTitle(request.getParameter("title"));
		user.setMobile(request.getParameter("mobile"));
		user.setEmail(request.getParameter("email"));
		user.setRole("ROLCENTRAL00000");
		
		visitor.setName(user.getName());
		visitor.setIdentifierType("username");
		
		if(isUserExist){
			user.editOnDB();
			visitor.editOnDB();
		}else{
			user.setUsername(username);
			user.addOnDB();
			visitor.setID(username);
			visitor.addOnDB();
		}
		
		response.sendRedirect("user?id=" + user.getUsername() + "&action=view");
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);
		String username	= request.getParameter("username");
		User user		= User.getUser(username);
		Visitor visitor	= Visitor.getVisitor(username);
		
		user.setName(request.getParameter("name"));
		user.setDepartment(request.getParameter("department"));
		user.setTitle(request.getParameter("title"));
		user.setMobile(request.getParameter("mobile"));
		user.setEmail(request.getParameter("email"));
		
		visitor.setName(user.getName());
		if(!request.getParameter("password").equals("")){
			user.setPassword(request.getParameter("password"));
		}
		
		user.editOnDB();
		visitor.editOnDB();
		response.sendRedirect("user?id=" + user.getUsername() + "&action=view");
	}
}