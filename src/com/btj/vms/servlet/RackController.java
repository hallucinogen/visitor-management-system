package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.Level;
import com.btj.vms.model.Rack;
import com.btj.vms.model.Equipment;
import com.btj.vms.model.Room;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class RackController
 */
@WebServlet("/administration/RackController")
public class RackController extends BaseController {

	private static final long serialVersionUID = 4590475337404074457L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public RackController() {
        super();
    }
    
    /**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Rack rack = null;
		if(mID != null){
			rack = Rack.getRack(mID);
		}
		
		request.setAttribute("location", "home");
		request.setAttribute("option", "building");

		if(mAction == null){
//			if(IOUtilities.isAjaxRequest(request)){
//				// HACK: we won't be using this for a while
//				String query = request.getParameter("q");
//				PrintWriter out = response.getWriter();
//			}else{
//				Rack[] racks = Rack.getSortedLimitedModel(0, 0, 0);
//				request.setAttribute("racks", racks);
//				request.setAttribute("currentPage", 1);
//				request.setAttribute("currentMode", 0);
//				request.setAttribute("currentColumn", 0);
//				request.setAttribute("lastPage", Rack.getTotalPage());
//
				mRD = request.getRequestDispatcher("rack/index.jsp");
				mRD.forward(request, response);
//			}
		}
		else if(mAction.equals("view")){
			if(rack == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested rack does not exist");
				return;
			}

			request.setAttribute("rack", rack);
			request.setAttribute("equipments", Equipment.getAllEquipmentRack(mID));

			mRD = request.getRequestDispatcher("rack/view.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("update")){
			if(rack == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested rack does not exist");
				return;
			}

			request.setAttribute("rack", rack);
			request.setAttribute("roomID", rack.getRoomID());
			
			mRD = request.getRequestDispatcher("rack/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")){
			request.setAttribute("rack", new Rack());
			request.setAttribute("roomID", mID);
			
			mRD = request.getRequestDispatcher("rack/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")){
			if(rack == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested rack does not exist");
				return;
			}

			String id		= rack.getRoomID();
			rack.deleteOnDB();
			response.sendRedirect("room?id="+id+"&action=view");
		}
//		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
//			int page   = Integer.valueOf((String) request.getParameter("page"));
//			int mode   = Integer.valueOf((String) request.getParameter("mode"));
//			int column = Integer.valueOf((String) request.getParameter("column"));
//			Rack[] racks = Rack.getSortedLimitedModel(column, (page-1)*20, mode);
//			request.setAttribute("racks", racks);
//			request.setAttribute("currentPage", page);
//			request.setAttribute("currentMode", mode);
//			request.setAttribute("currentColumn", column);
//			request.setAttribute("lastPage",  Rack.getTotalPage());
//
//			mRD = request.getRequestDispatcher("rack/index.jsp");
//			mRD.forward(request, response);
//		}
	}
    
    /**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");

		if(cmd.equals("create")) {
			doCreate(request, response);
		} else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}
	
	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);

		String roomID	= request.getParameter("roomID");
		int	capacity	= Integer.valueOf(request.getParameter("capacity"));
		
		Rack rack = new Rack();
		rack.setID(Server.instance(request).generateID(DBTable.RACK, Room.getRoom(roomID).getLevel().getBuildingID()));
		rack.setRoomID(roomID);
		rack.setCapacity(capacity);
		rack.addOnDB();
		
		response.sendRedirect("room?id=" + rack.getRoomID() + "&action=view");
	}
	
	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);

		String rackID	= request.getParameter("id");
		int	capacity	= Integer.valueOf(request.getParameter("capacity"));

		Rack rack	= Rack.getRack(rackID);
		rack.setCapacity(capacity);
		rack.editOnDB();

		response.sendRedirect("room?id=" + rack.getRoomID() + "&action=view");
	}
}
