package com.btj.vms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.model.User;

/**
 * Servlet implementation class LoginHandler
 */
@WebServlet("/LoginHandler")
public class LoginHandler extends BaseController {

	private static final long serialVersionUID = -236949525130847633L;

	public LoginHandler() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);

		if(mAction == null){
			if(request.getSession().getAttribute("User") == null) {
				response.sendRedirect("index.jsp");
			}else{
				response.sendRedirect("administration/building");
			}
		}else if(mAction.equals("logout")){
			request.getSession().setAttribute("User", null);
			request.getSession().setAttribute("lang", null);
			response.sendRedirect("index.jsp");
		}else{
			response.sendRedirect("administration/user");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		super.doPost(request, response);
		User user = new User(request.getParameter("username"), request.getParameter("password"));
		response.setContentType("text/html");
	
		if(user.authenticate()) {
			request.getSession().setAttribute("User", user);
			request.getSession().setAttribute("Login", new Boolean(true));
			response.sendRedirect("administration/building");
		} else {
			request.getSession().setAttribute("Login", false);
			response.sendRedirect("index.jsp?failed=true");
		}
	}
}

