package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.Level;
import com.btj.vms.model.Room;
import com.btj.vms.model.Rack;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class RoomController
 */
@WebServlet("/administration/RoomController")
public class RoomController extends BaseController{
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoomController() {
        super();
    }

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Room room = null;
		if(mID != null){
			room = Room.getRoom(mID);
		}
		
		request.setAttribute("location", "home");
		request.setAttribute("option", "building");

		if(mAction == null){
//			if(IOUtilities.isAjaxRequest(request)){
//				// HACK: we won't be using this for a while
//				String query = request.getParameter("q");
//				PrintWriter out = response.getWriter();
//			}
//			else{
//				Room[] rooms = Room.getSortedLimitedModel(0, 0, 0);
//				request.setAttribute("rooms", rooms);
//				request.setAttribute("currentPage", 1);
//				request.setAttribute("currentMode", 0);
//				request.setAttribute("currentColumn", 0);
//				request.setAttribute("lastPage", Room.getTotalPage());
//
				mRD = request.getRequestDispatcher("room/index.jsp");
				mRD.forward(request, response);
//			}
		}
		else if(mAction.equals("view")){
			if(room == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested room does not exist");
				return;
			}

			request.setAttribute("room", room);
			request.setAttribute("racks", Rack.getAllRackRoom(mID));

			mRD = request.getRequestDispatcher("room/view.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("update")){
			if(room == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested room does not exist");
				return;
			}

			request.setAttribute("room", room);
			request.setAttribute("levelID", room.getLevelID());

			mRD = request.getRequestDispatcher("room/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")){
			request.setAttribute("room", new Room());
			request.setAttribute("levelID", mID);

			mRD = request.getRequestDispatcher("room/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")){
			if(room == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested room does not exist");
				return;
			}

			String id		= room.getLevelID();
			room.deleteOnDB();
			response.sendRedirect("level?id="+id+"&action=view");
		}
//		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
//			int page   = Integer.valueOf((String) request.getParameter("page"));
//			int mode   = Integer.valueOf((String) request.getParameter("mode"));
//			int column = Integer.valueOf((String) request.getParameter("column"));
//			Room[] rooms = Room.getSortedLimitedModel(column, (page-1)*20, mode);
//			request.setAttribute("rooms", rooms);
//			request.setAttribute("currentPage", page);
//			request.setAttribute("currentMode", mode);
//			request.setAttribute("currentColumn", column);
//			request.setAttribute("lastPage",  Room.getTotalPage());
//
//			mRD = request.getRequestDispatcher("room/index.jsp");
//			mRD.forward(request, response);
//		}
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");

		if(cmd.equals("create")) {
			doCreate(request, response);
		}
		else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}

	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);

		String name		= request.getParameter("name");
		String levelID	= request.getParameter("levelID");
		double power	= Double.valueOf(request.getParameter("power"));
		
		Room room = new Room();
		room.setID(Server.instance(request).generateID(DBTable.ROOM, Level.getLevel(levelID).getBuildingID()));
		room.setName(name);
		room.setLevelID(levelID);
		room.setPower(power);
		room.addOnDB();
		
		response.sendRedirect("level?id=" + room.getLevelID() + "&action=view");
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);
		
		String name		= request.getParameter("name");
		String roomID	= request.getParameter("id");
		double power	= Double.valueOf(request.getParameter("power"));

		Room room	= Room.getRoom(roomID);
		room.setName(name);
		room.setPower(power);
		room.editOnDB();

		response.sendRedirect("level?id=" + room.getLevelID() + "&action=view");
	}
}
