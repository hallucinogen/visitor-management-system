package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.Barrier;
import com.btj.vms.model.Building;
import com.btj.vms.model.Level;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class BarrierController
 */
@WebServlet("/administration/BarrierController")
public class BarrierController extends BaseController{
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BarrierController() {
        super();
    }

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Barrier barrier = null;
		if(mID != null){
			barrier = Barrier.getBarrier(mID);
		}
		
		request.setAttribute("location", "home");
		request.setAttribute("option", "building");

		if(mAction == null){
//			if(IOUtilities.isAjaxRequest(request)){
//				// HACK: we won't be using this for a while
//				String query = request.getParameter("q");
//				PrintWriter out = response.getWriter();
//			}else{
//				Barrier[] barriers = Barrier.getSortedLimitedModel(0, 0, 0);
//				request.setAttribute("barriers", barriers);
//				request.setAttribute("currentPage", 1);
//				request.setAttribute("currentMode", 0);
//				request.setAttribute("currentColumn", 0);
//				request.setAttribute("lastPage", Barrier.getTotalPage());
//
				mRD = request.getRequestDispatcher("barrier/index.jsp");
				mRD.forward(request, response);
//			}
		}
		else if(mAction.equals("view")){
			if(barrier == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested barrier does not exist");
				return;
			}

			request.setAttribute("barrier", barrier);

			mRD = request.getRequestDispatcher("barrier/view.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("update")){
			if(barrier == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested barrier does not exist");
				return;
			}

			request.setAttribute("barrier", barrier);
			request.setAttribute("levelID", barrier.getLevelID());
			
			mRD = request.getRequestDispatcher("barrier/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")){
			request.setAttribute("barrier", new Barrier());
			request.setAttribute("levelID", mID);
			
			mRD = request.getRequestDispatcher("barrier/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")){
			if(barrier == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested barrier does not exist");
				return;
			}

			String id		= barrier.getLevelID();
			barrier.deleteOnDB();
			response.sendRedirect("level?id="+id+"&action=view");
		}
//		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
//			int page   = Integer.valueOf((String) request.getParameter("page"));
//			int mode   = Integer.valueOf((String) request.getParameter("mode"));
//			int column = Integer.valueOf((String) request.getParameter("column"));
//			Barrier[] barriers = Barrier.getSortedLimitedModel(column, (page-1)*20, mode);
//			request.setAttribute("barriers", barriers);
//			request.setAttribute("currentPage", page);
//			request.setAttribute("currentMode", mode);
//			request.setAttribute("currentColumn", column);
//			request.setAttribute("lastPage",  Barrier.getTotalPage());
//
//			mRD = request.getRequestDispatcher("barrier/index.jsp");
//			mRD.forward(request, response);
//		}
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");

		if(cmd.equals("create")) {
			doCreate(request, response);
		} else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}

	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);
		
		String levelID			= request.getParameter("levelID");
		String isPublicString	= request.getParameter("public");
		boolean isPublic		= isPublicString.equals("1") ? true : false;

		Barrier barrier = new Barrier();
		barrier.setID(Server.instance(request).generateID(DBTable.BARRIER, Level.getLevel(levelID).getBuildingID()));
		barrier.setLevelID(levelID);
		barrier.setIsPublic(isPublic);
		barrier.addOnDB();
		
		response.sendRedirect("level?id=" + barrier.getLevelID() + "&action=view");
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);

		String barrierID		= request.getParameter("id");
		String isPublicString	= request.getParameter("public");
		boolean isPublic		= (isPublicString.equals("") ? 1 : Integer.valueOf(isPublicString)) != 0;

		Barrier barrier	= Barrier.getBarrier(barrierID);
		barrier.setIsPublic(isPublic);
//		barrier.setLevelID(request.getParameter("levelID"));
		barrier.editOnDB();

		response.sendRedirect("level?id=" + barrier.getLevelID() + "&action=view");
	}
}