package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.model.CardLevel;

/**
 * Servlet implementation class SecurityController
 */
@WebServlet("/home/SecurityController")
public class SecurityController extends BaseController {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SecurityController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "security");

		if (mAction == null){
			CardLevel[] cards = CardLevel.getAllHeldCards();

			request.setAttribute("id", "");
			request.setAttribute("result", "");
			request.setAttribute("cards", cards);
			request.setAttribute("isSuccess", false);
			mRD = request.getRequestDispatcher("security/index.jsp");
			mRD.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "security");
		
		mID		= request.getParameter("id");
		String cmd = request.getParameter("command");
		
		if(cmd.equals("create")) {
			doCreate(request, response);
		} else if(cmd.equals("update")) {
			String cardID		= request.getParameter("cardID");
			
			CardLevel card = CardLevel.getCard(cardID);
			card.editOnDB();

			CardLevel[] cards = CardLevel.getAllHeldCards();

			request.setAttribute("id", "");
			request.setAttribute("result", "");
			request.setAttribute("cards", cards);
			request.setAttribute("isSuccess", true);
			mRD = request.getRequestDispatcher("security/index.jsp");
			mRD.forward(request, response);
		} else if(cmd.equals("verify")) {

			CardLevel[] cards = CardLevel.getAllHeldCards();
			String result = "";
			
			if(mID.equals("WPSMG_001")){ result = "confirmed"; }
			else{ result = "your Working Permit is invalid"; }
			request.setAttribute("id", mID);
			request.setAttribute("result", result);
			request.setAttribute("cards", cards);
			request.setAttribute("isSuccess", false);
			mRD = request.getRequestDispatcher("security/index.jsp");
			mRD.forward(request, response);
		}
	}

	@Override
	protected void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doCreate(request, response);
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doUpdate(request, response);
	}
}
