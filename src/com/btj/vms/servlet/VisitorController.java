package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.model.CardLevel;
import com.btj.vms.model.Level;
import com.btj.vms.model.Room;
import com.btj.vms.model.Server;
import com.btj.vms.model.Visit;
import com.btj.vms.model.Visitor;

/**
 * Servlet implementation class VisitorController
 */
@WebServlet("/home/VisitorController")
public class VisitorController extends BaseController {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitorController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "visitor");
		
		if (mAction == null){
			Room[] rooms = Room.getAllRooms();
			
			request.setAttribute("rooms", rooms);
			request.setAttribute("isSuccess", false);

			mRD = request.getRequestDispatcher("visitor/index.jsp");
			mRD.forward(request, response);
		}
		else if (mAction.equals("check")){
			PrintWriter out = response.getWriter();
			Visitor visitor = Visitor.getVisitor(mID);
			
			if (visitor == null) out.print("");
			else out.print(visitor.getIdentifierType()+";"+visitor.getName());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "visitor");
		
		String cmd = request.getParameter("command");
		
		if(cmd.equals("create")) {
			String name		= request.getParameter("name");
			String id		= request.getParameter("identifier");
			String type		= request.getParameter("type");
			String roomID	= request.getParameter("roomID");

			Visitor visitor = Visitor.getVisitor(id);
			if (visitor == null){
				visitor = new Visitor();
				visitor.setID(id);
				visitor.setName(name);
				visitor.setIdentifierType(type);
				visitor.addOnDB();
			}
			
			Visit visit = new Visit();
			visit.setID(Server.instance(request).generateID(DBTable.VISIT));
			visit.setVisitorID(id);
			visit.setActivity(true);
			visit.setRoomID(roomID);
			visit.addOnDB();
			
			request.setAttribute("visitorID", id);
			request.setAttribute("roomID", roomID);
			
			mRD = request.getRequestDispatcher("visitor/update.jsp");
			mRD.forward(request, response);
		} else if(cmd.equals("update")) {
			String visitorID	= request.getParameter("visitorID");
			String cardID		= request.getParameter("cardID");
			CardLevel card		= CardLevel.getCard(cardID);
			Room[] rooms 		= Room.getAllRooms();
			
			card.setVisitorID(visitorID);
			card.editOnDB();
			
			request.setAttribute("rooms", rooms);
			request.setAttribute("isSuccess", true);

			mRD = request.getRequestDispatcher("visitor/index.jsp");
			mRD.forward(request, response);
		}
	}
}
