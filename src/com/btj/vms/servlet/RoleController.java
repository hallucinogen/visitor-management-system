package com.btj.vms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.model.Role;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class RoleController
 */
@WebServlet("/administration/RoleController")
public class RoleController extends BaseController {

	private static final long serialVersionUID = 1L;
    
	private String[] defaultPage = {"/administration/equipment",
									"/administration/rack",
									"/administration/room",
									"/administration/barrier",
									"/administration/level",
									"/administration/building"};
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoleController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Role role = null;
		if(mID != null) {
			role = Role.getRole(mID);
		}
		
		request.setAttribute("location", "administration");
		request.setAttribute("option", "role");
		
		if(mAction == null) {
			Role[] roles = Role.getSortedLimitedModel(0, 0, 0);
			request.setAttribute("roles", roles);
			request.setAttribute("currentPage", 1);
			request.setAttribute("currentMode", 0);
			request.setAttribute("currentColumn", 0);
			request.setAttribute("lastPage", Role.getTotalPage());

			mRD = request.getRequestDispatcher("role/index.jsp");
			mRD.forward(request, response);
		} else if(mAction.equals("view")) {
			if(role == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested role does not exist");
				return;
			}
			
			request.setAttribute("role", role);
			mRD = request.getRequestDispatcher("role/view.jsp");
			mRD.forward(request, response);
		} else if(mAction.equals("update")) {
			if(role == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested role does not exist");
				return;
			}
			
			request.setAttribute("role", role);
			request.setAttribute("pages", role.getRolePageURLs());
			mRD = request.getRequestDispatcher("role/update.jsp");
			mRD.forward(request, response);
		} else if(mAction.equals("create")) {
			role = new Role();
			request.setAttribute("role", role);
			request.setAttribute("pages", role.getRolePageURLs());

			mRD = request.getRequestDispatcher("role/update.jsp");
			mRD.forward(request, response);
		} else if(mAction.equals("delete")) {
			if(role == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested role does not exist");
				return;
			}
			
			role.deleteOnDB();
			response.sendRedirect("role");
		} else if((mAction.equals("page"))||(mAction.equals("sort"))) {
			int page   = Integer.valueOf((String) request.getParameter("page"));
			int mode   = Integer.valueOf((String) request.getParameter("mode"));
			int column = Integer.valueOf((String) request.getParameter("column"));
			Role[] roles = Role.getSortedLimitedModel(column, (page-1)*20, mode);
			request.setAttribute("roles", roles);
			request.setAttribute("currentPage", page);
			request.setAttribute("currentMode", mode);
			request.setAttribute("currentColumn", column);
			request.setAttribute("lastPage", Role.getTotalPage());
			
			mRD = request.getRequestDispatcher("role/index.jsp");
			mRD.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doUpdate(request, response);

		String cmd = request.getParameter("command");
		
		if(cmd.equals("create")) {
			doCreate(request, response);
		} else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}
	
	@Override
	protected void doCreate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doCreate(request, response);
		
		String name			= request.getParameter("name");
		String[] home		= request.getParameterValues("home[]");
		String[] admin		= request.getParameterValues("admin[]");
		
		Role role = new Role();
		role.setID(Server.instance(request).generateID(DBTable.ROLE));
		role.setName(name);
		role.addOnDB();

		for(int i=0,n=defaultPage.length;i<n;++i)	{	role.addRolePage(defaultPage[i]);	}
		if(home != null){
			for(int i=0,n=home.length;i<n;++i)		{	role.addRolePage(home[i]);			}
		}
		if(admin != null){
			for(int i=0,n=admin.length;i<n;++i)		{	role.addRolePage(admin[i]);			}
		}
		
		response.sendRedirect("role?id=" + role.getID() + "&action=update");
	}
	
	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doUpdate(request, response);
		
		String roleID		= request.getParameter("id");
		String name			= request.getParameter("name");
		String[] home		= request.getParameterValues("home[]");
		String[] admin		= request.getParameterValues("admin[]");

		Role role = Role.getRole(roleID);
		role.setName(name);
		role.editOnDB();

		role.deleteAllRolePage();
		for(int i=0,n=defaultPage.length;i<n;++i)	{	role.addRolePage(defaultPage[i]);	}
		if(home != null){
			for(int i=0,n=home.length;i<n;++i)		{	role.addRolePage(home[i]);			}
		}
		if(admin != null){
			for(int i=0,n=admin.length;i<n;++i)		{	role.addRolePage(admin[i]);			}
		}

		response.sendRedirect("role?id=" + role.getID() + "&action=update");
	}
}
