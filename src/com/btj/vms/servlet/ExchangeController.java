package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.model.CardLevel;
import com.btj.vms.model.CardRoom;
import com.btj.vms.model.Room;
import com.btj.vms.model.Server;
import com.btj.vms.model.Visit;

/**
 * Servlet implementation class ExchangeController
 */
@WebServlet("/home/ExchangeController")
public class ExchangeController extends BaseController {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExchangeController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "exchange");

		PrintWriter out		= response.getWriter();

		if (mAction == null) {
			request.setAttribute("isSuccess", false);

			mRD = request.getRequestDispatcher("exchange/index.jsp");
			mRD.forward(request, response);
		}
		else if (mAction.equals("get")) {
			String cmd = (String) request.getParameter("cmd");
			if (cmd.equals("To")) {
				CardLevel card		= CardLevel.getCard(mID);
				Visit[] visits		= Visit.getAllVisitorVisits(card.getVisitorID());
				Room room			= Room.getRoom(visits[visits.length-1].getRoomID());
				CardRoom[] cards	= CardRoom.getAllNonHeldCards(room.getID());
				
				out.print(room.getName()+";");
				for(int i=0,n=cards.length;i<n;++i) {
	                out.print("<option value=\"" + cards[i].getID() + "\">"); //<option value="BCHxxxx" 
	                out.print(cards[i].getID()); //<option value="BCHxxxx">Jakarta
	                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
				}
			}
			else if (cmd.equals("From")) {
				CardRoom card		= CardRoom.getCard(mID);
				Visit[] visits		= Visit.getAllVisitorVisits(card.getVisitorID());
				Room room			= Room.getRoom(visits[visits.length-1].getRoomID());
				CardLevel[] cards	= CardLevel.getAllNonHeldCards(room.getLevelID());
				
				out.print(room.getName()+";");
				for(int i=0,n=cards.length;i<n;++i) {
	                out.print("<option value=\"" + cards[i].getID() + "\">"); //<option value="BCHxxxx" 
	                out.print(cards[i].getID()); //<option value="BCHxxxx">Jakarta
	                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
				}
			}
		}
		else if (mAction.equals("To")) {
			CardLevel[] cards = CardLevel.getAllHeldCards();
			
			out.print(";");
			for(int i=0,n=cards.length;i<n;++i) {
                out.print("<option value=\"" + cards[i].getID() + "\">"); //<option value="BCHxxxx" 
                out.print(cards[i].getID()); //<option value="BCHxxxx">Jakarta
                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
			}
		}
		else if (mAction.equals("From")) {
			CardRoom[] cards = CardRoom.getAllHeldCards();
			
			out.print(";");
			for(int i=0,n=cards.length;i<n;++i) {
                out.print("<option value=\"" + cards[i].getID() + "\">"); //<option value="BCHxxxx" 
                out.print(cards[i].getID()); //<option value="BCHxxxx">Jakarta
                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
			}
		}
		else if (mAction.equals("Return")) {
			CardLevel[] cards	= CardLevel.getAllHeldCards();
			
			out.print(";");
			for(int i=0,n=cards.length;i<n;++i) {
                out.print("<option value=\"" + cards[i].getID() + "\">"); //<option value="BCHxxxx" 
                out.print(cards[i].getID()); //<option value="BCHxxxx">Jakarta
                out.println("</option>");//<option value="BCHxxx">Jakarta</option>
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		request.setAttribute("location", "home");
		request.setAttribute("option", "exchange");

		String cmd = request.getParameter("command");
		
		if (cmd.equals("to")) {
			String cardInID		= request.getParameter("inTo");
			String cardOutID	= request.getParameter("outTo");
			CardLevel cardIn	= CardLevel.getCard(cardInID);
			CardRoom cardOut	= CardRoom.getCard(cardOutID);
			
			cardOut.setVisitorID(cardIn.getVisitorID());
			cardOut.editOnDB();
			if(!cardOut.getVisitorID().equals("")){
				cardIn.setVisitorID("");
				cardIn.editOnDB();
			}
		}
		else if (cmd.equals("from")) {
			String cardInID		= request.getParameter("inFrom");
			String cardOutID	= request.getParameter("outFrom");
			CardRoom cardIn		= CardRoom.getCard(cardInID);
			CardLevel cardOut	= CardLevel.getCard(cardOutID);
			
			cardOut.setVisitorID(cardIn.getVisitorID());
			cardOut.editOnDB();
			if(!cardOut.getVisitorID().equals("")){
				cardIn.setVisitorID("");
				cardIn.editOnDB();
			}
		}
		else if (cmd.equals("return")) {
			String cardID	= request.getParameter("inReturn");
			CardLevel card	= CardLevel.getCard(cardID);
			Visit[] visits	= Visit.getAllVisitorVisits(card.getVisitorID());

			Visit visit		= new Visit();
			visit.setID(Server.instance(request).generateID(DBTable.VISIT));
			visit.setVisitorID(card.getVisitorID());
			visit.setActivity(false);
			visit.setRoomID(visits[visits.length-1].getRoomID());
			visit.addOnDB();
			
			card.setVisitorID("");
			card.editOnDB();			
		}

		request.setAttribute("isSuccess", true);

		mRD = request.getRequestDispatcher("exchange/index.jsp");
		mRD.forward(request, response);		
	}
}