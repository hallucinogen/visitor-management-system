package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.Level;
import com.btj.vms.model.Room;
import com.btj.vms.model.Barrier;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class LevelController
 */
@WebServlet("/home/LevelController")
public class LevelController extends BaseController {

	private static final long serialVersionUID = 6112264418034984560L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public LevelController() {
        super();
    }
    
    /**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Level level = null;
		if(mID != null){
			level = Level.getLevel(mID);
		}
		
		request.setAttribute("location", "home");
		request.setAttribute("option", "building");

		if(mAction == null){
//			if(IOUtilities.isAjaxRequest(request)){
//				// HACK: we won't be using this for a while
//				String query = request.getParameter("q");
//				PrintWriter out = response.getWriter();
//			}else{
//				Level[] levels = Level.getSortedLimitedModel(0, 0, 0);
//				request.setAttribute("levels", levels);
//				request.setAttribute("currentPage", 1);
//				request.setAttribute("currentMode", 0);
//				request.setAttribute("currentColumn", 0);
//				request.setAttribute("lastPage", Level.getTotalPage());
//
				mRD = request.getRequestDispatcher("level/index.jsp");
				mRD.forward(request, response);
//			}
		}
		else if(mAction.equals("view")){
			if(level == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested level does not exist");
				return;
			}

			request.setAttribute("level", level);
			request.setAttribute("rooms", Room.getAllRoomLevel(mID));
			request.setAttribute("barriers", Barrier.getAllBarrierLevel(mID));

			mRD = request.getRequestDispatcher("level/view.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("update")){
			if(level == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested level does not exist");
				return;
			}

			request.setAttribute("level", level);
			request.setAttribute("buildingID", level.getBuildingID());
			
			mRD = request.getRequestDispatcher("level/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")){
			request.setAttribute("level", new Level());
			request.setAttribute("buildingID", mID);
			
			mRD = request.getRequestDispatcher("level/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")){
			if(level == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested level does not exist");
				return;
			}
			String id		= level.getBuildingID();
			level.deleteOnDB();
			response.sendRedirect("building?id="+id+"&action=view");
		}
//		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
//			int page   = Integer.valueOf((String) request.getParameter("page"));
//			int mode   = Integer.valueOf((String) request.getParameter("mode"));
//			int column = Integer.valueOf((String) request.getParameter("column"));
//			Level[] levels = Level.getSortedLimitedModel(column, (page-1)*20, mode);
//			request.setAttribute("levels", levels);
//			request.setAttribute("currentPage", page);
//			request.setAttribute("currentMode", mode);
//			request.setAttribute("currentColumn", column);
//			request.setAttribute("lastPage",  Level.getTotalPage());
//
//			mRD = request.getRequestDispatcher("level/index.jsp");
//			mRD.forward(request, response);
//		}
	}
    
    /**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");

		if(cmd.equals("create")) {
			doCreate(request, response);
		}
		else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}
	
	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);

		String buildingID	= request.getParameter("buildingID");
		int	physicalLevel	= Integer.valueOf(request.getParameter("physicalLevel"));
		
		Level level = new Level();
		level.setID(Server.instance(request).generateID(DBTable.LEVEL, buildingID));
		level.setBuildingID(buildingID);
		level.setPhysicalLevel(physicalLevel);
		level.addOnDB();

		response.sendRedirect("building?id=" + level.getBuildingID() + "&action=view");
	}
	
	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);

		String levelID		= request.getParameter("id");
		int	physicalLevel	= Integer.valueOf(request.getParameter("physicalLevel"));

		Level level	= Level.getLevel(levelID);
		level.setPhysicalLevel(physicalLevel);
		level.editOnDB();

		response.sendRedirect("building?id=" + level.getBuildingID() + "&action=view");
	}
}
