package com.btj.vms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.DBTable;
import com.btj.vms.model.Server;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/home/TestServlet")
public class TestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2116872412746960023L;

	/**
     * @see HttpServlet#HttpServlet()
     */
	public TestServlet() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletOutputStream out = response.getOutputStream();
		Server server = Server.instance(request);
		out.println(server.generateID(DBTable.BARRIER, "SMG"));
	}
}
