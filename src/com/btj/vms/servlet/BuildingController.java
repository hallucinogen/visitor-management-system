package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.helper.IOUtilities;
import com.btj.vms.model.Building;
import com.btj.vms.model.Level;
import com.btj.vms.model.BuildingIDGenerator;

/**
 * Servlet implementation class BuildingController
 */
@WebServlet("/administration/BuildingController")
public class BuildingController extends BaseController{
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuildingController() {
        super();
    }

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);
		
		Building building = null;
		if(mID != null){
			building = Building.getBuilding(mID);
		}
		
		request.setAttribute("location", "home");
		request.setAttribute("option", "building");

		if(mAction == null){
			if(IOUtilities.isAjaxRequest(request)){
				// HACK: we won't be using this for a while
				String query = request.getParameter("q");
				PrintWriter out = response.getWriter();
			}else{
				Building[] buildings = Building.getSortedLimitedModel(0, 0, 0);
				request.setAttribute("buildings", buildings);
				request.setAttribute("currentPage", 1);
				request.setAttribute("currentMode", 0);
				request.setAttribute("currentColumn", 0);
				request.setAttribute("lastPage", Building.getTotalPage());

				mRD = request.getRequestDispatcher("building/index.jsp");
				mRD.forward(request, response);
			}
		}
		else if(mAction.equals("view")){
			if(building == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested building does not exist");
				return;
			}

			request.setAttribute("building", building);
			request.setAttribute("levels", Level.getAllLevelBuilding(mID));

			mRD = request.getRequestDispatcher("building/view.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("update")){
			if(building == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested building does not exist");
				return;
			}

			request.setAttribute("building", building);
			mRD = request.getRequestDispatcher("building/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("create")){
			request.setAttribute("building", new Building());
			mRD = request.getRequestDispatcher("building/update.jsp");
			mRD.forward(request, response);
		}
		else if(mAction.equals("delete")){
			if(building == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested building does not exist");
				return;
			}

			BuildingIDGenerator generator = BuildingIDGenerator.getBuildingIDGenerator(building.getID());
			generator.deleteOnDB();
			building.deleteOnDB();
			response.sendRedirect("building");
		}
//		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
//			int page   = Integer.valueOf((String) request.getParameter("page"));
//			int mode   = Integer.valueOf((String) request.getParameter("mode"));
//			int column = Integer.valueOf((String) request.getParameter("column"));
//			Building[] buildings = Building.getSortedLimitedModel(column, (page-1)*20, mode);
//			request.setAttribute("buildings", buildings);
//			request.setAttribute("currentPage", page);
//			request.setAttribute("currentMode", mode);
//			request.setAttribute("currentColumn", column);
//			request.setAttribute("lastPage",  Building.getTotalPage());
//
//			mRD = request.getRequestDispatcher("building/index.jsp");
//			mRD.forward(request, response);
//		}
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		String cmd	= request.getParameter("command");
		
		if(cmd.equals("create")) {
			doCreate(request, response);
		}
		else if(cmd.equals("update")) {
			doUpdate(request, response);
		}
	}

	@Override
	protected void doCreate(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException{
		super.doCreate(request, response);
		String id						= request.getParameter("id");
		Building building				= Building.getBuilding(id);
		BuildingIDGenerator generator	= BuildingIDGenerator.getBuildingIDGenerator(id);
		boolean isExist 				= (building != null);
		
		if(!isExist){
			building	= new Building();
			generator	= new BuildingIDGenerator();
		}

		building.setName(request.getParameter("name"));
		building.setLocation(request.getParameter("location"));

		if(isExist){
			building.editOnDB();
		}
		else {
			building.setID(id);
			building.addOnDB();
			generator.SetBuildingID(id);
			generator.addOnDB();
		}

		response.sendRedirect("building");
	}

	@Override
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		super.doUpdate(request, response);
		String id			= request.getParameter("id");
		Building building	= Building.getBuilding(id);

		building.setName(request.getParameter("name"));
		building.setLocation(request.getParameter("location"));

		building.editOnDB();
		response.sendRedirect("building");
	}
}
