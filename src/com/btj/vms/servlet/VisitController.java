package com.btj.vms.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btj.vms.model.Visit;

/**
 * Servlet implementation class VisitController
 */
@WebServlet("/home/VisitController")
public class VisitController extends BaseController {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doGet(request, response);

		request.setAttribute("location", "administration");
		request.setAttribute("option", "visit");

		if (mAction == null){
			mRD = request.getRequestDispatcher("visit/index.jsp");
			mRD.forward(request, response);
		}		
		else if((mAction.equals("page"))||(mAction.equals("sort"))) {
			int page		= Integer.valueOf((String) request.getParameter("page"));
			int mode		= Integer.valueOf((String) request.getParameter("mode"));
			int column		= Integer.valueOf((String) request.getParameter("column"));
			int activity	= Integer.valueOf((String) request.getParameter("activity"));
			String from		= request.getParameter("from");
			String to		= request.getParameter("to");
			Visit[] visits	= Visit.getSortedLimitedModel(column, (page-1)*20, mode, activity);
			
			request.setAttribute("visits", visits);
			request.setAttribute("currentPage", page);
			request.setAttribute("currentMode", mode);
			request.setAttribute("currentColumn", column);
			request.setAttribute("lastPage",  Visit.getTotalPage(activity));

			request.setAttribute("activity", activity);
			request.setAttribute("from", from);
			request.setAttribute("to", to);
	
			mRD = request.getRequestDispatcher("visit/view.jsp");
			mRD.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);
		String activity = request.getParameter("activity");
		String from		= request.getParameter("begin-date");
		String to		= request.getParameter("end-date");
		response.sendRedirect("visit?action=page&page=0&mode=0&column=0&activity="+activity+"&from="+from+"&to="+to);
	}
}