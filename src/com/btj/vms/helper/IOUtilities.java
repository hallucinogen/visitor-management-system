package com.btj.vms.helper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Hallucinogen
 */
public class IOUtilities {
    public static String createChecksum(String filename) {
        try {
            BufferedInputStream bis =  new BufferedInputStream(new FileInputStream(filename));

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = bis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            bis.close();
            byte[] checkSum = complete.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < checkSum.length; i++) {
                sb.append(Integer.toString( ( checkSum[i] & 0xff ) + 0x100, 16).substring( 1 ));
            }
            return sb.toString();
        } catch (Exception ex) {
            
        }
        return null;
    }
    
    /**
	 * read a file and convert content of that file to string
	 * @param filepath
	 * @return
	 */
    public static String readFileAsString(String filePath) {
    	try {
    		return readStreamAsString(new FileInputStream(filePath));
    	} catch (FileNotFoundException fEx) {
    		
    	}

    	return "";
    }

    /**
	 * convert content of a stream to string
	 * @param stream
	 * @return
	 */
    public static String readStreamAsString(InputStream stream) {
        StringBuilder builder = new StringBuilder();

        try {
            BufferedInputStream bis = new BufferedInputStream(stream);
            int chr = 0;
            while ((chr = bis.read()) != -1) {
                builder.append((char) chr);
            }

            bis.close();
        } catch (IOException iEx) {
        }

        return builder.toString();
    }

    public static void writeToFileAsString(String filePath, String data) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));

            final byte[] byteData = data.getBytes();

            bos.write(byteData);

            bos.close();
        } catch (IOException iEx) {
            
        }
    }

    public static String compressString(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        return new String(compressStringToByte(str));
    }

    public static byte[] compressStringToByte(String str) {
        try {
            if (str == null || str.length() == 0) {
                return null;
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes("ISO-8859-1"));
            gzip.close();
            return out.toByteArray();
        } catch (IOException iEx) {
            
        }
        return null;
    }

    /**
     * Decompress byte of a string to its original string.
     * We must be sure that this byte array is from string
     * @param strByte
     * @return
     */
    public static String decompressByteToString(byte[] strByte) {
        try {
            if (strByte == null || strByte.length == 0) {
                return null;
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            /*Inflater inflater = new Inflater(true);
            inflater.setInput(strByte);
            while (inflater.inflate(bbuf) != 0) {
                out.write(bbuf);
            }
            inflater.end();*/
            ByteArrayInputStream in = new ByteArrayInputStream(strByte);
            GZIPInputStream gzip = new GZIPInputStream(in);
            byte[] bbuf = new byte[512];
            int readByte = 0;
            while ((readByte = gzip.read(bbuf)) != -1) {
                out.write(bbuf, 0, readByte);
            }
            gzip.close();
            return out.toString("ISO-8859-1");
        } catch (IOException iEx) {
            System.out.println(iEx.toString());
        }
       return null;
    }

    public static String decompressString(String str) {
        try {
            if (str == null || str.length() == 0) {
                return str;
            }
            return decompressByteToString(str.getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException unEx) {
            
        }
        return null;
    }

    public static void zipFile(String filePath) {
        try {
            File input = new File(filePath);

            File output = new File(input.getPath() + ".zip");

            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(output)));

            if (input.isDirectory()) {
                addDirectory(out, input, "");
            } else {
                addFile(out, input, "");
            }

            out.flush();
            out.close();
        } catch (IOException iEx) {
            
        }
    }

    public static void unzipFile(String filePath, String unzipRootPath) {
        try {
            ZipFile zip = new ZipFile(filePath);

            Enumeration< ? extends ZipEntry> zipEnum = zip.entries();

            while (zipEnum.hasMoreElements()) {
                ZipEntry item = zipEnum.nextElement();

                String newFilePath = unzipRootPath + File.separatorChar + item.getName();
                if (item.isDirectory()) {
                    File newDir = new File(newFilePath);
                    newDir.mkdirs();
                } else {
                    File newFile = new File(newFilePath);
                    new File(newFile.getParent()).mkdirs();

                    BufferedInputStream bis = new BufferedInputStream(zip.getInputStream(item));
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(newFilePath));

                    byte[] data = new byte[4096];
                    int count;
                    while ((count = bis.read(data)) != -1) {
                        bos.write(data, 0, count);
                    }

                    IOUtilities.closeStream(bis);
                    IOUtilities.closeStream(bos);
                }
            }
        } catch (IOException iEx) {
            
        }
    }

    private static void addDirectory(ZipOutputStream out, File file, String parentPathRelativeFromRoot) {
        File[] files = file.listFiles();
        String parentPath = null;
        if (parentPathRelativeFromRoot.equals("")) {
            parentPath = "";
        } else {
            parentPath = parentPathRelativeFromRoot + File.separator;
        }

        for (int i = 0; i < files.length; ++i) {
            if (files[i].isDirectory()) {
                addDirectory(out, files[i], parentPath + files[i].getName());
            }

            // either directory or not, add it
            addFile(out, files[i], parentPathRelativeFromRoot);
        }
    }

    private static void addFile(ZipOutputStream out, File file, String parentPathRelativeFromRoot) {
        try {
            BufferedInputStream bis = null;
            byte[] data = new byte[4096];

            String zipEntryPath = null;
            if (!parentPathRelativeFromRoot.equals("")) {
                zipEntryPath = parentPathRelativeFromRoot + File.separator + file.getName();
            } else {
                zipEntryPath = file.getName();
            }

            bis = new BufferedInputStream(new FileInputStream(file), data.length);
            out.putNextEntry(new ZipEntry(zipEntryPath));

            int count;
            while ((count = bis.read(data)) != -1) {
                out.write(data, 0, count);
            }

            out.closeEntry();
            bis.close();
        } catch (IOException iEx) {
            
        }
    }

    public static void closeStream(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException iEx) {
            
        }
    }

    public static void copyFile(String srFile, String dtFile){
        try{
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            InputStream in = new FileInputStream(f1);

            //For Overwrite the file.
            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0){
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
            System.out.println("File copied.");
        }catch(FileNotFoundException ex){
            System.out.println(ex.getMessage() + " in the specified directory.");
            System.exit(0);
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
      }

    public static boolean isAjaxRequest(HttpServletRequest request){
    	return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
    
    /**
     * if num.length()<6, add leading zeros so the length is 6
     * @param num
     * @return
     */
    public static String pad(String num) {
		String leadingZeros = "";
		if(num.length()<6){
			for(int i=0,n=6-num.length();i<n;++i){
				leadingZeros += "0"; 
			}
			
		    num = leadingZeros + num;
		}
		
		return num;
	}
    
    /**
     * Convert December 2012 into 2012-12-01; February 2024 into 2012-02-01
     */
    public static String getNumericRepFromDateString(String date){
    	String retval = "";
    	String[] split = date.split(" ");
    	
    	if(split.length == 2){
    		String month = split[0];
    		retval += split[1] + "-";
    		
    		if(month.equals("January"))			{	retval += "01";	}
    		else if(month.equals("February"))	{	retval += "02";	}
    		else if(month.equals("March"))		{	retval += "03";	}
    		else if(month.equals("April"))		{	retval += "04";	}
    		else if(month.equals("May"))		{	retval += "05";	}
    		else if(month.equals("June"))		{	retval += "06";	}
    		else if(month.equals("July"))		{	retval += "07";	}
    		else if(month.equals("August"))		{	retval += "08";	}
    		else if(month.equals("September"))	{	retval += "09";	}
    		else if(month.equals("October"))	{	retval += "10";	}
    		else if(month.equals("November"))	{	retval += "11";	}
    		else if(month.equals("December"))	{	retval += "12";	}
    		
    		retval += "-01";
    	}
    	
    	return retval;
    }
    
    /**
     * Convert date object into string with format 'format'
     * @param date
     * @param format
     * @return
     */
    public static String convertDateToString(Date date, String format){
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	return sdf.format(date);
    }
    
    public static Date convertStringToDate(String date, String format){
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	
    	sdf.setLenient(false);
    	try {
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	return new Date(1);
    }
    
    public static String getTodayDateAsString(String format){
    	GregorianCalendar date = new GregorianCalendar();
    	return IOUtilities.convertDateToString(date.getTime(), format);
    }
    
    public static String formatDouble(Double number, String format, boolean groupingUsed){
    	DecimalFormat df = new DecimalFormat(format);
    	df.setGroupingUsed(groupingUsed);
    	return df.format(number);
    }
    
    public static String formatStringsToJSONArray(String[] inputs){
		JSONArray retval = new JSONArray();
		for(int i=0,n=inputs.length; i<n; ++i) {
			if(inputs[i] != null){
				retval.put(inputs[i]);
			}
		}
		
		return retval.toString();
    }
    
    public static String[] getStringsFromJSONArray(String inputs){
    	JSONArray strings = null;
    	String[] retval = new String[0];
		
    	try {
			strings = new JSONArray(inputs);
			retval = new String[strings.length()];
			for (int i = 0, n = strings.length(); i < n; ++i) {
	        	retval[i] = strings.getString(i);
	        }
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
        return retval;
    }

    public static final String TRACKER_URL = "http://lwastuargo-xxi.appspot.com/xxi_server_tracker/track";
    
    // UNIT TEST
    public static void main(String[] args) {
        //zipFile("C:\\Users\\1\\Downloads\\Blog");
        //unzipFile("C:\\Users\\1\\Downloads\\Blog.zip", "C:\\Users\\1\\Downloads\\TestUnzip");
        String zipped = compressString("Gogo ganteng");
        String unzipped = decompressString(zipped);
        byte[] b = zipped.getBytes();
        String zippedFromByte = new String(b);
        String unzippedFromByte = decompressString(zippedFromByte);
        System.out.println("1 " + zipped);
        System.out.println("2 " + unzipped);
        System.out.println("3 " + zippedFromByte);
        System.out.println("4 " + unzippedFromByte);
        System.out.println("5 " + zipped.equals(zippedFromByte));
        System.out.println("6 " + zipped.compareTo(zippedFromByte));
    }
}

