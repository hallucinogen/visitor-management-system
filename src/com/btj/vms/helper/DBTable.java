package com.btj.vms.helper;

public class DBTable {
	public static final String APPROVAL					= "approval";
	public static final String APPROVALSHEET			= "approvalsheet";
	public static final String APPROVALSHEET_EQUIPMENT	= "approvalsheet_equipment";
	public static final String BARRIER					= "barrier";
	public static final String BUILDING					= "building";
	public static final String BUILDING_ID_GENERATOR	= "building_id_generator";
	public static final String CARD						= "card";
	public static final String CARD_BARRIER				= "card_barrier";
	public static final String CARD_LEVEL				= "card_level";
	public static final String CARD_ROOM				= "card_room";
	public static final String COMPUTER_ITEM			= "computeritem";
	public static final String EQUIPMENT				= "equipment";
	public static final String LEVEL					= "level";
	public static final String RACK						= "rack";
	public static final String ROLE						= "role";
	public static final String ROLE_PAGE				= "role_page";
	public static final String ROOM						= "room";
	public static final String USER						= "user";
	public static final String VISIT					= "visit";
	public static final String VISITOR					= "visitor";
	public static final String WORKING_PERMIT			= "workingpermit";
	public static final String WORKING_AUTHENTICATOR	= "workingpermit";	
}
