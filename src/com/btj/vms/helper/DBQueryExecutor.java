package com.btj.vms.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.btj.vms.model.Server;

public class DBQueryExecutor {

	public DBQueryExecutor() {
		this(DB_ADDRESS, DB_NAME, Server.instance().getMainServerUser(), Server.instance().getMainServerPass());
	}

	/**
	 * make a DB wrapper to the given address and DB name. Require username and password
	 * @param address
	 * @param dbName
	 * @param dbUsername
	 * @param dbPassword
	 */
	public DBQueryExecutor(String address, String dbName, String dbUsername, String dbPassword) {
		mDBAddress 	= address;
		mDBName		= dbName;
		mDBUsername	= dbUsername;
		mDBPassword	= dbPassword;

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
	}

	private void connectToDatabase(String address, String dbName, String dbUsername, String dbPassword) throws SQLException {
		mConnection = DriverManager.getConnection(String.format(
			"jdbc:mysql://%s/%s?user=%s&password=%s",
			address, dbName, dbUsername, dbPassword));
	}

	/**
	 * Execute a given SQL query
	 * @param query
	 * @return result of the query from predefined database
	 * @throws SQLException
	 */
	public ResultSet executeQuery(String query) throws SQLException {
		// if connection is null/closed, try to make connection
		if (mConnection == null || mConnection.isClosed()) {
			connectToDatabase(mDBAddress, mDBName, mDBUsername, mDBPassword);
		}
		mStatement = mConnection.createStatement();
		if (mStatement.execute(query)) {
			return mStatement.getResultSet();
		} else {
			// query without result (or invalid query), return null
			return null;
		}
	}

	public PreparedStatement makePreparedStatement(String query) throws SQLException{
		if (mConnection == null || mConnection.isClosed()) {
			connectToDatabase(mDBAddress, mDBName, mDBUsername, mDBPassword);
		}
		
		return mConnection.prepareStatement(query);
	}

	/**
	 * close the query statement.
	 * We must call this function after calling executeQuery to close connection
	 * @throws SQLException
	 */
	public void closeQuery() throws SQLException {
		if (mStatement != null) {
			mStatement.close();
		}
	}

	public void closeConnection() throws SQLException {
		if (mConnection != null) {
			mConnection.close();
		}
	}

	public boolean isConnected() throws SQLException {
		return mConnection != null && !mConnection.isClosed();
	}

	/**
	 * singleton of DBQueryExecutor which uses default DB configuration
	 * it makes wrapper for local DB connection
	 * @return
	 */
	public static DBQueryExecutor instance() {
		if (sInstance == null) {
			sInstance = new DBQueryExecutor();
		}
		return sInstance;
	}

	private Statement mStatement;
	private Connection mConnection;

	protected String mDBAddress;
	protected String mDBName;
	protected String mDBUsername;
	protected String mDBPassword;

	/** singleton */
	private static DBQueryExecutor sInstance;
	private final static String DB_ADDRESS	= "localhost";
	public final static String DB_NAME		= "vms";
	private final static String DB_USERNAME = "root";
	private final static String DB_PASSWORD = "";
}
