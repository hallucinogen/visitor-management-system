package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Level {
	/**
	 * Minimalist Level constructor (for create)
	 */
	public Level() {
		this("","", 0);
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param buildingID
	 * @param physicalLevel
	 */
	public Level(String id, String buildingID, Integer physicalLevel) {
		mID 			= id;
		mBuildingID 	= buildingID;
		mPhysicalLevel	= physicalLevel;
	}

	/**
	 * get all Level from DB
	 * @return
	 */
	public static Level[] getAllLevels() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Level[] retval = new Level[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.LEVEL));
			if (result != null) {
				ArrayList<Level> array = new ArrayList<Level>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Level[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Level from DB
	 * @param id specified Level
	 * @return
	 */
	public static Level getLevel(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Level retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `levelID` = '%s';", DBTable.LEVEL, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all Level from Building
	 * @param buildingID
	 * @return
	 */
	public static Level[] getAllLevelBuilding(String buildingID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Level[] retval = new Level[0];
		String stmt = String.format("SELECT * FROM `%s` WHERE `buildingID` = '%s' ORDER BY `physicalLevel` ASC;", DBTable.LEVEL, buildingID);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Level> array = new ArrayList<Level>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Level[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Level[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Level[] retval = new Level[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"levelID", "buildingID", "physicalLevel"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.LEVEL, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Level> array = new ArrayList<Level>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Level[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.LEVEL));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Level getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("levelID");
		String buildingID 	= result.getString("buildingID");
		int physicalLevel	= result.getInt("physicalLevel");
		return new Level(id, buildingID, physicalLevel);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`levelID`, `buildingID`, `physicalLevel`) VALUES ('%s', '%s', '%s');"
				, DBTable.LEVEL, mID, mBuildingID, mPhysicalLevel);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `levelID` = '%s';", DBTable.LEVEL, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `buildingID` = '%s', `physicalLevel` = '%s'  WHERE `levelID` = '%s';"
				, DBTable.LEVEL, mBuildingID, mPhysicalLevel, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()			{	return mID;				}
	public String getBuildingID()	{	return mBuildingID;		}
	public Integer getPhysicalLevel()	{	return mPhysicalLevel;	}

	public Building getBuilding() {
		return Building.getBuilding(mBuildingID);
	}

	@Override
	public String toString() {
		return getBuilding().getName() + " - " + mPhysicalLevel;
	}

	public void setID(String id)					{	mID = id;						}
	public void setBuildingID(String buildingID)	{	mBuildingID = buildingID;		}
	public void setPhysicalLevel(int physicalLevel)	{	mPhysicalLevel = physicalLevel;	}

	private String mID;
	private String mBuildingID;
	private Integer mPhysicalLevel;
}
