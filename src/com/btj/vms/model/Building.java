package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Building {
	/**
	 * Minimalist Building constructor (for create)
	 */
	public Building() {
		this("","", "");
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param name
	 * @param location
	 */
	public Building(String id, String name, String location) {
		mID 			= id;
		mName 			= name;
		mLocation 		= location;
	}

	/**
	 * get all Building from DB
	 * @return
	 */
	public static Building[] getAllBuildings() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Building[] retval = new Building[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.BUILDING));
			if (result != null) {
				ArrayList<Building> array = new ArrayList<Building>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Building[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Building from DB
	 * @param id specified Building
	 * @return
	 */
	public static Building getBuilding(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Building retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `buildingID` = '%s';", DBTable.BUILDING, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Building[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Building[] retval = new Building[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"buildingID", "name", "location"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.BUILDING, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Building> array = new ArrayList<Building>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Building[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.BUILDING));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Building getModelFromResultSet(ResultSet result) throws SQLException {
		String id 		= result.getString("buildingID");
		String name 	= result.getString("name");
		String location	= result.getString("location");
		return new Building(id, name, location);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`buildingID`, `name`, `location`) VALUES ('%s', '%s', '%s');"
				, DBTable.BUILDING, mID, mName, mLocation);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `buildingID` = '%s';", DBTable.BUILDING, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `name` = '%s', `location` = '%s' WHERE `buildingID` = '%s';"
				, DBTable.BUILDING, mName, mLocation, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()				{	return mID;				}
	public String getName()				{	return mName;			}
	public String getLocation()			{	return mLocation;		}

	public void setID(String id)				{	mID				= id;		}
	public void setName(String name)			{	mName			= name;		}
	public void setLocation(String location)	{	mLocation		= location;	}

	private String mID;
	private String mName;
	private String mLocation;
}
