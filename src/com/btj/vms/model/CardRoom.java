package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class CardRoom {
	/**
	 * Minimalist Card constructor (for create)
	 */
	public CardRoom() {
		this("","","");
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param roomID
	 * @param visitorID
	 */
	public CardRoom(String id, String roomID, String visitorID) {
		mID			= id;
		mRoomID		= roomID;
		mVisitorID	= visitorID;
	}

	/**
	 * get all Card from DB
	 * @return
	 */
	public static CardRoom[] getAllCards() {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardRoom[] retval = new CardRoom[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.CARD_ROOM));
			if (result != null) {
				ArrayList<CardRoom> array = new ArrayList<CardRoom>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardRoom[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all non held Card from DB with specified roomID
	 * @param roomID
	 * @return
	 */
	public static CardRoom[] getAllNonHeldCards(String roomID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardRoom[] retval = new CardRoom[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `roomID` = '%s' AND `visitorID` = '';", DBTable.CARD_ROOM, roomID));
			if (result != null) {
				ArrayList<CardRoom> array = new ArrayList<CardRoom>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardRoom[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all held Card from DB
	 * @return
	 */
	public static CardRoom[] getAllHeldCards() {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardRoom[] retval = new CardRoom[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `visitorID` <> '';", DBTable.CARD_ROOM));
			if (result != null) {
				ArrayList<CardRoom> array = new ArrayList<CardRoom>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardRoom[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get a Card from DB
	 * @param id specified Card
	 * @return
	 */
	public static CardRoom getCard(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardRoom retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `cardID` = '%s';", DBTable.CARD_ROOM, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static CardRoom[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardRoom[] retval = new CardRoom[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"cardID", "roomID", "visitorID"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.CARD_ROOM, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<CardRoom> array = new ArrayList<CardRoom>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardRoom[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.CARD_ROOM));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static CardRoom getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("cardID");
		String roomID		= result.getString("roomID");
		String visitorID	= result.getString("visitorID");
		
		return new CardRoom(id, roomID, visitorID);
	}
	
	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`cardID`, `roomID`, `visitorID`) VALUES ('%s', '%s', '%s');"
				, DBTable.CARD_ROOM, mID, mRoomID, mVisitorID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `cardID` = '%s';", DBTable.CARD_ROOM, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `roomID` = '%s', `visitorID` = '%s' WHERE `cardID` = '%s';"
				, DBTable.CARD_ROOM, mRoomID, mVisitorID, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()			{	return mID;			}
	public String getRoomID()		{	return mRoomID;		}
	public String getVisitorID()	{	return mVisitorID;	}

	public Room getRoom() {
		if (mRoom == null) {
			mRoom = Room.getRoom(mRoomID);
		}
		return mRoom;
	}

	public void setID(String id)				{	mID			= id;			}
	public void setRoomID(String roomID)		{	mRoomID		= roomID;		}
	public void setVisitorID(String visitorID)	{	mVisitorID	= visitorID;	}	

	private String mID;
	private String mRoomID;
	private String mVisitorID;

	private Room mRoom;
}