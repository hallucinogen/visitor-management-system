package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class CardLevel {
	/**
	 * Minimalist Card constructor (for create)
	 */
	public CardLevel() {
		this("","","");
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param levelID
	 * @param visitorID
	 */
	public CardLevel(String id, String levelID, String visitorID) {
		mID			= id;
		mLevelID	= levelID;
		mVisitorID	= visitorID;
	}

	/**
	 * get all Card from DB
	 * @return
	 */
	public static CardLevel[] getAllCards() {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardLevel[] retval = new CardLevel[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.CARD_LEVEL));
			if (result != null) {
				ArrayList<CardLevel> array = new ArrayList<CardLevel>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardLevel[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all non held Card from DB with specified LevelID
	 * @param levelID
	 * @return
	 */
	public static CardLevel[] getAllNonHeldCards(String levelID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardLevel[] retval = new CardLevel[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `levelID` = '%s' AND `visitorID` = '';", DBTable.CARD_LEVEL, levelID));
			if (result != null) {
				ArrayList<CardLevel> array = new ArrayList<CardLevel>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardLevel[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all held Card from DB
	 * @return
	 */
	public static CardLevel[] getAllHeldCards() {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardLevel[] retval = new CardLevel[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `visitorID` <> '';", DBTable.CARD_LEVEL));
			if (result != null) {
				ArrayList<CardLevel> array = new ArrayList<CardLevel>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardLevel[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get a Card from DB
	 * @param id specified Card
	 * @return
	 */
	public static CardLevel getCard(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardLevel retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `cardID` = '%s';", DBTable.CARD_LEVEL, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static CardLevel[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		CardLevel[] retval = new CardLevel[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"cardID", "levelID", "visitorID"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.CARD_LEVEL, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<CardLevel> array = new ArrayList<CardLevel>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new CardLevel[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.CARD_LEVEL));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static CardLevel getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("cardID");
		String levelID		= result.getString("levelID");
		String visitorID	= result.getString("visitorID");
		
		return new CardLevel(id, levelID, visitorID);
	}
	
	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`cardID`, `levelID`, `visitorID`) VALUES ('%s', '%s', '%s');"
				, DBTable.CARD_LEVEL, mID, mLevelID, mVisitorID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `cardID` = '%s';", DBTable.CARD_LEVEL, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `levelID` = '%s', `visitorID` = '%s' WHERE `cardID` = '%s';"
				, DBTable.CARD_LEVEL, mLevelID, mVisitorID, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()			{	return mID;			}
	public String getLevelID()		{	return mLevelID;	}
	public String getVisitorID()	{	return mVisitorID;	}

	public Level getLevel() {
		if (mLevel == null) {
			mLevel = Level.getLevel(mLevelID);
		}
		return mLevel;
	}

	public void setID(String id)				{	mID			= id;			}
	public void setLevelID(String levelID)		{	mLevelID	= levelID;		}
	public void setVisitorID(String visitorID)	{	mVisitorID	= visitorID;	}	

	private String mID;
	private String mLevelID;
	private String mVisitorID;

	private Level mLevel;
}