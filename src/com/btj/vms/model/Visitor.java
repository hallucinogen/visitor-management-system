package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Visitor {
	/**
	 * Minimalist Visitor constructor (for create)
	 */
	public Visitor(){
		this("", "", "");
	}
	
	/**
	 * default constructor for getting from DB
	 * @param id
	 * @param name
	 * @param identifierType
	 */
	public Visitor (String id, String name, String identifierType) {
		mID = id;
		mName = name;
		mIdentifierType = identifierType;
	}
	
	/**
	 * get all Visitor from DB
	 * @return
	 */
	public static Visitor[] getAllVisitors() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visitor[] retval = new Visitor[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.VISITOR));
			if (result != null) {
				ArrayList<Visitor> array = new ArrayList<Visitor>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Visitor[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Visitor from DB
	 * @param id specified Visitor
	 * @return
	 */
	public static Visitor getVisitor(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visitor retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `visitorID` = '%s';", DBTable.VISITOR, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Visitor[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visitor[] retval = new Visitor[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"visitorID", "name", "identifierType"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.VISITOR, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Visitor> array = new ArrayList<Visitor>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Visitor[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.VISITOR));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Visitor getModelFromResultSet(ResultSet result) throws SQLException {
		String id 				= result.getString("visitorID");
		String name 			= result.getString("name");
		String identifierType	= result.getString("identifierType");
		return new Visitor(id, name, identifierType);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`visitorID`, `name`, `identifierType`) VALUES ('%s', '%s', '%s');"
				, DBTable.VISITOR, mID, mName, mIdentifierType);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `visitorID` = '%s';", DBTable.VISITOR, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `name` = '%s', `identifierType` = '%s' WHERE `visitorID` = '%s';"
				, DBTable.VISITOR, mName, mIdentifierType, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public String getID() 					{	return mID;				}
	public String getName() 				{	return mName;			}
	public String getIdentifierType()		{	return mIdentifierType;	}
	
	public void setID(String ID) 							{	mID				= ID;				}
	public void setName(String name) 						{	mName 			= name;				}
	public void setIdentifierType(String identifierType)	{	mIdentifierType = identifierType;	}
	
	private String mID;
	private String mName;
	private String mIdentifierType;
}
