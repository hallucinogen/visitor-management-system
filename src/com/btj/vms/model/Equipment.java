package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Equipment {
	/**
	 * Minimalist Equipment constructor (for create)
	 */
	public Equipment() {
		this("", false, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, false, "", "", false);
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param current
	 * @param phase
	 * @param heat
	 * @param power
	 * @param weight
	 * @param height
	 * @param width
	 * @param depth
	 * @param redundant
	 * @param rackID
	 * @param description
	 * @param installed
	 */
	public Equipment(String id, Boolean current, Integer phase, Double heat, Double power, Double weight, Double height, Double width, Double depth, Boolean redundant, String rackID, String description, Boolean installed) {
		mID 			= id;
		mCurrent		= current;
		mPhase			= phase;
		mHeat			= heat;
		mPower			= power;
		mWeight			= weight;
		mHeight			= height;
		mWidth			= width;
		mDepth			= depth;
		isRedundant		= redundant;
		mRackID			= rackID;
		mDescription	= description;
		isInstalled		= installed;
	}


	/**
	 * get all Equipment from DB
	 * @return
	 */
	public static Equipment[] getAllEquipments() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Equipment[] retval = new Equipment[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.EQUIPMENT));
			if (result != null) {
				ArrayList<Equipment> array = new ArrayList<Equipment>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Equipment[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Equipment from DB
	 * @param id specified Equipment
	 * @return
	 */
	public static Equipment getEquipment(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Equipment retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `equipmentID` = '%s';", DBTable.EQUIPMENT, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all Equipment from Rack
	 * @param rackID
	 * @return
	 */
	public static Equipment[] getAllEquipmentRack(String rackID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Equipment[] retval = new Equipment[0];
		String stmt = String.format("SELECT * FROM `%s` WHERE `rackID` = '%s';", DBTable.EQUIPMENT, rackID);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Equipment> array = new ArrayList<Equipment>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Equipment[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Equipment[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Equipment[] retval = new Equipment[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"equipmentID", "current", "phase", "heat", "power", "weight", "height", "width", "depth", "redundant", "rackID", "description", "isInstalled"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.EQUIPMENT, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Equipment> array = new ArrayList<Equipment>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Equipment[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.EQUIPMENT));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Equipment getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("equipmentID");
		Boolean current 	= result.getInt("current") != 0;
		Integer phase 		= result.getInt("phase");
		Double heat 		= result.getDouble("heat");
		Double power 		= result.getDouble("power");
		Double weight 		= result.getDouble("weight");
		Double height 		= result.getDouble("height");
		Double width 		= result.getDouble("width");
		Double depth 		= result.getDouble("depth");
		Boolean redundant	= result.getInt("redundant") != 0;
		String rackID 		= result.getString("rackID");
		String description 	= result.getString("description");
		Boolean installed	= result.getInt("isInstalled") != 0;
		return new Equipment(id, current, phase, heat, power, weight, height, width, depth, redundant, rackID, description, installed);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`equipmentID`, `current`, `phase`, `heat`, `power`, `weight`, `height`, `width`, `depth`, `redundant`, `rackID`, `description`, `isInstalled`)"+
				" VALUES ('%s', '%d', '%d', '%lf', '%lf', '%lf', '%lf', '%lf', '%lf', '%d', '%s', '%s', '%d');"
				, DBTable.EQUIPMENT, mID, mCurrent ? 1 : 0, mPhase, mHeat, mPower, mWeight, mHeight, mWidth, mDepth, isRedundant ? 1 : 0, mRackID, mDescription, isInstalled ? 1 : 0);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `equipmentID` = '%s';", DBTable.EQUIPMENT, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `current` = '%d', `phase` = '%d', `heat` = '%lf', `power` = '%lf', `weight` = '%lf', `height` = '%lf', `width` = '%lf'," +
				" `depth` = '%lf', `redundant` = '%d', `rackID` = '%s', `description` = '%s' , `isInstalled` = '%d' WHERE `equipmentID` = '%s';"
				, DBTable.EQUIPMENT, mCurrent ? 1 : 0, mPhase, mHeat, mPower, mWeight, mHeight, mWidth, mDepth, isRedundant ? 1 : 0, mRackID, mDescription, isInstalled ? 1 : 0, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()			{	return mID;				}
	public Boolean getCurrent()		{	return mCurrent;		}
	public Integer getPhase()		{	return mPhase;			}
	public Double getHeat()			{	return mHeat;			}
	public Double getPower()		{	return mPower;			}
	public Double getWeight()		{	return mWeight;			}
	public Double getHeight()		{	return mHeight;			}
	public Double getWidth()		{	return mWidth;			}
	public Double getDepth()		{	return mDepth;			}
	public Boolean isRedundant()	{	return isRedundant;		}
	public String getRackID()		{	return mRackID;			}
	public String getDescription()	{	return mDescription;	}
	public Boolean isInstalled()	{	return isInstalled;		}
	public Rack getRack() {
		return Rack.getRack(mRackID);
	}

	public void setID(String id)					{	mID				= id;			}
	public void setCurrent(Boolean current)			{	mCurrent		= current;		}
	public void setPhase(Integer phase)				{	mPhase			= phase;		}
	public void setHeat(Double heat)				{	mHeat			= heat;			}
	public void setPower(Double power)				{	mPower			= power;		}
	public void setWeight(Double weight)			{	mWeight			= weight;		}
	public void setHeight(Double height)			{	mHeight			= height;		}
	public void setWidth(Double width)				{	mWidth			= width;		}
	public void setDepth(Double depth)				{	mDepth			= depth;		}
	public void setRedundant(Boolean redundant)		{	isRedundant		= redundant;	}
	public void setRackID(String rackID)			{	mRackID			= rackID;		}
	public void setDescription(String description)	{	mDescription	= description;	}
	public void setInstalled(Boolean installed)		{	isInstalled		= installed;	}
	
	private String mID;
	private Boolean mCurrent;
	private Integer mPhase;
	private Double mHeat;
	private Double mPower;
	private Double mWeight;
	private Double mHeight;
	private Double mWidth;
	private Double mDepth;
	private Boolean isRedundant;
	private String mRackID;
	private String mDescription;
	private Boolean isInstalled;
}
