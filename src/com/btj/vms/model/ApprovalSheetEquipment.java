package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class ApprovalSheetEquipment {
	/**
	 * Minimalist ApprovalSheet constructor (for create)
	 */
	public ApprovalSheetEquipment() {
		this("","","");
	}
	
	/**
	 * default constructorfor DB
	 * @param workingpermitID
	 * @param equipmentID
	 * @param inout
	 */
	public ApprovalSheetEquipment(String workingpermitID, String equipmentID, String inout) {
		mWorkingPermitID = workingpermitID;
		mEquipmentID = equipmentID;
		mINOUT = inout;
	}
	
	/**
	 * get all ApprovalSheetEquipment from DB
	 * @return
	 */
	public static ApprovalSheetEquipment[] getAllApprovalSheetEquipments() {
		DBQueryExecutor executor = new DBQueryExecutor();

		ApprovalSheetEquipment[] retval = new ApprovalSheetEquipment[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.APPROVALSHEET_EQUIPMENT));
			if (result != null) {
				ArrayList<ApprovalSheetEquipment> array = new ArrayList<ApprovalSheetEquipment>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new ApprovalSheetEquipment[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a ApprovalSheetEquipment from DB with specified WorkingPermitID
	 * @param specified WorkingPermitID
	 * @return
	 */
	public static ApprovalSheetEquipment[] getApprovalSheetEquipmentFromWorkingPermit(String workingPermitID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<ApprovalSheetEquipment> retval = new ArrayList<ApprovalSheetEquipment>();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `workingPermitID` = '%s';", DBTable.APPROVALSHEET_EQUIPMENT, workingPermitID));
			if (result != null) {
				while (result.next()) {
					retval.add(getModelFromResultSet(result));
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return (ApprovalSheetEquipment[]) retval.toArray();
	}
	
	/**
	 * get a ApprovalSheetEquipment from DB with specified EquipmentID
	 * @param specified EquipmentID
	 * @return
	 */
	public static ApprovalSheetEquipment[] getApprovalSheetEquipmentFromEquipment(String equipmentID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<ApprovalSheetEquipment> retval = new ArrayList<ApprovalSheetEquipment>();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `equipmentID` = '%s';", DBTable.APPROVALSHEET_EQUIPMENT, equipmentID));
			if (result != null) {
				while (result.next()) {
					retval.add(getModelFromResultSet(result));
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return (ApprovalSheetEquipment[]) retval.toArray();
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static ApprovalSheetEquipment[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ApprovalSheetEquipment[] retval = new ApprovalSheetEquipment[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"workingPermitID", "equipmentID", "inout"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.APPROVALSHEET_EQUIPMENT, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<ApprovalSheetEquipment> array = new ArrayList<ApprovalSheetEquipment>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new ApprovalSheetEquipment[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.APPROVALSHEET_EQUIPMENT));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static ApprovalSheetEquipment getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("workingPermitID");
		String equipmentID 	= result.getString("equipmentID");
		String inout		= result.getString("inout");
		return new ApprovalSheetEquipment(id, equipmentID, inout);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`workingPermitID`, `equipmentID`, `inout`) VALUES ('%s', '%s', '%s');"
				, DBTable.APPROVALSHEET_EQUIPMENT, mWorkingPermitID, mEquipmentID, mINOUT);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE (`workingPermitApprovalID` = '%s' AND `equipmentID` = '%s');", DBTable.APPROVALSHEET_EQUIPMENT, mWorkingPermitID, mEquipmentID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `equipmentID` = '%s', `inout` = '%s' WHERE `workingPermitApprovalID` = '%s';"
				, DBTable.APPROVALSHEET_EQUIPMENT, mEquipmentID, mINOUT, mWorkingPermitID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	private String mWorkingPermitID;
	private String mEquipmentID;
	private String mINOUT;
	
	public String getWorkingPermitID()		{	return mWorkingPermitID;	}
	public String getEquipmentID()		 	{	return mEquipmentID;		}
	public String getInOut()			 	{	return mINOUT;				}
	public WorkingPermit getWorkingPermit()	{
		return WorkingPermit.getWorkingPermit(mWorkingPermitID);
	}
	public Equipment getEquipment()	{
		return Equipment.getEquipment(mEquipmentID);
	}
	
	public void setWorkingPermitID(String workingpermitID)	{	mWorkingPermitID = workingpermitID;	}
	public void setEquipmentID(String equipmentID)			{	mEquipmentID = equipmentID;			}
	public void setInOut(String inout)						{	mINOUT = inout;						}
}
