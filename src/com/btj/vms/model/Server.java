package com.btj.vms.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

import com.btj.vms.helper.IOUtilities;

public class Server {
	private Server(String configurationFilePath) {	
		readFromJSONString(IOUtilities.readFileAsString(configurationFilePath));
		mConfigurationFilePath = configurationFilePath;
	}

	/**
	 * Get instance of a server with given directory
	 * Note that this instance will be saved as singleton object
	 * @param directory. Have to be absolute path. Use request.getRealPath() to get absolute path
	 * @return server of that directory
	 */
	public static Server instance(String directory) {
		if (sServers == null) {
			sServers = new HashMap<String, Server>();
		}
		if (!sServers.containsKey(directory)) {
			sServers.put(directory, new Server(directory));
		}
		return sServers.get(directory);
	}

	/**
	 * Get first any instance of server
	 * @return
	 */
	public static Server instance() {
		if (sServers == null) {
			return null;
		}
		// return first available server
		for (Server server : sServers.values()) {
			return server;
		}
		return null;
	}

	/**
	 * Get instance of a server with given http request
	 * Note that this instance will be saved as singleton object
	 * @param request. Will use request.getRealPath and will use default config directory
	 * @return server given by caller
	 */
	public static Server instance(HttpServletRequest request) {
		return instance(request.getRealPath(DEFAULT_SERVER_PATH));
	}

	public void readFromJSONString(String jsonString) {
		try {
			JSONObject jsonobject = new JSONObject(jsonString);
			mServerID 			= jsonobject.getString(SERVER_ID);
			mIsInstalled 		= jsonobject.getBoolean(IS_INSTALLED);
			mIncrementalID 		= jsonobject.getInt(INCREMENTAL_ID);
			mMainServerIP		= jsonobject.getString(MAIN_SERVER_IP);
			mMainServerUser		= jsonobject.getString(MAIN_SERVER_DB_USER);
			mMainServerPassword	= jsonobject.getString(MAIN_SERVER_DB_PASS);

			JSONObject db_prefix = jsonobject.getJSONObject(DB_PREFIX);
			Iterator<String> keys = db_prefix.keys();
			
			mDBPrefix = new HashMap<String, String>();
			
			while(keys.hasNext()) {
				String key = keys.next();
				mDBPrefix.put(key, db_prefix.getString(key));
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String writeToJSONString() {
		try {
			JSONObject jsonobject = new JSONObject();
			jsonobject.put(SERVER_ID, mServerID);
			jsonobject.put(IS_INSTALLED, mIsInstalled);
			jsonobject.put(INCREMENTAL_ID, mIncrementalID);
			jsonobject.put(MAIN_SERVER_IP, mMainServerIP);
			jsonobject.put(MAIN_SERVER_DB_USER, mMainServerUser);
			jsonobject.put(MAIN_SERVER_DB_PASS, mMainServerPassword);

			JSONObject db_prefix = new JSONObject();
			for(String key: mDBPrefix.keySet()) {
				db_prefix.put(key, mDBPrefix.get(key));
			}
			
			jsonobject.put(DB_PREFIX, db_prefix);
			
			return jsonobject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	public String generateID(String DB) {
		String id = String.format("%s%s%05d", mDBPrefix.get(DB), mServerID, mIncrementalID++);
		
		//update server JSON config
		IOUtilities.writeToFileAsString(mConfigurationFilePath, writeToJSONString());
		
		return id;
	}
	
	public String generateID(String DB, String buildingID) {
		BuildingIDGenerator big = BuildingIDGenerator.getBuildingIDGenerator(buildingID);
		String id = String.format("%s_%03d", big.getBuildingID(), big.getDBCounter(DB));
		return id;
	}
	
	public String getServerID()			{	return mServerID;			}
	public String getMainServerIP()		{	return mMainServerIP;		}
	public String getMainServerUser()	{	return mMainServerUser;		}
	public String getMainServerPass()	{	return mMainServerPassword;	}
	public Boolean getIsInstalled() 	{	return mIsInstalled;		}
	
	/**
	 * for now, main server IP is dynamic, so we want to be able to edit it somewhere else
	 * When editing serverIP, save it to file
	 * @param serverIP
	 */
	public void setMainServerIP(String serverIP) {
		mMainServerIP = serverIP;
		//update server JSON config
		IOUtilities.writeToFileAsString(mConfigurationFilePath, writeToJSONString());
	}

	private String mModelID;
	private String mServerID;
	private String mMainServerIP, mMainServerUser, mMainServerPassword;
	private Boolean mIsInstalled;
	private Integer mIncrementalID;
	private HashMap<String, String> mDBPrefix;	// use DBTable static class for key
	
	private String mConfigurationFilePath;
	
	// singleton
	private static HashMap<String, Server> sServers;

	// default server file position
	public static String DEFAULT_SERVER_PATH = "server_configuration.btj";

	// keys
	private static String SERVER_ID 			= "ServerID";
	private static String MAIN_SERVER_IP 		= "MainServerIP";
	private static String MAIN_SERVER_DB_USER 	= "MainServerDBUser";
	private static String MAIN_SERVER_DB_PASS 	= "MainServerDBPassword";
	private static String IS_INSTALLED 			= "IsInstalled";
	private static String INCREMENTAL_ID		= "IncrementalID";
	private static String DB_PREFIX				= "DBPrefix";
}
