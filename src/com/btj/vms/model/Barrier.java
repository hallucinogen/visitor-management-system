package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Barrier {
	/**
	 * Minimalist Barrier constructor (for create)
	 */
	public Barrier() {
		this("","", false);
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param levelID
	 * @param isPublic
	 */
	public Barrier(String id, String levelID, boolean isPublic) {
		mID 		= id;
		mLevelID 	= levelID;
		mPublic		= isPublic;
	}

	/**
	 * get all Barrier from DB
	 * @return
	 */
	public static Barrier[] getAllBarriers() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Barrier[] retval = new Barrier[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.BARRIER));
			if (result != null) {
				ArrayList<Barrier> array = new ArrayList<Barrier>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Barrier[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Barrier from DB
	 * @param id specified Barrier
	 * @return
	 */
	public static Barrier getBarrier(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Barrier retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `barrierID` = '%s';", DBTable.BARRIER, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all Barrier from Level
	 * @param levelID
	 * @return
	 */
	public static Barrier[] getAllBarrierLevel(String levelID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Barrier[] retval = new Barrier[0];
		String stmt = String.format("SELECT * FROM `%s` WHERE `levelID` = '%s';", DBTable.BARRIER, levelID);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Barrier> array = new ArrayList<Barrier>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Barrier[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Barrier[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Barrier[] retval = new Barrier[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"barrierID", "levelID", "public"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.BARRIER, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Barrier> array = new ArrayList<Barrier>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Barrier[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.BARRIER));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Barrier getModelFromResultSet(ResultSet result) throws SQLException {
		String id 		= result.getString("barrierID");
		String levelID 	= result.getString("levelID");
		boolean isPub	= result.getInt("public") != 0;
		return new Barrier(id, levelID, isPub);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`barrierID`, `levelID`, `public`) VALUES ('%s', '%s', '%d');"
				, DBTable.BARRIER, mID, mLevelID, mPublic ? 1 : 0);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `barrierID` = '%s';", DBTable.BARRIER, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `levelID` = '%s', `public` = '%d'  WHERE `barrierID` = '%s';"
				, DBTable.BARRIER, mLevelID, mPublic ? 1 : 0, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()				{	return mID;			}
	public String getLevelID()			{	return mLevelID;	}
	public boolean isPublic()			{	return mPublic;		}

	public Level getLevel() {
		return Level.getLevel(mLevelID);
	}

	public void setID(String id)			{	mID = id;			}
	public void setLevelID(String levelID)	{	mLevelID = levelID;	}
	public void setIsPublic(boolean isPub)	{	mPublic = isPub;	}

	private String mID;
	private String mLevelID;
	private boolean mPublic;
}
