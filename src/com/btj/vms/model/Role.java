package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Role {
	/**
	 * Minimalist Role constructor (for create)
	 */
	public Role() {
		this("","");
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param name
	 */
	public Role(String id, String name) {
		mID = id;
		mName = name;

		mPageURLs = getAllRolePages(mID);
	}
	
	/**
	 * get all Roles from DB
	 * @return
	 */
	public static Role[] getAllRoles() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Role[] retval = new Role[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.ROLE));
			if (result != null) {
				ArrayList<Role> array = new ArrayList<Role>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Role[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get all Role_page from DB with specified Role ID
	 * @return
	 */
	public static ArrayList<String> getAllRolePages(String roleID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<String> retval = new ArrayList<String>();

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `roleID` = '%s';", DBTable.ROLE_PAGE, roleID));
			if (result != null) {
				while (result.next()) {
					// Database is 1-base-indexed
					String page = result.getString("pageURL");
					retval.add(page);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	public static boolean getRoleAccess(String roleID, String pageURL) {
		DBQueryExecutor executor = new DBQueryExecutor();

		boolean retval = false;

		String stmt = String.format("SELECT * FROM `%s` WHERE `roleID` = '%s' AND `pageURL` = '%s';", DBTable.ROLE_PAGE, roleID, pageURL);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				retval = false;
				while (result.next()) {
					retval = true;
				}
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get a group from DB
	 * @param id specified group
	 * @return
	 */
	public static Role getRole(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Role retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `roleID` = '%s';", DBTable.ROLE, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Role[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Role[] retval = new Role[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"roleID", "name"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.ROLE, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Role> array = new ArrayList<Role>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Role[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.ROLE));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return finalRetval;
	}

	protected static Role getModelFromResultSet(ResultSet result) throws SQLException {
		String id = result.getString("roleID");
		String name = result.getString("name");
		return new Role(id, name);
	}
	
	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`roleID`, `name`) VALUES ('%s', '%s');"
				, DBTable.ROLE, mID, mName);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void addRolePage(String pageURL){
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`roleID`, `pageURL`) VALUES ('%s', '%s');"
				, DBTable.ROLE_PAGE, mID, pageURL);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `roleID` = '%s';", DBTable.ROLE, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `name` = '%s' WHERE `roleID` = '%s';"
				, DBTable.ROLE, mName, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void deleteAllRolePage(){
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("DELETE FROM `%s` WHERE `roleID` = '%s';", DBTable.ROLE_PAGE, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public String getID()				{	return mID;			}
	public String getName()				{	return mName;		}
	public ArrayList<String> getRolePageURLs()	{	return mPageURLs;	}
	
	public void setID(String id)		{	mID = id;		}
	public void setName(String name)	{	mName = name;	}
	
	private String mID;
	private String mName;
	private ArrayList<String> mPageURLs;
}
