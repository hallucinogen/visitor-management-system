package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Room {
	/**
	 * Minimalist Room constructor (for create)
	 */
	public Room() {
		this("","","",0.0);
	}

	/**
	 * default constructor for and getting from DB
	 * @param id
	 * @param levelID
	 */
	public Room(String id, String name, String levelID, double power) {
		mID 		= id;
		mName		= name;
		mLevelID 	= levelID;
		mPower		= power;
	}


	/**
	 * get all Room from DB
	 * @return
	 */
	public static Room[] getAllRooms() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Room[] retval = new Room[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.ROOM));
			if (result != null) {
				ArrayList<Room> array = new ArrayList<Room>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Room[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Room from DB
	 * @param id specified Room
	 * @return
	 */
	public static Room getRoom(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Room retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `roomID` = '%s';", DBTable.ROOM, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all Room from Level
	 * @param levelID
	 * @return
	 */
	public static Room[] getAllRoomLevel(String levelID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Room[] retval = new Room[0];
		String stmt = String.format("SELECT * FROM `%s` WHERE `levelID` = '%s';", DBTable.ROOM, levelID);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Room> array = new ArrayList<Room>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Room[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Room[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Room[] retval = new Room[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"roomID", "name", "levelID", "power"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.ROOM, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Room> array = new ArrayList<Room>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Room[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.ROOM));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Room getModelFromResultSet(ResultSet result) throws SQLException {
		String id 		= result.getString("roomID");
		String name 	= result.getString("name");
		String levelID 	= result.getString("levelID");
		Double power 	= result.getDouble("power");
		return new Room(id, name, levelID, power);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`roomID`, `name`, `levelID`, `power`) VALUES ('%s', '%s', '%s', '%f');"
				, DBTable.ROOM, mID, mName, mLevelID, mPower);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `roomID` = '%s';", DBTable.ROOM, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `name` = '%s', `levelID` = '%s', `power` = '%f' WHERE `roomID` = '%s';"
				, DBTable.ROOM, mName, mLevelID, mPower, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getID()				{	return mID;			}
	public String getName()				{	return mName;		}
	public String getLevelID()			{	return mLevelID;	}
	public double getPower()			{	return mPower;		}

	public Level getLevel() {
		if (mLevel == null) {
			mLevel = Level.getLevel(mLevelID);
		}
		return mLevel;
	}

	public void setID(String id)			{	mID = id;			}
	public void setName(String name)		{	mName = name;		}
	public void setLevelID(String levelID)	{	mLevelID = levelID;	}
	public void setPower(double power)		{	mPower = power;		}

	private String mID;
	private String mName;
	private String mLevelID;
	private double mPower;

	private Level mLevel;
}
