package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class ApprovalSheet {
	/**
	 * Minimalist ApprovalSheet constructor (for create)
	 */
	public ApprovalSheet() {
		this("","","");
	}
	
	/**
	 * default constructor for DB
	 * @param workingpermitID
	 * @param project
	 * @param vendor
	 */
	public ApprovalSheet(String workingpermitID, String project, String vendor) {
		mWorkingPermitID = workingpermitID;
		mProject = project;
		mVendor = vendor;
		mASE = ApprovalSheetEquipment.getApprovalSheetEquipmentFromWorkingPermit(workingpermitID);
	}
	
	/**
	 * get all ApprovalSheet from DB
	 * @return
	 */
	public static ApprovalSheet[] getAllApprovals() {
		DBQueryExecutor executor = new DBQueryExecutor();

		ApprovalSheet[] retval = new ApprovalSheet[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.APPROVALSHEET));
			if (result != null) {
				ArrayList<ApprovalSheet> array = new ArrayList<ApprovalSheet>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new ApprovalSheet[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a ApprovalSheet from DB with specified WorkingPermitID
	 * @param specified WorkingPermitID
	 * @return
	 */
	public static ApprovalSheet[] getApprovalSheet(String workingPermitID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<ApprovalSheet> retval = new ArrayList<ApprovalSheet>();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `workingPermitID` = '%s';", DBTable.APPROVALSHEET, workingPermitID));
			if (result != null) {
				while (result.next()) {
					retval.add(getModelFromResultSet(result));
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return (ApprovalSheet[]) retval.toArray();
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static ApprovalSheet[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ApprovalSheet[] retval = new ApprovalSheet[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"workingPermitID", "vendor", "project"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.APPROVALSHEET, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<ApprovalSheet> array = new ArrayList<ApprovalSheet>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new ApprovalSheet[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.APPROVALSHEET));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static ApprovalSheet getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("workingPermitID");
		String vendor 		= result.getString("vendor");
		String project 		= result.getString("project");
		return new ApprovalSheet(id, project, vendor);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`workingPermitID`, `vendor`, `project`) VALUES ('%s', '%s', '%s');"
				, DBTable.APPROVALSHEET, mWorkingPermitID, mVendor, mProject);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `workingPermitApprovalID` = '%s';", DBTable.APPROVALSHEET, mWorkingPermitID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `vendor` = '%s', `project` = '%s' WHERE `workingPermitApprovalID` = '%s';"
				, DBTable.APPROVALSHEET, mWorkingPermitID, mVendor, mProject);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public WorkingPermit getWorkingPermit() {
		return WorkingPermit.getWorkingPermit(mWorkingPermitID);
	}
	
	private String mWorkingPermitID;
	private String mProject;
	private String mVendor;
	private ApprovalSheetEquipment[] mASE;
	
	public String getWorkingPermitID() {
		return mWorkingPermitID;
	}
	public String getProject() {
		return mProject;
	}
	public String getVendor() {
		return mVendor;
	}
	public ApprovalSheetEquipment[] getApprovalSheetEquipments() {
		return mASE;
	}
	
	public void setWorkingPermitID(String WorkingPermitID) {
		this.mWorkingPermitID = WorkingPermitID;
	}
	public void setProject(String Project) {
		this.mProject = Project;
	}
	public void setVendor(String Vendor) {
		this.mVendor = Vendor;
	}
}
