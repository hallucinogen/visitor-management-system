package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Rack {
	/**
	 * Minimalist Rack constructor (for create)
	 */
	public Rack() {
		this("", "", 0);
	}
	
	/**
	 * default constructor for and getting from DB
	 * @param ID
	 * @param Room
	 * @param rackCapacity
	 */
	public Rack(String ID, String RoomID, Integer capacity) {
		mID = ID;
		mRoomID = RoomID;
		mCapacity = capacity;
	}
	
	/**
	 * get all Rack from DB
	 * @return
	 */
	public static Rack[] getAllRooms() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Rack[] retval = new Rack[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.RACK));
			if (result != null) {
				ArrayList<Rack> array = new ArrayList<Rack>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Rack[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Rack from DB
	 * @param id specified Rack
	 * @return
	 */
	public static Rack getRack(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Rack retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `rackID` = '%s';", DBTable.RACK, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get all Rack from Room
	 * @param roomID
	 * @return
	 */
	public static Rack[] getAllRackRoom(String roomID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Rack[] retval = new Rack[0];
		String stmt = String.format("SELECT * FROM `%s` WHERE `roomID` = '%s';", DBTable.RACK, roomID);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Rack> array = new ArrayList<Rack>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Rack[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Rack[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Rack[] retval = new Rack[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"rackID", "roomID", "capacity"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.RACK, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Rack> array = new ArrayList<Rack>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Rack[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.RACK));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}
	
	/**
	 * get remaining capacity from DB
	 */
	public int getRemainingCapacity() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = this.getCapacity();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s` WHERE `rackID` = '%s';", DBTable.EQUIPMENT, mID));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				result.close();
				finalRetval = this.getCapacity()-retval;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}
	
	protected static Rack getModelFromResultSet(ResultSet result) throws SQLException {
		String id 				= result.getString("rackID");
		String RoomID 			= result.getString("roomID");
		Integer rackCapacity	= result.getInt("capacity");
		return new Rack(id, RoomID, rackCapacity);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`rackID`, `roomID`, `capacity`) VALUES ('%s', '%s', '%d');"
				, DBTable.RACK, mID, mRoomID, mCapacity);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `rackID` = '%s';", DBTable.RACK, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `capacity` = '%d', `roomID` = '%s'  WHERE `rackID` = '%s';"
				, DBTable.RACK, mCapacity, mRoomID, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public String getID()				{	return mID;			}
	public String getRoomID()				{	return mRoomID;		}
	public Integer getCapacity()		{	return mCapacity;	}

	public Room getRoom() {
		return Room.getRoom(mRoomID);
	}

	public void setID(String id)						{	mID = id;				}
	public void setRoomID(String RoomID)					{	mRoomID = RoomID;		}
	public void setCapacity(Integer Capacity)			{	mCapacity = Capacity;	}
	
	private String mID;
	private String mRoomID;
	private Integer mCapacity;
}
