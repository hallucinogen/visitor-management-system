package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class WorkingPermit {
	/**
	 * Minimalist WorkingPermit constructor (for create)
	 */
	public WorkingPermit() {
		this("","","","");
	}

	/**
	 * default constructor for getting from DB
	 * @param id
	 * @param dateFrom
	 * @param visitorID
	 * @param dateTo
	 */
	public WorkingPermit(String id, String visitorID, String dateFrom, String dateTo) {
		mID						= id;
		mVisitorID				= visitorID;
		mDateFrom				= dateFrom;
		mDateTo					= dateTo;
		mApproval 				= Approval.getApprovalFromWorkingPermit(mID);
	}


	/**
	 * get all WorkingPermit from DB
	 * @return
	 */
	public static WorkingPermit[] getAllWorkingPermits() {
		DBQueryExecutor executor = new DBQueryExecutor();

		WorkingPermit[] retval = new WorkingPermit[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.WORKING_PERMIT));
			if (result != null) {
				ArrayList<WorkingPermit> array = new ArrayList<WorkingPermit>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new WorkingPermit[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a WorkingPermit from DB
	 * @param id specified WorkingPermit
	 * @return
	 */
	public static WorkingPermit getWorkingPermit(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		WorkingPermit retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `workingPermitID` = '%s';", DBTable.WORKING_PERMIT, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static WorkingPermit[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		WorkingPermit[] retval = new WorkingPermit[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"workingPermitID", "datefrom", "dateto", "vendor", "project", "username"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.WORKING_PERMIT, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<WorkingPermit> array = new ArrayList<WorkingPermit>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new WorkingPermit[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.WORKING_PERMIT));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static WorkingPermit getModelFromResultSet(ResultSet result) throws SQLException {
		String id 		= result.getString("workingPermitID");
		String visitorID= result.getString("visitorID");
		String dateFrom = result.getString("datefrom");
		String dateTo 	= result.getString("dateto");
		return new WorkingPermit(id, visitorID, dateFrom, dateTo);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`workingPermitID`, `visitorID`, `datefrom`, `dateto`) VALUES ('%s', '%s', '%s', '%s');"
				, DBTable.WORKING_PERMIT, mID, mVisitorID, mDateFrom, mDateTo);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `workingPermitID` = '%s';", DBTable.WORKING_PERMIT, mID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `visitorID` = '%s', `datefrom` = '%s', `dateto` = '%s'" +
				" WHERE `workingPermitID` = '%s';"
				, DBTable.WORKING_PERMIT, mVisitorID, mDateFrom, mDateTo, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public Boolean isApproved() {
		for (int i = 0; i < mApproval.length; i++) {
			if(!mApproval[i].getApproved())
				return false;
		}
		return true;
	}
	
	public String getID() 		{	return mID;			}
	public String getVisitorID(){	return mVisitorID;	}
	public String getDateFrom()	{	return mDateFrom;	}
	public String getDateTo()	{	return mDateTo;		}
	public Approval[] getApproval()	{	return mApproval;	}
	
	public void setID(String iD)				{	mID = iD;				}
	public void setVisitorID(String visitorID)	{	mVisitorID = visitorID;	}
	public void setDateFrom(String dateFrom)	{	mDateFrom = dateFrom;	}
	public void setDateTo(String dateTo)		{	mDateTo = dateTo;		}

	private String mID;
	private String mVisitorID;
	private String mDateFrom;
	private String mDateTo;
	private Approval[] mApproval;	
}
