package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Visit {
	/**
	 * Minimalist Visit constructor (for create)
	 */
	public Visit(){
		this("", "", "", false, "");
	}
	
	/**
	 * default constructor for getting from DB
	 * @param id
	 * @param time
	 * @param visitorID
	 * @param activity
	 * @param roomID
	 */
	public Visit (String id, String time, String visitorID, Boolean activity, String roomID) {
		mID			= id;
		mTime		= time;
		mVisitorID	= visitorID;
		mActivity	= activity;
		mRoomID		= roomID;
	}
	
	/**
	 * get all Visit from DB
	 * @return
	 */
	public static Visit[] getAllVisits() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visit[] retval = new Visit[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.VISIT));
			if (result != null) {
				ArrayList<Visit> array = new ArrayList<Visit>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Visit[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get all Visit from spesific Visitor
	 * @param id
	 * @return
	 */
	public static Visit[] getAllVisitorVisits(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visit[] retval = new Visit[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `visitorID` = '%s';", DBTable.VISIT,id));
			if (result != null) {
				ArrayList<Visit> array = new ArrayList<Visit>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Visit[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get a Visit from DB
	 * @param id
	 * @return
	 */
	public static Visit getVisit(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visit retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `visitID` = '%s';", DBTable.VISIT, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @param activity
	 * @return
	 */
	public static Visit[] getSortedLimitedModel(int column, int start, int mode, int activity) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Visit[] retval = new Visit[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"visitID", "time", "visitorID", "activity", "roomID"};
		String stmt = null;
		if(activity==2) stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.VISIT, category[column], orderMode, start);
		else stmt = String.format("SELECT * FROM `%s` WHERE `activity` = '%d' ORDER BY `%s` %s LIMIT %d, 20;", DBTable.VISIT, activity, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Visit> array = new ArrayList<Visit>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Visit[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 * @param activity
	 */
	public static int getTotalPage(int activity) {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = null;
		if(activity == 2) stmt = String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.VISIT);
		else stmt = String.format("SELECT COUNT(*) AS totalRow FROM `%s` WHERE `activity` = '%d';", DBTable.VISIT, activity);
		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Visit getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("visitID");
		String time 		= result.getString("time");
		String visitorID	= result.getString("visitorID");
		Boolean activity	= result.getInt("activity") != 0;
		String roomID		= result.getString("roomID");
		return new Visit(id, time, visitorID, activity, roomID);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`visitID`, `time`, `visitorID`, `activity`, `roomID`) VALUES ('%s', CURRENT_TIMESTAMP(), '%s', '%d', '%s');"
				, DBTable.VISIT, mID, mVisitorID, mActivity ? 1 : 0, mRoomID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `visitID` = '%s';", DBTable.VISIT, mTime);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `time` = '%s', `visitorID` = '%s', `activity` = '%d', `roomID` = '%s' WHERE `visitID` = '%s';"
				, DBTable.VISIT, mTime, mVisitorID, mActivity ? 1 : 0, mRoomID, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public String getID() 				{	return mID;			}
	public String getTime()				{	return mTime;		}
	public String getVisitorID() 		{	return mID;			}
	public Boolean getActivity() 		{	return mActivity;	}
	public String getRoomID()			{	return mRoomID;		}
	
	public void setID(String id)		 		{	mID			= id;			}
	public void setTime(String time)		 	{	mTime		= time;			}
	public void setVisitorID(String visitorID)	{	mVisitorID	= visitorID;	}
	public void setActivity(Boolean activity)	{	mActivity	= activity;		}
	public void setRoomID(String roomID)		{	mRoomID		= roomID;		}
	
	private String mID;
	private String mTime;
	private String mVisitorID;
	private Boolean mActivity;
	private String mRoomID;
}
