package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.codec.digest.DigestUtils;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class User {
	/**
	 * Minimalist User constructor (for create)
	 */
	public User(){
		this("", "");
	}

	/**
	 * User constructor for login
	 * @param username
	 * @param password
	 */
	public User(String username, String password) {
		this(username, password, "", "","","","","");
	}

	/**
	 * default constructor for DB
	 * @param username
	 * @param password
	 * @param name
	 * @param role
	 * @param department
	 * @param title
	 * @param mobile
	 * @param email
	 */
	public User(String username, String password, String name, String role, String department, String title, String mobile, String email) {
		mUsername = username;
		mName = name;
		mRoleID = role;
		mPassword = DigestUtils.md5Hex(password);
		mDepartment = department;
		mTitle = title;
		mMobile = mobile;
		mEmail = email;
		
		setRole();
	}

	/**
	 * Authenticate user, try checking with database
	 * @return true if the user is authenticated by database
	 */
	public boolean authenticate() {
		ResultSet rs = null;
		DBQueryExecutor executor = new DBQueryExecutor();

		boolean retval = false;

		try {
			rs = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `username` = '%s';", DBTable.USER, mUsername));
			if(rs != null) {
				if(!rs.next()) {
					// username not found
					retval = false;
				} else if(mPassword.equals(rs.getString("password"))) {
					//success
					mRoleID = rs.getString("roleID");
					setRole();
					mName = rs.getString("name");

					retval = true;
				}

				rs.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		//password not match
		return retval;
	}
	
	/**
	 * get all user from DB
	 * @return
	 */
	public static User[] getAllUser() {
		DBQueryExecutor executor = new DBQueryExecutor();

		User[] retval = new User[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.USER));
			if (result != null) {
				ArrayList<User> array = new ArrayList<User>();
				while (result.next()) {
					try {
						array.add(getModelFromResultSet(result));
					} catch (SQLException sEx) {
						// if exception occur in one record, don't let it affect anything else
					}
				}

				retval = new User[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get a user from DB
	 * @param id specified user
	 * @return
	 */
	public static User getUser(String id){
		DBQueryExecutor executor = new DBQueryExecutor();

		User retval = null;

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `username` = '%s';", DBTable.USER, id));
			if (result != null) {
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	public static User[] getUserFromQuery(String query){
		DBQueryExecutor executor = new DBQueryExecutor();
		User[] retval = new User[0];

		try {
			// we can't use String.format here
			ResultSet result = executor.executeQuery("SELECT * FROM `" + DBTable.USER + "` WHERE `name` LIKE '%" + query + "%';");
			if (result != null) {
				ArrayList<User> array = new ArrayList<User>();
				while (result.next()) {
					try {
						array.add(getModelFromResultSet(result));
					} catch (SQLException sEx) {
						// if exception occur in one record, don't let it affect anything else
					}
				}

				retval = new User[array.size()];
				array.toArray(retval);

				// don't forget to close query
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static User[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		User[] retval = new User[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"username", "name", "department", "title", "mobile", "email", "roleID"};
		String stmt;
		if(column==2)
			stmt = String.format("SELECT * FROM `%s` LEFT JOIN `%s` ON `%s`.`role`=`%s`.`roleID` ORDER BY `%s`.`%s` %s LIMIT %d, 20;",
								  DBTable.USER, DBTable.ROLE, DBTable.USER, DBTable.ROLE, DBTable.ROLE, "name", orderMode, start);
		else
			stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.USER, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<User> array = new ArrayList<User>();
				while (result.next()) {
					try {
						array.add(getModelFromResultSet(result));
					} catch (SQLException sEx) {
						// if exception occur in one record, don't let it affect anything else
					}
				}

				retval = new User[array.size()];
				array.toArray(retval);

				// don't forget to close query
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}
	
	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();
		
		int finalRetval = 1;

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.USER));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close query
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return finalRetval;
	}

	protected static User getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("username");
		String name 		= result.getString("name");
		String role 		= result.getString("roleID");
		String password 	= result.getString("password");
		String department 	= result.getString("department");
		String title 		= result.getString("title");
		String mobile 		= result.getString("mobile");
		String email	 	= result.getString("email");
		return new User(id, password, name, role, department, title, mobile, email);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s` " +
				"(`username`, `name`, `roleID`, `password`, `department`, `title`, `email`, `mobile`) " +
				"VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');"
				, DBTable.USER, mUsername, mName, mRoleID, mPassword, mDepartment, mTitle, mEmail, mMobile);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("DELETE FROM `%s` WHERE `username` = '%s';",
				DBTable.USER, mUsername);
		try {
			executor.executeQuery(stmt);
			executor.closeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s` SET " +
				"`name` = '%s', `roleID` = '%s', `password` = '%s', `department` = '%s', `title` = '%s', " +
				"`mobile` = '%s', `email` = '%s' WHERE `username` = '%s';"
				, DBTable.USER, mName, mRoleID, mPassword, mDepartment, mTitle, mMobile, mEmail, mUsername);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		User user = (User) obj;
		return mUsername.equals(user.getUsername());
	}
	
	public String getUsername()		{	return mUsername;	}
	public String getName()			{	return mName;		}
	public String getRoleID()		{	return mRoleID;		}
	public String getDepartment()	{	return mDepartment;	}
	public String getTitle()		{	return mTitle;		}
	public String getMobile()		{	return mMobile;		}
	public String getEmail()		{	return mEmail;		}
	public Role   getRole()			{	return mRole;		}
	
	public void setUsername(String username)	{	mUsername = username;						}
	public void setName(String name)			{	mName = name;								}
	public void setPassword(String password)	{	mPassword = DigestUtils.md5Hex(password);	}
	public void setRole(String role)			{	mRoleID = role;								}
	public void setDepartment(String department){	mDepartment = department;					}
	public void setTitle(String title)			{	mTitle = title;								}
	public void setMobile(String mobile)		{	mMobile = mobile;							}
	public void setEmail(String email)			{	mEmail = email;								}
	public void setRole(){
		if(mUsername != null){
			mRole = Role.getRole(mRoleID);
		}
	}

	private String mUsername;
	private String mName;
	private String mPassword;
	private String mRoleID;
	private String mDepartment;
	private String mTitle;
	private String mMobile;
	private String mEmail;
	private Role   mRole;
}
