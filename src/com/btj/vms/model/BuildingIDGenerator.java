package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class BuildingIDGenerator {
	/**
	 * Minimalist BuildingIDGenerator constructor (for create)
	 */
	public BuildingIDGenerator() {
		this("", 0, 0, 0, 0);
	}

	/**
	 * default constructor for and getting from DB
	 * @param buildingID
	 * @param barrierCounter
	 * @param levelCounter
	 * @param rackCounter
	 * @param roomCounter
	 */
	public BuildingIDGenerator(String buildingID, Integer barrierCounter, Integer levelCounter, Integer rackCounter, Integer roomCounter) {
		mBuildingID = buildingID;
		mBarrierCounter = barrierCounter;
		mLevelCounter = levelCounter;
		mRackCounter = rackCounter;
		mRoomCounter = roomCounter;
	}
	
	/**
	 * get all BuildingIDGenerator from DB
	 * @return
	 */
	public static Building[] getAllBuildings() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Building[] retval = new Building[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.BUILDING_ID_GENERATOR));
			if (result != null) {
				ArrayList<BuildingIDGenerator> array = new ArrayList<BuildingIDGenerator>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Building[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a BuildingIDGenerator from DB
	 * @param id specified Building
	 * @return
	 */
	public static BuildingIDGenerator getBuildingIDGenerator(String id) {
		DBQueryExecutor executor = new DBQueryExecutor();

		BuildingIDGenerator retval = null;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `buildingID` = '%s';", DBTable.BUILDING_ID_GENERATOR, id));
			if (result != null) {
				retval = null;
				while (result.next()) {
					retval = getModelFromResultSet(result);
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return retval;
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Building[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Building[] retval = new Building[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"buildingID", "name", "location"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.BUILDING_ID_GENERATOR, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<BuildingIDGenerator> array = new ArrayList<BuildingIDGenerator>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Building[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.BUILDING_ID_GENERATOR));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static BuildingIDGenerator getModelFromResultSet(ResultSet result) throws SQLException {
		String id 		= result.getString("buildingID");
		Integer barrier 	= result.getInt("barrier");
		Integer level	= result.getInt("level");
		Integer rack		= result.getInt("rack");
		Integer room		= result.getInt("room");
		return new BuildingIDGenerator(id, barrier, level, rack, room);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`buildingID`, `barrier`, `level`, `rack`, `room`) VALUES ('%s', '%d', '%d', '%d', '%d');"
				, DBTable.BUILDING_ID_GENERATOR, mBuildingID, mBarrierCounter, mLevelCounter, mRackCounter, mRoomCounter);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE `buildingID` = '%s';", DBTable.BUILDING_ID_GENERATOR, mBuildingID);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `barrier` = '%s', `level` = '%s', `rack` = '%s', `room` = '%s'  WHERE `buildingID` = '%s';"
				, DBTable.BUILDING_ID_GENERATOR, mBarrierCounter, mLevelCounter, mRackCounter, mRoomCounter, mBuildingID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public String getBuildingID()			{	return mBuildingID;		}
	public Integer getDBCounter(String DBtable) {
		Integer ret = null;
		if(DBtable.equals(DBTable.BARRIER)) {
			ret = mBarrierCounter++;
		}
		else if(DBtable.equals(DBTable.LEVEL)) {
			ret = mLevelCounter++;
		}
		else if(DBtable.equals(DBTable.RACK)) {
			ret = mRackCounter++;
		}
		else if(DBtable.equals(DBTable.ROOM)) {
			ret = mRoomCounter++;
		}
		editOnDB();
		return ret;
	}
	
	public void SetBuildingID(String id) {	mBuildingID	= id;	}
	
	private String mBuildingID;
	private Integer mBarrierCounter;
	private Integer mLevelCounter;
	private Integer mRackCounter;
	private Integer mRoomCounter;
}
