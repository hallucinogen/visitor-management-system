package com.btj.vms.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.btj.vms.helper.DBQueryExecutor;
import com.btj.vms.helper.DBTable;

public class Approval {
	/**
	 * Minimalist Approval constructor (for create)
	 */
	public Approval() {
		this("", "", false, "");
	}
	
	/**
	 * default constructor for getting from DB
	 * @param workingPermitID
	 * @param username
	 * @param approved
	 * @param approvedDate
	 */
	public Approval(String workingPermitID, String username, Boolean approved, 
			String approvedDate) {
		mID				= workingPermitID;
		mUsername		= username;
		mApproved		= approved;
		mApprovedDate	= approvedDate;
	}
	
	/**
	 * get all Approval from DB
	 * @return
	 */
	public static Approval[] getAllApprovals() {
		DBQueryExecutor executor = new DBQueryExecutor();

		Approval[] retval = new Approval[0];

		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s`;", DBTable.APPROVAL));
			if (result != null) {
				ArrayList<Approval> array = new ArrayList<Approval>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Approval[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get a Approval from DB with specified WorkingPermitID
	 * @param specified WorkingPermitID
	 * @return
	 */
	public static Approval[] getApprovalFromWorkingPermit(String workingPermitID) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<Approval> retval = new ArrayList<Approval>();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `workingPermitID` = '%s';", DBTable.APPROVAL, workingPermitID));
			if (result != null) {
				while (result.next()) {
					retval.add(getModelFromResultSet(result));
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return (Approval[]) retval.toArray();
	}
	
	/**
	 * get a Approval from DB with specified Username
	 * @param specified Username
	 * @return
	 */
	public static Approval[] getApprovalFromUsername(String username) {
		DBQueryExecutor executor = new DBQueryExecutor();

		ArrayList<Approval> retval = new ArrayList<Approval>();
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT * FROM `%s` WHERE `username` = '%s';", DBTable.APPROVAL, username));
			if (result != null) {
				while (result.next()) {
					retval.add(getModelFromResultSet(result));
				}

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return (Approval[]) retval.toArray();
	}

	/**
	 * get sorted 20 model from DB
	 * @param column
	 * @param start
	 * @param mode
	 * @return
	 */
	public static Approval[] getSortedLimitedModel(int column, int start, int mode) {
		DBQueryExecutor executor = new DBQueryExecutor();

		Approval[] retval = new Approval[0];

		String orderMode = mode==0 ? "ASC" : "DESC";
		String[] category = {"workingPermitID", "username", "approved", "approvedDate"};
		String stmt = String.format("SELECT * FROM `%s` ORDER BY `%s` %s LIMIT %d, 20;", DBTable.APPROVAL, category[column], orderMode, start);
		try {
			ResultSet result = executor.executeQuery(stmt);
			if (result != null) {
				ArrayList<Approval> array = new ArrayList<Approval>();
				while (result.next()) {
					array.add(getModelFromResultSet(result));
				}

				retval = new Approval[array.size()];
				array.toArray(retval);

				// don't forget to close result
				result.close();
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
		
		return retval;
	}

	/**
	 * get total Page from DB
	 * 1 page = 20 rows
	 */
	public static int getTotalPage() {
		DBQueryExecutor executor = new DBQueryExecutor();

		int finalRetval = 1;
		try {
			ResultSet result = executor.executeQuery(String.format("SELECT COUNT(*) AS totalRow FROM `%s`;", DBTable.APPROVAL));
			if (result != null && result.next()) {
				int retval = result.getInt("totalRow");
				if(retval==0) ++retval;
				// don't forget to close result
				result.close();
				finalRetval = (retval+19)/20;
			}
		} catch (SQLException sEx) {
			sEx.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}

		return finalRetval;
	}

	protected static Approval getModelFromResultSet(ResultSet result) throws SQLException {
		String id 			= result.getString("workingPermitID");
		String username 	= result.getString("username");
		Boolean	approved	= (result.getInt("approved") == 0) ? false : true;
		String approvedDate	= result.getString("approvedDate");
		return new Approval(id, username, approved, approvedDate);
	}

	public void addOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("INSERT INTO `%s`" +
				" (`workingPermitID`, `username`, `approved`, `approvedDate`) VALUES ('%s', '%s', '%d', '%s');"
				, DBTable.APPROVAL, mID, mUsername, mApproved ? 1 : 0, mApprovedDate);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void deleteOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt;
		try {
			stmt = String.format("DELETE FROM `%s` WHERE (`workingPermitID` = '%s' AND `username` = '%s');", DBTable.APPROVAL, mID, mUsername);
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}

	public void editOnDB() {
		DBQueryExecutor executor = new DBQueryExecutor();
		String stmt = String.format("UPDATE `%s`" +
				" SET `username` = '%s', `approved` = '%d', `approvedDate` = '%s' WHERE `workingPermitID` = '%s';"
				, DBTable.APPROVAL, mUsername, mApproved ? 1 : 0, mApprovedDate, mID);
		try {
			executor.executeQuery(stmt);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				executor.closeQuery();
				executor.closeConnection();
			} catch (SQLException sEx) {
				sEx.printStackTrace();
			}
		}
	}
	
	public String getID()			{	return mID;				}
	public String getUsername()		{	return mUsername;		}
	public Boolean getApproved()	{	return mApproved;		}
	public String getApprovedDate()	{	return mApprovedDate;	}

	public void setID(String id)						{	mID = id;						}
	public void setUsername(String username)			{	mUsername = username;			}
	public void setApproved(Boolean approved)			{	mApproved = approved;			}
	public void setApprovedDate(String approvedDate)	{	mApprovedDate = approvedDate;	}

	private String mID;
	private String mUsername;
	private Boolean mApproved;
	private String mApprovedDate;	
}
