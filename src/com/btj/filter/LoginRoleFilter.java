package com.btj.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.btj.vms.model.Role;
import com.btj.vms.model.User;

/**
 * Servlet Filter implementation class LoginRoleFilter
 */
@WebFilter({"/visitor/*","/administration/*"})
public class LoginRoleFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginRoleFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// place your code here
		//System.out.println("filter");
		HttpServletRequest req		= (HttpServletRequest) request;
        HttpServletResponse resp	= (HttpServletResponse) response;
        HttpSession session 		= ((HttpServletRequest) request).getSession();
        User user = (User) session.getAttribute("User");

        //System.out.println(session);
        //System.out.println(req.getContextPath());
        
        if (session == null || user == null) {
            resp.sendRedirect(req.getContextPath() + "/index.jsp");
        } else {
        	String roleID			= user.getRoleID();
        	String pageURLToAccess	= req.getRequestURI();
        	String queryString		= req.getQueryString() != null ? req.getQueryString() : "";
        	
        	if(isAccessible(roleID, pageURLToAccess, queryString)){
        		chain.doFilter(request, response);
        	} else {
        		resp.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not authorized to do this action");	
        	}
        }
	}
	
	/**
	 * Determine whether a page (identified by address is accessible or not)
	 * 
	 * getServletName first
	 * if servletName with RoleID exist, return true else return false
	 * 
	 * @param roleID
	 * @param pageURLToAccess
	 * @return
	 */
	private boolean isAccessible(String roleID, String pageURLToAccess, String queryString){
		String pathInfo = getPathInfo(pageURLToAccess);

		//Special for news, we need to include the queryString if the action is update or create
		if(pathInfo.equals("/visitor/home") && queryString.indexOf("action=create")!=-1){
			pathInfo += "?" + queryString;
		}
		
		//System.out.println(pageURLToAccess + "#" + queryString + "#" + roleID + "#" + pathInfo);
		if(Role.getRoleAccess(roleID, pathInfo)){
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * requestURL: /VMS/news/home
	 * pathInfo: /news/home
	 * 
	 * requestURL: /VMS/administration/group
	 * pathInfo: /administration/group
	 */
	private String getPathInfo(String requestURL){
		int secondSlashPos	= requestURL.indexOf('/', 1);
		int questionPos		= requestURL.indexOf('?');
		
		return requestURL.substring(secondSlashPos, questionPos > -1 ? questionPos-1 : requestURL.length());
	}
	
	/**
	 * /news/home, servletName: home
	 * /administration/group, servletName: group
	 * /report, servletName: report
	 * 
	 * @param pathInfo
	 * @return
	 */
	/*private String getServletName(String pathInfo){
		int secondSlashPos	= pathInfo.indexOf('/', 1);
		
		if(secondSlashPos > -1){
			return pathInfo.substring(secondSlashPos+1, pathInfo.length());
		}else{
			return pathInfo.substring(1, pathInfo.length());
		}
	}*/

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}
}