package com.btj.helper.translate;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.btj.vms.helper.IOUtilities;

public class Translation {
	public Translation() {
		mCurrentLanguage = LANGUAGE_ENGLISH;

		mTranslationMap = new ArrayList<HashMap<String,String>>();

		// decode local JSON file
		String englishJSON 		= IOUtilities.readStreamAsString(getClass().getResourceAsStream("english.json"));
		String indonesiaJSON 	= IOUtilities.readStreamAsString(getClass().getResourceAsStream("indonesia.json"));

		String[] jsonToDecode = new String[] { englishJSON, indonesiaJSON };

		for (int i = 0; i < jsonToDecode.length; ++i) {
			HashMap<String, String> translation = new HashMap<String, String>();

			try {
				JSONObject json = new JSONObject(jsonToDecode[i]);

				String[] names = JSONObject.getNames(json);
				for (int j = 0; j < names.length; ++j) {
					translation.put(names[j], json.getString(names[j]));
				}
			} catch (JSONException jEx) {
				jEx.printStackTrace();
			}

			mTranslationMap.add(translation);
		}
	}

	public static String translate(String resourceName) {
		return translate(resourceName, instance().mCurrentLanguage);
	}

	public static String translate(String resourceName, int language) {
		String retval = instance().mTranslationMap.get(language).get(resourceName);

		return retval == null ? resourceName : retval;
	}

	public static void setLanguage(int language) {
		instance().mCurrentLanguage = language;
	}

	private static Translation instance() {
		if (sInstance == null) {
			sInstance = new Translation();
		}
		return sInstance;
	}

	private static Translation sInstance;

	public static int LANGUAGE_ENGLISH 		= 0;
	public static int LANGUAGE_INDONESIA 	= 1;

	private int mCurrentLanguage;
	private ArrayList<HashMap<String, String>> mTranslationMap;
}
