<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Server"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
    if("id".equals(session.getAttribute("lang"))) Translation.setLanguage(Translation.LANGUAGE_INDONESIA); else Translation.setLanguage(Translation.LANGUAGE_ENGLISH);
    
    ArrayList<String> rolePages = null; 
    User user = (User) session.getAttribute("User");
    String name = "";
    String id = "";
    if(user != null){
        name    = user.getName();
        id      = user.getUsername();
        int space=name.indexOf(" ");
        if(space != -1){
            name = name.substring(0, space);
        }
        rolePages = user.getRole().getRolePageURLs();
    }
    
    // always make instance of server
    Server.instance(request);
%>
            <%if (user != null){%>
            <div id="menu" class="navbar">
                <div class="navbar-inner">
                    <ul class="nav">
                        <li class="dropdown <%=(request.getAttribute("location")=="home")?"active":"\b"%>" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <%=Translation.translate("Home")%><b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a <%=(request.getAttribute("option")=="building")?"class=\"bold\"":"href=\"administration/building\""%>><%=Translation.translate("Building")%></a></li>
                                <%if(rolePages.contains("/home/visitor")){%>
                                <li><a <%=(request.getAttribute("option")=="visitor")?"class=\"bold\"":"href=\"home/visitor\""%>><%=Translation.translate("Visitor")%></a></li>
                                <%}%>
                                <%if((rolePages.contains("/home/exchange")) || (rolePages.contains("/home/visitor"))){%>
                                <li><a <%=(request.getAttribute("option")=="exchange")?"class=\"bold\"":"href=\"home/exchange\""%>><%=Translation.translate("Exchange")+" "+Translation.translate("Card")%></a></li>
                                <%}%>                                
                                <%if(rolePages.contains("/home/security")){%>
                                <li><a <%=(request.getAttribute("option")=="security")?"class=\"bold\"":"href=\"home/security\""%>><%=Translation.translate("Security")%></a></li>
                                <%}%>                                
                            </ul>
                        </li>
                        <li class="dropdown <%=(request.getAttribute("location")=="administration")?"active":"\b"%>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <%=Translation.translate("Administration")%><b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <%if(rolePages.contains("/administration/user")){%>
                                <li><a <%=(request.getAttribute("option")=="user")?"class=\"bold\"":"href=\"administration/user\""%>><%=Translation.translate("User")%></a></li>
                                <%}%>
                                <%if(rolePages.contains("/administration/role")){%>
                                <li><a <%=(request.getAttribute("option")=="role")?"class=\"bold\"":"href=\"administration/role\""%>><%=Translation.translate("Role")%></a></li>
                                <%}%>
                                <%if(rolePages.contains("/administration/card")){%>
                                <li><a <%=(request.getAttribute("option")=="card")?"class=\"bold\"":"href=\"administration/card\""%>><%=Translation.translate("Card")%></a></li>
                                <%}%>                       
                                <%if(rolePages.contains("/administration/visit")){%>
                                <li><a <%=(request.getAttribute("option")=="visit")?"class=\"bold\"":"href=\"administration/visit\""%>><%=Translation.translate("Visit Log")%></a></li>
                                <%}%>                       
                            </ul>
                        </li>
                    </ul>
                    <div id="user-cp">
                        <span id="greetings"><%=Translation.translate("Welcome")%>, <a href="administration/user?id=<%=id%>&action=view"><%=name%></a>&nbsp;</span>
                        <a href="login?action=logout" style="color:blue"><%=Translation.translate("Logout")%></a>
                    </div>
                </div>
            </div>
            <%}%>