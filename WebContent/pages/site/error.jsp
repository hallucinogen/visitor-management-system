<%@ page isErrorPage="true"%>
<jsp:include page="../site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<h3 style="margin: 20px">Error ${pageContext.errorData.statusCode}</h3>
		<div style="margin: 20px">
			Stack Trace:<pre style="font-size: 9pt"><%exception.printStackTrace(new java.io.PrintWriter(out));%></pre>
			Request that failed: <b>${pageContext.errorData.requestURI}</b>
			<br />
			Servlet name: <b>${pageContext.errorData.servletName}</b>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - Error";
	</script>
<jsp:include page="../site/footer.jsp"></jsp:include>