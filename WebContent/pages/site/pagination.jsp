                <%@page import="com.btj.helper.translate.Translation"%>
				<%
                int[] pages         = new int[5];
                int currentPage     = Integer.valueOf(request.getAttribute("currentPage").toString());
                int lastPage        = Integer.valueOf(request.getAttribute("lastPage").toString());
                
                //state of card
                String type         = null;
                if(request.getAttribute("type") != null) type = (String) request.getAttribute("type");
                
                //state of visit log
                Integer activity    = null;
                if(request.getAttribute("activity") != null) activity = Integer.valueOf(request.getAttribute("activity").toString());
                String from         = (String) request.getAttribute("from");
                String to           = (String) request.getAttribute("to");
                
                if(lastPage>1){
                %>                
			    <div class="pagination pagination-right">
			        <ul>
			            <%
			            if((lastPage>=5)){
			                int startPage = 1;
			                if(currentPage>=3) startPage = currentPage - 2;
			                if((lastPage-currentPage)<2) startPage = lastPage - 4;
			                for(int i=0;i<5;++i) pages[i] = startPage+i;
			            } else
			                for(int i=0;i<lastPage;++i) pages[i] = i+1;
			            %>
			            <li><a <%=((lastPage==1)||(currentPage==1)) ? "" : "onclick=\"changePage(1)\""%> title ="<%=Translation.translate("First")%>">&lt;&lt;</a></li>
			            <li><a <%=pages[0]==currentPage ? "style=\"font-weight:bold\"" : "onclick=\"changePage("+pages[0]+")\""%>><%=pages[0]%></a></li>
			            <% for(int i=1;i<5;++i){ if(pages[i] != 0){ %>
			                <li><a <%=pages[i]==currentPage ? "style=\"font-weight:bold\"" : "onclick=\"changePage("+pages[i]+")\""%>><%=pages[i]%></a></li>
                        <% } }%>
			            <li><a <%=((lastPage==1)||(currentPage==lastPage)) ? "" : "onclick=\"changePage("+lastPage+")\""%> title ="<%=Translation.translate("Last")%>">&gt;&gt;</a></li>
			        </ul>
			    </div>
				<%}%>
				<script type="text/javascript">
			        function toPage(action, page, column, mode){
			        	url = "<%=request.getAttribute("location")%>/<%=request.getAttribute("option")%>?action=" + action +
			                  "&page=" + page + "&column=" + column + "&mode=" + mode + 
                              "<%=type==null ? "" : "&type="+type%>" +
			                  "<%=activity==null ? "" : "&activity="+activity+"&from="+from+"&to="+to%>";
                        window.location=url;
                    }
			        
					function changePage(page) {
						var column = <%=Integer.valueOf(request.getAttribute("currentColumn").toString())%>;
						var mode   = <%=Integer.valueOf(request.getAttribute("currentMode").toString())%>;
	                    toPage("page", page, column, mode);
					}
					
					$(".sorted").click(function(){
						var page   = <%=Integer.valueOf(request.getAttribute("currentPage").toString())%>;
						var column = <%=Integer.valueOf(request.getAttribute("currentColumn").toString())%>;
                        var mode   = <%=Integer.valueOf(request.getAttribute("currentMode").toString())%>;
                        var click_column = $(this).attr('id');
						click_column = Number(click_column.substring(1,click_column.length))-1;
                        if(click_column==column) mode^=1;
                        else{
                            column=click_column;
                            mode=0;
                        }
                        toPage("sort", page, column, mode);
					});
				</script>