<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<base href="${pageContext.request.contextPath}/"/>
		<link rel="stylesheet" type="text/css" href="media/all-page.css">
		<link rel="stylesheet" type="text/css" href="media/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="media/bootstrap-responsive.css">
		<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
	</head>
	<body>
		<div id="header">
	    	<div id="header-top">&nbsp</div>
	    	<div id="header-middle">
                <div id="header-logo">
                    <img src="media/img/logo.jpg">
                </div>
                <div id="header-title">Visitor Management</div>
            </div>
            <jsp:include page="../../pages/site/menu.jsp"></jsp:include>
		</div>