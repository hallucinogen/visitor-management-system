		<div style="clear: both;"></div>
		<div id="footer">
			<div id="footer-middle">
			 <div><a onclick="set_language(0)">English</a> | <a onclick="set_language(1)">Indonesia</a></div>
			 <div><b>Copyright Bangunindo Teknusa Jaya</b></div>
			</div>
			<div id="footer-bottom">&nbsp</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				//Delete accordion-group which does not have any children
				var accordions = $(".accordion-body");
				for(var i=0,n=accordions.length; i<n; ++i){
					if(accordions.eq(i).children().length <= 0){
						accordions.eq(i).parent().remove();
					}
				}
			});
		
			function set_language(option) {
				$.ajax({
					type: "POST",
					url: "pages/site/set_language.jsp",
					data: {lang:option},
					success: function(){
						location.reload();
					}
				});
			}
		</script>
	</body>
</html>