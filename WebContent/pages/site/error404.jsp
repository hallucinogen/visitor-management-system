<%@ page isErrorPage="true"%>
<jsp:include page="../site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<h3 style="margin: 20px">Error ${pageContext.errorData.statusCode}</h3>
		<div style="margin: 20px">
			Error Message: <b style="color:red">${requestScope['javax.servlet.error.message']}</b>
			<br/>
			Request that failed: <b>${pageContext.errorData.requestURI}</b>
			<br />
			Servlet name: <b>${pageContext.errorData.servletName}</b>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - Error";
	</script>
<jsp:include page="../site/footer.jsp"></jsp:include>