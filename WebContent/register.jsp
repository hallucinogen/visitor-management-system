<%@page import="com.btj.helper.translate.Translation"%>
<jsp:include page="pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="content">
			<form id="crowd" method="post" action="administration/user">
				<input name="command" type="hidden" value="create">
				<div class="view-field">
					<div class="question">Username</div>
					<div class="separator"></div>
					<div><input name="username" type="text"></div>
				</div>
				<div class="view-field">
					<div class="question"><%=Translation.translate("Full Name")%></div>
					<div class="separator"></div>
					<div><input name="name" type="text"></div>
				</div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Department")%></div>
                    <div class="separator"></div>
                    <div><input name="department" type="text"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Title")%></div>
                    <div class="separator"></div>
                    <div><input name="title" type="text"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Mobile")%></div>
                    <div class="separator"></div>
                    <div><input name="mobile" type="text"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Email")%></div>
                    <div class="separator"></div>
                    <div><input name="email" type="text"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Password")%></div>
                    <div class="separator"></div>
                    <div><input name="password" type="password"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Confirm Password")%></div>
                    <div class="separator"></div>
                    <div><input name="re-password" type="password"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Photo")%></div>
                    <div class="separator"></div>
                    <div><i>On Development</i></div>
                </div>                
 				<button type="submit" class="btn btn-inverse">Create</button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Create")%> <%=Translation.translate("User")%>';
	</script>
<jsp:include page="pages/site/footer.jsp"></jsp:include>
