<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();    
}

String buildingID   = (String) request.getAttribute("buildingID");
Boolean isSuccess   = (Boolean) request.getAttribute("isSuccess");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <%=Translation.translate("Exchange")%>
        </div>
        <div id="content">
            <ul class="nav nav-tabs" id="exchange-tab">
                <%if(rolePages.contains("/home/visitor")){%>
                <li class="active"><a href="#return" data-toggle="tab"><%=Translation.translate("Return")%></a></li>
                <%} if(rolePages.contains("/home/exchange")){%>
                <li <%if(!rolePages.contains("/home/visitor")){%>class="active"<%}%>><a href="#to" data-toggle="tab"><%=Translation.translate("to")+" "+Translation.translate("Room")%></a></li>
                <li><a href="#from" data-toggle="tab"><%=Translation.translate("from")+" "+Translation.translate("Room")%></a></li>
                <%}%>
            </ul>
            <div class="tab-content">
                <div class="tab-pane<%if(rolePages.contains("/home/visitor")){%> active<%}%>" id="return">
                    <form id="crowd" method="post" action="home/exchange">
                        <input name="command" type="hidden" value="return">
                        <input name="buildingID" type="hidden" value="<%=buildingID%>">
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Card ID")%></div>
                            <div class="separator"></div>
                            <div><select name="inReturn" onFocus="update(2)"></select></div>
                        </div>
                        <button type="submit" class="btn btn-inverse"><%=Translation.translate("Submit")%></button>
                    </form>                    
                </div>
                <div class="tab-pane<%if(!rolePages.contains("/home/visitor")){%> active<%}%>" id="to">
                    <form id="crowd" method="post" action="home/exchange">
                        <input name="command" type="hidden" value="to">
                        <input name="buildingID" type="hidden" value="<%=buildingID%>">
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Card In")%></div>
                            <div class="separator"></div>
                            <div><select name="inTo"  onFocus="update(0)" onChange="onCardChanged(this,0)"></select></div>
                        </div>
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Room")%></div>
                            <div class="separator"></div>
                            <div><input name="roomTo" type=text readonly></div>
                        </div>
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Card Out")%></div>
                            <div class="separator"></div>
                            <div><select name="outTo"></select></div>
                        </div>
                        <button type="submit" class="btn btn-inverse"><%=Translation.translate("Submit")%></button>
                    </form>
                </div>
                <div class="tab-pane" id="from">
                    <form id="crowd" method="post" action="home/exchange">
                        <input name="command" type="hidden" value="from">
                        <input name="buildingID" type="hidden" value="<%=buildingID%>">
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Card In")%></div>
                            <div class="separator"></div>
                            <div><select name="inFrom"  onFocus="update(1)" onChange="onCardChanged(this,1)"></select></div>
                        </div>
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Room")%></div>
                            <div class="separator"></div>
                            <div><input name="roomFrom" type=text readonly></div>
                        </div>
                        <div class="view-field">
                            <div class="question"><%=Translation.translate("Card Out")%></div>
                            <div class="separator"></div>
                            <div><select name="outFrom"></select></div>
                        </div>
                        <button type="submit" class="btn btn-inverse"><%=Translation.translate("Submit")%></button>
                    </form>                    
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.title = "Visitor Management System - " +  '<%=Translation.translate("Exchange")+" "+Translation.translate("Card")%>';

        //successful transaction
        <% if(isSuccess){ %>
        $(document).ready(function(){
            alert("Success");
        });
        <% } %>        
        
        var action = new Array();
        action[0] = "To";
        action[1] = "From";
        action[2] = "Return";
        
        //When card input field clicked, update it's value 
        function update(idx){
        	$.ajax({
                url: "home/exchange?action="+action[idx],
                type: "GET",
                success: function(data) {
                    $("select[name=in"+action[idx]+"]").html("<option selected></option>"+data.substring(1,data.length));
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Oops...something is going wrong: " + errorThrown + ", please refresh the page and try again");
                }
            });
        }

        //When card input field changed, update card output field
        function onCardChanged(elmt, idx){
            var CardID = $(elmt).val();
            $.ajax({
                url: "home/exchange?action=get&id="+CardID+"&cmd="+action[idx],
                type: "GET",
                success: function(data) {
                    var i       = data.indexOf(";");
                    var room    = data.substring(0,i);
                    data        = data.substring(i+1,data.length);
                    $("input[name=room"+action[idx]+"]").val(room);
                    $("select[name=out"+action[idx]+"]").html(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Oops...something is going wrong: " + errorThrown + ", please refresh the page and try again");
                }
            });
        }
    </script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>