<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.vms.model.CardLevel"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%@page import="java.util.ArrayList"%>
<%
	String id  	 	    = (String) request.getAttribute("id");
	String result       = (String) request.getAttribute("result");
	CardLevel[] cards   = (CardLevel[]) request.getAttribute("cards");
    Boolean isSuccess   = (Boolean) request.getAttribute("isSuccess");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="news/home"><%=Translation.translate("Home")%></a> &gt; <%=Translation.translate("Security")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="home/security">
				<input name="command" type="hidden" value="<%=result.equals("confirmed") ? "update" : "verify"%>">
				<div class="view-field">
                    <div class="question"><%=Translation.translate("ID")%></div>
                    <div class="separator"></div>
                    <div><input name="id" type="text" value="<%=id%>"<%=result.equals("confirmed") ? " readonly" : ""%>></div>
                </div>
                <%if(!result.equals("")){%>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Result")%></div>
                    <div class="separator"></div>
                    <div id="result"><%=result%></div>
                </div>
                <% } %>
                <%if(result.equals("confirmed")){%>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Card")%></div>
                    <div class="separator"></div>
                    <div>
                        <select name="cardID">
                        <%
                        for(int i=0,n=cards.length; i<n; ++i){
							out.print("<option value=\"" + cards[i].getID() + "\""); //<option value="Magelang"
                            out.print(">"); //<option value="Magelang" selected>
                            out.print(cards[i].getID()); //<option value="Magelang" selected>Magelang
                            out.println("</option>");//<option value="Magelang" selected></option>
                        }
                        %>
                        </select>
                    </div>
                </div>
 				<% } %>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate(result.equals("confirmed") ? "Done" : "Verify")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " +  '<%=Translation.translate("Verify")%> <%=Translation.translate("Security")%>';
		<% if(isSuccess){ %>
	    $(document).ready(function(){
            alert("Success");
        });		
		<% } %>
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>