<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%@page import="java.util.ArrayList"%>
<%
Room[] rooms        = (Room[])  request.getAttribute("rooms");
Boolean isSuccess   = (Boolean) request.getAttribute("isSuccess");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <%=Translation.translate("Visitor")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="home/visitor">
				<input name="command" type="hidden" value="create">
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Identifier")%></div>
                    <div class="separator"></div>
                    <div><input name="identifier" type="text" onBlur="isRegistered()"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Identifier Type")%></div>
                    <div class="separator"></div>
                    <div>
                        <select name="type">
                            <option selected disabled></option>
                            <option value="KTP">KTP</option>
                            <option value="SIM">SIM</option>
                            <option value="Kartu Pelajar">Kartu Pelajar</option>
                            <option value="Passport">Passport</option>
                        </select>
                    </div>
                </div>
                <input name="command" type="hidden" value="create">
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Full Name")%></div>
                    <div class="separator"></div>
                    <div><input name="name" type="text"></div>
                </div>
				<div class="view-field">
                    <div class="question"><%=Translation.translate("Room")%></div>
                    <div class="separator"></div>
                    <div>
                        <select name="roomID">
                            <option selected disabled></option>
	                        <%
	                        for(int i=0,n=rooms.length; i<n; ++i){
	                            out.print("<option value=\"" + rooms[i].getID() + "\""); //<option value="Magelang"
	                            out.print(">"); //<option value="Magelang" selected>
	                            out.print(rooms[i].getName()); //<option value="Magelang" selected>Magelang
	                            out.println("</option>");//<option value="Magelang" selected></option>
	                        }
	                        %>
                        </select>
                    </div>
                </div>
 				<button type="submit" class="btn btn-inverse"><%=Translation.translate("Next")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " +  '<%=Translation.translate("Visitor")%>';

		//successful transaction
		<% if(isSuccess){ %>
	    $(document).ready(function(){
            alert("Success");
        });
		<% } %>
        
		//remember Identifier Type before it get changed
        var defaultSelect = $("select[name=type]").html();

        //if visitor registered then appear automatically
		function isRegistered(){
            var id = $("input[name=identifier]").val();
            $.ajax({
                url: "home/visitor?action=check&id="+id,
                type: "GET",
                success: function(data) {
                	if (data.length==0) {
                        $("input[name=type]").replaceWith("<select name=\"type\">"+defaultSelect+"</select>");
                        $("input[name=name]").val("");
                        $("input[name=name]").attr("readonly",false);
                	}
                	else{
                        var i  = data.indexOf(";");
                        var type = data.substring(0,i);
                        name   = data.substring(i+1,data.length);
                        $("select[name=type]").replaceWith("<input name=\"type\" type=\"text\" value=\""+type+"\" readonly>");
                        $("input[name=type]").replaceWith("<input name=\"type\" type=\"text\" value=\""+type+"\" readonly>");
                        $("input[name=name]").val(name);
                        $("input[name=name]").attr("readonly","readonly");
                	}
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Oops...something is going wrong: " + errorThrown + ", please refresh the page and try again");
                }
            });
        }
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>