<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.vms.model.CardLevel"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%@page import="java.util.ArrayList"%>
<%
String visitorID    = (String) request.getAttribute("visitorID");
String roomID       = (String) request.getAttribute("roomID");
CardLevel[] cards   = CardLevel.getAllNonHeldCards(Room.getRoom(roomID).getLevelID());
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
        <div id="breadcrumb">
            <a href="adminsitration/building"><%=Translation.translate("Home")%></a> &gt; <%=Translation.translate("Visitor")%>
        </div>
        <div id="content">
            <form id="crowd" method="post" action="home/visitor">
                <input name="command" type="hidden" value="update">
                <input name="visitorID" type="hidden" value="<%=visitorID%>">
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Level")%></div>
                    <div class="separator"></div>
                    <div id="level"><%=Room.getRoom(roomID).getLevel().getPhysicalLevel()%></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Card")%></div>
                    <div class="separator"></div>
                    <div>
                        <%if(cards.length==0){%>
                        <div><i>all Card used up</i></div>
                        <%} else{ %>
                        <select name="cardID">
                            <option selected disabled></option>
	                        <%
	                        for(int i=0,n=cards.length; i<n; ++i){
	                            out.print("<option value=\"" + cards[i].getID() + "\""); //<option value="Magelang"
	                            out.print(">"); //<option value="Magelang" selected>
	                            out.print(cards[i].getID()); //<option value="Magelang" selected>Magelang
	                            out.println("</option>");//<option value="Magelang" selected></option>
	                        }
	                        %>
                        </select>
                        <%}%>
                    </div>
                </div>
                <%if(cards.length>0){%>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate("Submit")%></button>
                <%}%>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        document.title = "Visitor Management System - " +  '<%=Translation.translate("New")%> <%=Translation.translate("Visitor")%>';
    </script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>