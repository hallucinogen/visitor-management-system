<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Building"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();
}

Building[] buildings	= (Building[])request.getAttribute("buildings");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
    	<div class="sub-content">
        	<div id="breadcrumb">
                <div><%=Translation.translate("Home")%></div>
            </div>
            <%if(rolePages.contains("administration-building")){%>
                <a id="breadcrumb-button" href="administration/building?action=create"><button class="btn btn-primary"><%=Translation.translate("New Building")%></button></a>
            <%}%>
        </div>
		<div id="content">
			<%for(int i=0; i<buildings.length; ++i){%>
            <div class="list-big">
	                <div class="col-left"><img src="media/img/logo.jpg"></div>
	                <div class="col-right">
	                    <%if(rolePages.contains("administration-building")){%>
                        <div class="col-right-left">
                        <%}%>
                        <a href="administration/building?id=<%=buildings[i].getID()%>&action=view">
	                        <h3><%=buildings[i].getName()%></h3>
    	                    <div><%=buildings[i].getLocation()%></div>
	                    </a>
	                    <%if(rolePages.contains("administration-building")){%>
                        <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/building?id=<%=buildings[i].getID()%>&action=update"><%=Translation.translate("Edit")%></a>
                        </div>
                        <div class="col-right-right"><a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/building?id=<%=buildings[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');">x</a></div>
                        <%}%>
	                </div>
            </div>                                                  
            <%}%>
            <jsp:include page="../../pages/site/pagination.jsp"></jsp:include>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Building")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>