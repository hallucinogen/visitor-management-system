<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Building"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();
}

Building building	= (Building)request.getAttribute("building");
Level[] levels      = (Level[])request.getAttribute("levels");
String isSorted     = "sorted";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <%=Translation.translate("Building")%>
		</div>
		<div id="content">
		    <h3><%=building.getName()%></h3>
		    <div><%=building.getLocation()%></div>
		    <div class="sub-content">
		        <h4><%=Translation.translate("Level")%> : </h4>
		        <%if(rolePages.contains("administration-level")){%>
                <a href="administration/level?id=<%=building.getID()%>&action=create"><button class="btn btn-primary"><%=Translation.translate("New Level")%></button></a>
                <%}%>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c2" class="<%=isSorted%>"><%=Translation.translate("Level")%></th>
                        <%if(rolePages.contains("administration-level")){%>
                        <th id="c3"><%=Translation.translate("Action")%></th>
                        <%}%>
                    </tr>
                </thead>
                <tbody>
                    <%for(int i=0;i<levels.length;++i){%>
                    <tr>
                        <td>
                            <a href="administration/level?id=<%=levels[i].getID()%>&action=view"><%=levels[i].getID()%></a>
                        </td>
                        <td><%=levels[i].getPhysicalLevel()%></td>
                        <%if(rolePages.contains("administration-level")){%>
                        <td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/level?id=<%=levels[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/level?id=<%=levels[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                        <%}%>
                    </tr>
                    <%}%>
                </tbody>
            </table>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Building")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>