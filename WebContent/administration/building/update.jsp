<%@page import="com.btj.vms.model.Building"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
Building building   = (Building) request.getAttribute("building");
Level[] levels      = Level.getAllLevelBuilding(building.getID());
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <%=Translation.translate("Building")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="administration/building">
				<input name="command" type="hidden" value="<%=(building.getID().equals("")) ? "create" : "update"%>">
				<div class="view-field">
					<div class="question">ID</div>
					<div class="separator"></div>
					<div><input name="id" type="text" value="<%=building.getID()%>" <%=(building.getID().equals("")) ? "" : "readonly"%>></div>
 				</div>
 				<div class="view-field">
                    <div class="question"><%=Translation.translate("Name")%></div>
                    <div class="separator"></div>
                    <div><input name="name" type="text" value="<%=building.getName()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Location")%></div>
                    <div class="separator"></div>
                    <div><input name="location" type="text" value="<%=building.getLocation()%>"></div>
                </div>
 				<button type="submit" class="btn btn-inverse"><%=Translation.translate(building.getID().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(building.getID().equals("") ? "Create" : "Update")%> <%=Translation.translate("Building")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>