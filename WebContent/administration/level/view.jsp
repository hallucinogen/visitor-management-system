<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.vms.model.Barrier"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();
}

Level level         = (Level)request.getAttribute("level");
Room[] rooms        = (Room[])request.getAttribute("rooms");
Barrier[] barriers  = (Barrier[])request.getAttribute("barriers");
String isSorted     = "sorted";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=level.getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <%=Translation.translate("Level")%>
		</div>
		<div id="content">
		    <h3><%=level.getPhysicalLevel()%></h3>
            <div class="sub-content">
                <h4><%=Translation.translate("Room")%> : </h4>
                <%if(rolePages.contains("administration-room")){%>
                <a href="administration/room?id=<%=level.getID()%>&action=create"><button class="btn btn-primary"><%=Translation.translate("New Room")%></button></a>
                <%}%>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c2" class="<%=isSorted%>"><%=Translation.translate("Name")%></th>
                        <th id="c3" class="<%=isSorted%>"><%=Translation.translate("Power")%></th>
                        <%if(rolePages.contains("administration-room")){%>
                        <th id="c4"><%=Translation.translate("Action")%></th>
                        <%}%>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i=0;i<rooms.length;++i){ %>
                    <tr>
                        <td><a href="administration/room?id=<%=rooms[i].getID()%>&action=view"><%=rooms[i].getID()%></a></td>
                        <td><%=rooms[i].getName()%></td>
                        <td><%=rooms[i].getPower()+" Watt"%></td>
                        <%if(rolePages.contains("administration-room")){%><td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/room?id=<%=rooms[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/room?id=<%=rooms[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                        <%}%>
                    </tr>
                    <% } %>
                </tbody>
            </table>
            <%if(rolePages.contains("administration-barrier")){%>
            <div class="sub-content">
                <h4><%=Translation.translate("Barrier")%> : </h4>
                <a href="administration/barrier?id=<%=level.getID()%>&action=create"><button class="btn btn-primary"><%=Translation.translate("New Barrier")%></button></a>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c3" class="<%=isSorted%>"><%=Translation.translate("Public")%></th>
                        <th id="c4"><%=Translation.translate("Action")%></th>
                    </tr>
                </thead>
                <tbody>
                    <%for(int i=0;i<barriers.length;++i){%>
                    <tr>
                        <td><a href="administration/barrier?id=<%=barriers[i].getID()%>&action=view"><%=barriers[i].getID()%></a></td>
                        <td class="caged"><%=Translation.translate(barriers[i].isPublic() ? "Yes" : "No")%></td>
                        <td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/barrier?id=<%=barriers[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/barrier?id=<%=barriers[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
            <%}%>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Level")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>