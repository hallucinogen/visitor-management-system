<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
Level level         = (Level) request.getAttribute("level");
String buildingID   = (String) request.getAttribute("buildingID");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=buildingID%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <%=Translation.translate("Level")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="administration/level">
				<input name="command" type="hidden" value="<%=(level.getID().equals("")) ? "create" : "update"%>">
                <input name="id" type="hidden" value="<%=level.getID()%>">
                <input name="buildingID" type="hidden" value="<%=buildingID%>">
				<div class="view-field">
 					<div class="question">Level</div>
 					<div class="separator"></div>
					<div><input name="physicalLevel" type="text" value="<%=level.getPhysicalLevel()%>"></div>
 				</div>
				<button type="submit" class="btn btn-inverse"><%=Translation.translate(level.getID().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(level.getID().equals("") ? "Create" : "Update")%> <%=Translation.translate("Level")%>';
		
		// TODO: make ajax request to change building depending on building
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
