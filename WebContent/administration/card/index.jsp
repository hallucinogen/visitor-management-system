<%@page import="com.btj.vms.model.CardLevel"%>
<%@page import="com.btj.vms.model.CardRoom"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
String type             = (String) request.getAttribute("type");
CardLevel[] cardLevels  = null;
CardRoom[] cardRooms    = null;
String isSorted         = null;

if(type.equals("room")) {
	cardRooms = (CardRoom[]) request.getAttribute("cards");
	isSorted = cardRooms.length > 1 ? "sorted" : "";
}
else {
	cardLevels = (CardLevel[]) request.getAttribute("cards");
	isSorted = cardLevels.length > 1 ? "sorted" : "";
}
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/card"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("Card")%>
        </div>
        <div id="content">
            <ul class="nav nav-tabs" id="card-tab">
                <li <%if(type.equals("level")){%>class="active"<%}%>><a href="administration/card?type=level"><%=Translation.translate("Level")%></a></li>
                <li <%if(type.equals("room")){%>class="active"<%}%>><a href="administration/card?type=room"><%=Translation.translate("Room")%></a></li>
            </ul>
            <a href="administration/card?action=create&type=<%=type%>"><button type="button" class="btn btn-inverse" name="main-button"><%=Translation.translate("New Card")%></button></a>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c2" class="<%=isSorted%>"><%=Translation.translate(type.equals("room") ? "Room" : "Level")%></th>
                        <th id="c3"><%=Translation.translate("Action")%></th>
                    </tr>
                </thead>
                <tbody>
                    <% if(type.equals("room")) {
                        for(int i=0;i<cardRooms.length;++i){ %>
                    <tr>
                        <td><%=cardRooms[i].getID()%></td>
                        <td><%=cardRooms[i].getRoom().getName()%></td>
                        <td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/card?type=room&id=<%=cardRooms[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/card?type=room&id=<%=cardRooms[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    <% }}
                    else{
                        for(int i=0;i<cardLevels.length;++i){ %>
                    <tr>
                        <td><%=cardLevels[i].getID()%></td>
                        <td><%=cardLevels[i].getLevel().getPhysicalLevel()%></td>
                        <td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/card?type=level&id=<%=cardLevels[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/card?type=level&id=<%=cardLevels[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                    </tr>
                    <% }} %>
                </tbody>                
            </table>            
            <jsp:include page="../../pages/site/pagination.jsp"></jsp:include>
        </div>
    </div>
    <script type="text/javascript">
        document.title = "Visitor Management System - " + '<%=Translation.translate("Card")%>';
    </script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>