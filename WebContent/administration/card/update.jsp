<%@page import="com.btj.vms.model.CardLevel"%>
<%@page import="com.btj.vms.model.CardRoom"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
String type             = (String) request.getAttribute("type");
Level[] levels          = Level.getAllLevels();
Room[] rooms            = Room.getAllRooms();
CardLevel cardLevel     = null;
CardRoom cardRoom       = null;
String cmd              = null;
String id               = null;


if(type.equals("room")) {
    cardRoom    = (CardRoom) request.getAttribute("card");
    cmd         = (cardRoom.getID().equals("")) ? "create" : "update";
    id          = cardRoom.getID();
}
else {
    cardLevel   = (CardLevel) request.getAttribute("card");
    cmd         = (cardLevel.getID().equals("")) ? "create" : "update";
    id          = cardLevel.getID();
}
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/card"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("Card")%>
        </div>
        <div id="content">
            <form id="crowd" method="post" action="administration/card">
                <input name="command" type="hidden" value="<%=cmd%>">
                <div class="view-field">
                    <div class="question">ID</div>
                    <div class="separator"></div>
                    <div><input name="id" type="text" value="<%=id%>" <%=(id.equals("")) ? "" : "readonly"%>></div>
                </div>
                <%if(id.equals("")) {%>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Type")%></div>
                    <div class="separator"></div>
                    <div>
                        <input class="pos-radio" type="radio" name="type" value="level" onClick="changeTypeField(this)">Level
                        <input class="pos-radio" type="radio" name="type" value="room" onClick="changeTypeField(this)">Room                 
                    </div>
                </div>
                <%} else {%>
                <input name="type" type="hidden" value="<%=type%>">
                <%}%>
                <div id="type-field" class="view-field">
                    <div class="question"><%=Translation.translate(type.equals("room") ? "Room" : "Level")%></div>
                    <div class="separator"></div>
                    <div>
                        <select name="<%=type.equals("room") ? "roomID" : "levelID"%>">
                            <option selected></option>
                            <%
                            if(type.equals("room")) {
                                for(int i=0;i<rooms.length;++i) {
                                    out.print("<option value=\"" + rooms[i].getID() + "\">"); //<option value="BCHxxxx" 
                                    out.print(rooms[i].getName()); //<option value="BCHxxxx">Jakarta
                                    out.println("</option>");//<option value="BCHxxx">Jakarta</option>
                                }
                            }
                            else {
                                for(int i=0;i<levels.length;++i) {
                                    out.print("<option value=\"" + levels[i].getID() + "\">"); //<option value="BCHxxxx" 
                                    out.print(levels[i].getPhysicalLevel()); //<option value="BCHxxxx">Jakarta
                                    out.println("</option>");//<option value="BCHxxx">Jakarta</option>
                                }                               
                            }
                            %>
                            
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate(id.equals("") ? "Create" : "Update")%></button>
                </form>
        </div>
    </div>
    <script type="text/javascript">
        document.title = "Visitor Management System - " + '<%=Translation.translate(id.equals("") ? "Create" : "Update")%> <%=Translation.translate("Card")%>';

        //When radio button clicked
        function changeTypeField(elmt){
            var type = $(elmt).val();
            $.ajax({
                url: "administration/card?action=get&type="+type,
                type: "GET",
                success: function(data) {
                    $("#type-field .question").html(type.substring(0,1).toUpperCase()+type.substring(1,type.length));
                    $("#type-field select").attr("name",type+"ID");
                    $("#type-field select").html("<option selected></option>"+data.substring(1,data.length));
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Oops...something is going wrong: " + errorThrown + ", please refresh the page and try again");
                }
            });
        }        
    </script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>