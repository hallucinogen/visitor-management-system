<%@page import="com.btj.vms.model.Role"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%@page import="java.util.ArrayList"%>
<%
	request.setAttribute("location", "administration");
	request.setAttribute("option", "role");
	
	Role role = (Role) request.getAttribute("role");
	ArrayList<String> pages = (ArrayList<String>) request.getAttribute("pages");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/role"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("Role")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="administration/role">
				<input name="id" type="hidden" value="<%=role.getID()%>">
				<input name="command" type="hidden" value="<%=(role.getID().equals("")) ? "create" : "update" %>">
				<div class="user-input">
					<div class="question"><%=Translation.translate("Role Name")%></div>
					<div class="separator"></div>
					<div><input name="name" type="text" value="<%=role.getName()%>"></div>
 				</div>
 				<ul class="no-style-ul">
 					<li>
                        <input name="parent-check[]" type="checkbox" value="home"> <%=Translation.translate("Home")%>
                        <ul class="no-style-ul">
                            <li><input name="home[]" type="checkbox" value="/home/visitor" <%=pages.contains("/home/visitor") ? "checked" : ""%>> <%=Translation.translate("Visitor")%></li>
                            <li><input name="home[]" type="checkbox" value="/home/exchange" <%=pages.contains("/home/exchange") ? "checked" : ""%>> <%=Translation.translate("Exchange")+" "+Translation.translate("Card")%></li>
                            <li><input name="home[]" type="checkbox" value="/home/security" <%=pages.contains("/home/security") ? "checked" : ""%>> <%=Translation.translate("Security")%></li>
                        </ul>
						<input name="parent-check[]" type="checkbox" value="admin"> <%=Translation.translate("Administration")%>
 						<ul class="no-style-ul">
	 						<li><input name="admin[]" type="checkbox" value="/administration/user" <%=pages.contains("/administration/user") ? "checked" : ""%>> <%=Translation.translate("User")%></li>
	 						<li><input name="admin[]" type="checkbox" value="/administration/role" <%=pages.contains("/administration/role") ? "checked" : ""%>> <%=Translation.translate("Role")%></li>
                            <li><input name="admin[]" type="checkbox" value="/administration/card" <%=pages.contains("/administration/card") ? "checked" : ""%>> <%=Translation.translate("Card")%></li>
                            <li><input name="admin[]" type="checkbox" value="/administration/visit" <%=pages.contains("/administration/visit") ? "checked" : ""%>> <%=Translation.translate("Visit Log")%></li>
                            <li><input name="admin[]" type="checkbox" value="administration-rack" <%=pages.contains("administration-rack") ? "checked" : ""%>> <%=Translation.translate("Rack")%></li>
                            <li><input name="admin[]" type="checkbox" value="administration-room" <%=pages.contains("administration-room") ? "checked" : ""%>> <%=Translation.translate("Room")%></li>
                            <li><input name="admin[]" type="checkbox" value="administration-barrier" <%=pages.contains("administration-barrier") ? "checked" : ""%>> <%=Translation.translate("Barrier")%></li>
                            <li><input name="admin[]" type="checkbox" value="administration-level" <%=pages.contains("administration-level") ? "checked" : ""%>> <%=Translation.translate("Level")%></li>
                            <li><input name="admin[]" type="checkbox" value="administration-building" <%=pages.contains("administration-building") ? "checked" : ""%>> <%=Translation.translate("Building")%></li>
                        </ul>
 					</li>
 				</ul>
 				<button type="submit" class="btn btn-inverse"><%=Translation.translate(role != null ? "Update" : "Create")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(role != null ? "Update" : "Create")%> <%=Translation.translate("Role")%>';
		
		$("input[name=parent-check\\[\\]]").click(function(e){
			var checked = $(this).attr('checked');
			var name	= $(this).val();
			
			checkAll(name, checked);
		});
		
		function checkAll(name, checked){
			$("input[name=" + name + "\\[\\]]").attr('checked', checked ? true : false);
		}
	</script>
	<style>
		.no-style-ul{
			list-style-type: none;
			font-size: 10pt;
		}
		
		input[type=checkbox] {
		    vertical-align: -0.1em;
		}
	</style>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>