<%@page import="com.btj.vms.model.Role"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
	Role[] roles	= (Role[]) request.getAttribute("roles");
    String isSorted	= roles.length > 1 ? "sorted" : "";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/role"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("Role")%>
		</div>
		<div id="content">
			<a href="administration/role?action=create"><button type="button" class="btn btn-inverse" name="main-button"><%=Translation.translate("New Role")%></button></a>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th id="c1" class="<%=isSorted%>">ID</th>
						<th id="c2" class="caged <%=isSorted%>"><%=Translation.translate("Name")%></th>
						<th id="c3"><%=Translation.translate("Action")%></th>
					</tr>
				</thead>
				<tbody>
					<% for(int i=0;i<roles.length;++i){ %>
					<tr>
						<td><%=roles[i].getID()%></td>
						<td class="caged"><a href="administration/role?id=<%=roles[i].getID()%>&action=update"><%=roles[i].getName()%></a></td>
						<td class="button-column">
							<a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/role?id=<%=roles[i].getID()%>&action=update"><i class="icon-edit"></i></a>
							<a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/role?id=<%=roles[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
						</td>
					</tr>
					<% } %>
				</tbody>
			</table>
            <jsp:include page="../../pages/site/pagination.jsp"></jsp:include>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Role")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
