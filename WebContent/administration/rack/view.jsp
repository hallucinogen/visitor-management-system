<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Rack"%>
<%@page import="com.btj.vms.model.Equipment"%>
<%@page import="com.btj.helper.translate.Translation"%>

<%@page import="java.util.GregorianCalendar"%>
<%	
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();
}

Rack rack               = (Rack) request.getAttribute("rack");
Equipment[] equipments  = (Equipment[])request.getAttribute("equipments");
String isSorted         = "sorted";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=rack.getRoom().getLevel().getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=rack.getRoom().getLevelID()%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <a href="administration/room?id=<%=rack.getRoomID()%>&action=view">
                <%=Translation.translate("Room")%>
            </a>
            &gt;
            <%=Translation.translate("Rack")%>
        </div>
		<div id="content">
            <h3><%=rack.getID()%></h3>
			<div class="view-field">
				<div class="question"><%=Translation.translate("Capacity")%></div>
				<div class="separator"></div>
				<div><%=rack.getCapacity()%></div>
			</div>
			<div class="sub-content">
                <h4><%=Translation.translate("Equipment")%> : </h4>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c2" class="<%=isSorted%>"><%=Translation.translate("Description")%></th>
                        <th id="c3" class="<%=isSorted%>"><%=Translation.translate("Dimension")%></th>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i=0;i<equipments.length;++i){ %>
                    <tr>
                        <td><a href="administration/room?id=<%=equipments[i].getID()%>&action=view"><%=equipments[i].getID()%></a></td>
                        <td><%=equipments[i].getDescription()%></td>
                        <td><%=equipments[i].getHeight() + " " + equipments[i].getWidth() + " " + equipments[i].getDepth() + " cm"%></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
		</div>
	</div>
	<script type="text/javascript" src="js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Rack")%>';

		$(document).ready(function(){
			
		});
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>