<%@page import="com.btj.vms.model.Rack"%>
<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
Rack rack       = (Rack) request.getAttribute("rack");
String roomID   = (String) request.getAttribute("roomID");
Room room       = Room.getRoom(roomID);
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=room.getLevel().getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=room.getLevel().getID()%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <a href="administration/room?id=<%=roomID%>&action=view">
                <%=Translation.translate("Room")%>
            </a>
            &gt;
            <%=Translation.translate("Rack")%>
        </div>
		<div id="content">
		    <form id="crowd" method="post" action="administration/rack">
 				<input name="command" type="hidden" value="<%=(rack.getID().equals("")) ? "create" : "update"%>">
                <input name="id" type="hidden" value="<%=rack.getID()%>">
                <input name="roomID" type="hidden" value="<%=roomID%>">
				<div class="view-field">
 					<div class="question">Capacity</div>
 					<div class="separator"></div>
					<div><input name="capacity" type="text" value="<%=rack.getCapacity()%>"></div>
 				</div>
				<button type="submit" class="btn btn-inverse"><%=Translation.translate(rack.getID().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(rack.getID().equals("") ? "Create" : "Update")%> <%=Translation.translate("Rack")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
