<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
Room room       = (Room) request.getAttribute("room");
String levelID  = (String) request.getAttribute("levelID");
Level level     = Level.getLevel(levelID);
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=level.getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=levelID%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <%=Translation.translate("Room")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="administration/room">
				<input name="command" type="hidden" value="<%=(room.getID().equals("")) ? "create" : "update"%>">
                <input name="id" type="hidden" value="<%=room.getID()%>">
                <input name="levelID" type="hidden" value="<%=levelID%>">
				<div class="view-field">
                    <div class="question">Name</div>
                    <div class="separator"></div>
                    <div><input name="name" type="text" value="<%=room.getName()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question">Power (Watt)</div>
                    <div class="separator"></div>
                    <div><input name="power" type="text" value="<%=room.getPower()%>"></div>
                </div>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate(room.getID().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(room.getID().equals("") ? "Create" : "Update")%> <%=Translation.translate("Room")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>