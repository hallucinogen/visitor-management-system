<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.vms.model.Room"%>
<%@page import="com.btj.vms.model.Rack"%>
<%@page import="com.btj.helper.translate.Translation"%>

<%@page import="java.util.GregorianCalendar"%>
<%	
ArrayList<String> rolePages = null; 
User user = (User) session.getAttribute("User");
String name = "";
String id = "";

if(user != null){
    name    = user.getName();
    id      = user.getUsername();
    int space=name.indexOf(" ");
    if(space != -1){
        name = name.substring(0, space);
    }
    rolePages = user.getRole().getRolePageURLs();
}

Room room       = (Room) request.getAttribute("room");
Rack[] racks    = (Rack[])request.getAttribute("racks");
String isSorted = "sorted";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=room.getLevel().getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=room.getLevelID()%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <%=Translation.translate("Room")%>
		</div>
		<div id="content">
    		<h3><%=room.getName()%></h3>
            <div class="view-field">
                <div class="question">Power</div>
                <div class="separator"></div>
                <div><%=room.getPower()%> Watt</div>
            </div>
            <div class="sub-content">
                <h4><%=Translation.translate("Rack")%> : </h4>
                <%if(rolePages.contains("administration-rack")){%>
                <a href="administration/rack?id=<%=room.getID()%>&action=create"><button class="btn btn-primary"><%=Translation.translate("New Rack")%></button></a>
                <%}%>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th id="c1" class="<%=isSorted%>">ID</th>
                        <th id="c2" class="<%=isSorted%>"><%=Translation.translate("Capacity")%></th>
                        <%if(rolePages.contains("administration-rack")){%>
                        <th id="c3"><%=Translation.translate("Action")%></th>
                        <%}%>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i=0;i<racks.length;++i){ %>
                    <tr>
                        <td><a href="administration/rack?id=<%=racks[i].getID()%>&action=view"><%=racks[i].getID()%></a></td>
                        <td><%=racks[i].getRemainingCapacity()+"/"+racks[i].getCapacity()%></td>
                        <%if(rolePages.contains("administration-rack")){%>
                        <td class="button-column">
                            <a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/rack?id=<%=racks[i].getID()%>&action=update"><i class="icon-edit"></i></a>
                            <a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/rack?id=<%=racks[i].getID()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
                        </td>
                        <%}%>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
	</div>
	<script type="text/javascript" src="js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Room")%>';

		$(document).ready(function(){
			
		});
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>