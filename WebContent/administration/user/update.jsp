<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
User user		= (User) request.getAttribute("user");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/user"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("User")%>
		</div>
		<div id="content">
			<form id="crowd" method="post" action="administration/user">
				<input name="command" type="hidden" value="<%=(user.getUsername().equals("")) ? "create" : "update"%>">
				<div class="view-field">
					<div class="question">Username</div>
					<div class="separator"></div>
					<div><input name="username" type="text" value="<%=user.getUsername()%>" <%=(user.getUsername().equals("")) ? "" : "readonly"%>></div>
				</div>
				<div class="view-field">
					<div class="question"><%=Translation.translate("Full Name")%></div>
					<div class="separator"></div>
					<div><input name="name" type="text" value="<%=user.getName()%>"></div>
				</div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Department")%></div>
                    <div class="separator"></div>
                    <div><input name="department" type="text" value="<%=user.getDepartment()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Title")%></div>
                    <div class="separator"></div>
                    <div><input name="title" type="text" value="<%=user.getTitle()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Mobile")%></div>
                    <div class="separator"></div>
                    <div><input name="mobile" type="text" value="<%=user.getMobile()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Email")%></div>
                    <div class="separator"></div>
                    <div><input name="email" type="text" value="<%=user.getEmail()%>"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Password")%></div>
                    <div class="separator"></div>
                    <div><input name="password" type="password"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Confirm Password")%></div>
                    <div class="separator"></div>
                    <div><input name="re-password" type="password"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Photo")%></div>
                    <div class="separator"></div>
                    <div><i>On Development</i></div>
                </div>                
 				<button type="submit" class="btn btn-inverse"><%=Translation.translate(user.getUsername().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(user.getUsername().equals("") ? "Create" : "Update")%> <%=Translation.translate("User")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
