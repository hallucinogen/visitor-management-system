<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
User[] users	= (User[])request.getAttribute("users");
String isSorted	= users.length > 1 ? "sorted" : "";
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/user"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("User")%>
		</div>
		<div id="content">
			<a href="administration/user?action=create"><button type="button" class="btn btn-inverse" name="main-button"><%=Translation.translate("New User")%></button></a>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th id="c1" class="<%=isSorted%>">ID</th>
						<th id="c2" class="caged <%=isSorted%>"><%=Translation.translate("Name")%></th>
						<th id="c3" class="caged <%=isSorted%>"><%=Translation.translate("Role")%></th>
						<th id="c8"><%=Translation.translate("Action")%></th>
					</tr>
				</thead>
				<tbody>
					<% for(int i=0;i<users.length;++i){ %>
					<tr>
						<td class="caged"><%
							out.println(users[i].getUsername());
						%></td>
						<td class="caged"><a href="administration/user?id=<%=users[i].getUsername()%>&action=view"><%
							out.println(users[i].getName());
						%></a></td>
						<td class="caged"><%
							out.println(users[i].getRole().getName());
						%></td>
						<td class="button-column">
							<a class="edit" title="<%=Translation.translate("Edit")%>" href="administration/user?id=<%=users[i].getUsername()%>&action=update"><i class="icon-edit"></i></a>
							<a class="delete" title="<%=Translation.translate("Delete")%>" href="administration/user?id=<%=users[i].getUsername()%>&action=delete" onclick="return confirm('Are you sure you want to delete this entry ?');"><i class="icon-remove"></i></a>
						</td>
					</tr>
					<% } %>
				</tbody>
			</table>
			<jsp:include page="../../pages/site/pagination.jsp"></jsp:include>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("User")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
	