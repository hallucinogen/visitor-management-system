<%@page import="com.btj.vms.model.Role"%>
<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.helper.translate.Translation"%>

<%@page import="java.util.GregorianCalendar"%>
<%	
User user			= (User) request.getAttribute("user");
User loggedUser     = (User) session.getAttribute("User");
String requestDate	= (String) request.getAttribute("requestDate");
int currentMonth	= Integer.valueOf(requestDate.split("-")[1]) - 1; //minus 1 because Attendance is built using date.getMonth (0-indexed)
int currentYear		= Integer.valueOf(requestDate.split("-")[0]);
	
boolean salaryAccessible = Role.getRoleAccess(loggedUser.getRoleID(), "/administration/salary");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			<a href="administration/building"><%=Translation.translate("Home")%></a> &gt; <a href="administration/user"><%=Translation.translate("Administration")%></a> &gt; <%=Translation.translate("User")%>
		</div>
		<div id="content">
			<ul class="nav nav-tabs" id="user-tab">
		    	<li class="active"><a href="#first" data-toggle="tab"><%=Translation.translate("Info")%></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="first">
                    <div class="view-field">
                        <div class="question"><%=Translation.translate("Role")%></div>
                        <div class="separator"></div>
                        <div><a href="administration/role?id=<%=user.getRoleID()%>&action=update"><%=user.getRole().getName()%></a></div>
                    </div>
					<div class="view-field">
						<div class="question">Username</div>
						<div class="separator"></div>
						<div><%=user.getUsername()%></div>
					</div>
					<div class="view-field">
						<div class="question"><%=Translation.translate("Full Name")%></div>
						<div class="separator"></div>
						<div><%=user.getName()%></div>
					</div>
					<div class="view-field">
						<div class="question"><%=Translation.translate("Department")%></div>
						<div class="separator"></div>
						<div><%=user.getDepartment()%></div>
					</div>
					<div class="view-field">
						<div class="question"><%=Translation.translate("Title")%></div>
						<div class="separator"></div>
						<div><%=user.getTitle()%></div>
					</div>
					<div class="view-field">
						<div class="question"><%=Translation.translate("Mobile")%></div>
						<div class="separator"></div>
						<div><%=user.getMobile()%></div>
					</div>
					<div class="view-field">
						<div class="question"><%=Translation.translate("Email")%></div>
						<div class="separator"></div>
						<div><%=user.getEmail()%></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("View")%> <%=Translation.translate("User")%>';
		
		var attendance		= null;
		var selectedDayID	= null;
		$(document).ready(function(){
			
		});
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>