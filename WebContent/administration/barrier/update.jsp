<%@page import="com.btj.vms.model.Barrier"%>
<%@page import="com.btj.vms.model.Level"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
Barrier barrier = (Barrier) request.getAttribute("barrier");
String levelID  = (String) request.getAttribute("levelID");
Level level     = Level.getLevel(levelID);
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=level.getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=levelID%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <%=Translation.translate("Barrier")%>
        </div>
		<div id="content">
			<form id="crowd" method="post" action="administration/barrier">
				<input name="command" type="hidden" value="<%=(barrier.getID().equals("")) ? "create" : "update"%>">
                <input name="id" type="hidden" value="<%=barrier.getID()%>">
                <input name="levelID" type="hidden" value="<%=levelID%>">
				<div class="view-field">
 					<div class="question"><%=Translation.translate("Public")%></div>
 					<div class="separator"></div>
 					<div>
                        <input class="pos-radio" type="radio" name="public" value="1" <%=barrier != null ? barrier.isPublic() ? "checked" : "" : ""%>>Yes
                        <input class="pos-radio" type="radio" name="public" value="0" <%=barrier != null ? !barrier.isPublic() ? "checked" : "" : ""%>>No
                    </div>
 				</div>
 				<button type="submit" class="btn btn-inverse"><%=Translation.translate(barrier.getID().equals("") ? "Create" : "Update")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate(barrier.getID().equals("") ? "Create" : "Update")%> <%=Translation.translate("Barrier")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>
