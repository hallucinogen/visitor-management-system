<%@page import="com.btj.vms.model.Barrier"%>
<%@page import="com.btj.helper.translate.Translation"%>

<%@page import="java.util.GregorianCalendar"%>
<%	
Barrier barrier	= (Barrier) request.getAttribute("barrier");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=barrier.getLevel().getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=barrier.getLevelID()%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <%=Translation.translate("Barrier")%>
        </div>
		<div id="content">
            <h3><%=barrier.getID()%></h3>
			<div class="view-field">
				<div class="question"><%=Translation.translate("Public")%></div>
				<div class="separator"></div>
				<div><%=Translation.translate(barrier.isPublic() ? "Yes" : "No")%></div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/autoNumeric-1.7.5.js"></script>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Barrier")%>';

		$(document).ready(function(){
			
		});
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>