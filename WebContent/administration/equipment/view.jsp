<%@page import="com.btj.vms.model.Equipment"%>
<%@page import="com.btj.helper.translate.Translation"%>

<%@page import="java.util.GregorianCalendar"%>
<%  
Equipment equipment = (Equipment) request.getAttribute("equipment");
%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
    <div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/building?id=<%=equipment.getRack().getRoom().getLevel().getBuildingID()%>&action=view">
                <%=Translation.translate("Building")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=equipment.getRack().getRoom().getLevelID()%>&action=view">
                <%=Translation.translate("Level")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=equipment.getRack().getRoomID()%>&action=view">
                <%=Translation.translate("Room")%>
            </a>
            &gt;
            <a href="administration/level?id=<%=equipment.getRackID()%>&action=view">
                <%=Translation.translate("Rack")%>
            </a>
            &gt;
            <%=Translation.translate("Equipment")%>
        </div>
        <div id="content">
            <h3><%=equipment.getDescription()%></h3>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Current")%></div>
                <div class="separator"></div>
                <div><%=equipment.getCurrent()%></div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Phase")%></div>
                <div class="separator"></div>
                <div><%=equipment.getPhase()%></div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Heat")%></div>
                <div class="separator"></div>
                <div><%=equipment.getHeat()%></div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Power")%></div>
                <div class="separator"></div>
                <div><%=equipment.getPower()%> Watt</div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Weight")%></div>
                <div class="separator"></div>
                <div><%=equipment.getWeight()%> kg</div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Dimension")%></div>
                <div class="separator"></div>
                <div><%=equipment.getHeight()%> x <%=equipment.getWidth()%> x <%=equipment.getDepth()%> cm</div>
            </div>
            <div class="view-field">
                <div class="question"><%=Translation.translate("Redundant Power")%></div>
                <div class="separator"></div>
                <div><%=Translation.translate(equipment.isRedundant() ? "Yes" : "No")%></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/autoNumeric-1.7.5.js"></script>
    <script type="text/javascript">
        document.title = "Visitor Management System - " + '<%=Translation.translate("Equipment")%>';

        $(document).ready(function(){
            
        });
    </script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>