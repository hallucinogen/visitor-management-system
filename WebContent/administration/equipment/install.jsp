<%@page import="com.btj.helper.translate.Translation"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
		<div id="breadcrumb">
			Equipment
		</div>
		<div id="content">
			<form id="crowd" method="post" action="home/equipment">
                <div class="content-group">
                    <div class="content-group-header"><%=Translation.translate("Equipment")%></div>
                    <table id="equipment-table">
	                    <thead>
	                        <tr>
	                            <th>Bar Code</th>
	                            <th>Floor</th>
	                            <th>AC/DC</th>
	                            <th>Phase</th>
	                            <th>Heat<br>(BTU/Hr)</th>
	                            <th>Power<br>(Watt)</th>
	                            <th>Weight<br>(Kg)</th>
	                            <th>Dimension<br>(cm)</th>
	                            <th>Redundant<br>Power</th>
	                            <th>Location</th>
	                            <th>Description</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td><input name="id" type="text"></td>
	                            <td><input name="level" type="text"></td>
	                            <td><input name="current" type="text"></td>
	                            <td><input name="phase" type="text"></td>
	                            <td><input name="heat" type="text"></td>
	                            <td><input name="power" type="text"></td>
	                            <td><input name="weight" type="text"></td>
	                            <td><input name="height" type="text" placeholder="H">x<input name="width" type="text" placeholder="W">x<input name="depth" type="text" placeholder="D"></td>
	                            <td><input name="redundant" type="text"></td>
	                            <td><input name="location" type="text"></td>
	                            <td><input name="description" type="text"></td>
	                        </tr>
	                    </tbody>
	                </table>                
	                <fieldset>
		               <legend>Attach Document</legend>
		               <div>
		                  <input name="location" type="text">
		                  <button name="del-btn" type="button" class="btn btn-primary"><%=Translation.translate("Browse")%></button>
		               </div>
		            </fieldset>
                    <div class="content-group-footer">&nbsp</div>
                </div>
                <div class="content-group">
                    <div class="content-group-header"><%=Translation.translate("Installation")%></div>
	                <div class="content-group-body">
	                    <div id="col-left">
			                <div class="view-field">
			                    <div class="question"><%=Translation.translate("Schedule Installation")%></div>
			                    <div class="separator"></div>
			                    <div><input name="begin-date" type="date"> to <input name="end-date" type="date"></div>
			                </div>
		                    <div class="view-field">
		                        <div class="question"><%=Translation.translate("Vendor")%></div>
		                        <div class="separator"></div>
		                        <div><input name="vendor" type="text"></div>
		                    </div>
		                    <div class="view-field">
		                        <div class="question"><%=Translation.translate("Project Name")%></div>
		                        <div class="separator"></div>
		                        <div><input name="project" type="text"></div>
		                    </div>
		                    <div class="view-field">
		                        <div class="question"><%=Translation.translate("User")%></div>
		                        <div class="separator"></div>
		                        <div><select name="userID"></select></div>
		                    </div>
		                </div>
		                <div id="col-right">
		                   <div><%=Translation.translate("Note")%></div>
	                       <div><textarea name="note" rows="7"></textarea></div>
		                </div>
	                </div>
                    <div class="content-group-footer">&nbsp</div>
                </div>
                <div class="content-group">
                    <div class="content-group-header"><%=Translation.translate("Approval Name List")%></div>
                    <div id="approval-manipulator">
	                    <select name="approvalID"></select>
	                    <button name="add-btn" type="button" class="btn btn-primary"><%=Translation.translate("Add")%></button>
                        <button name="del-btn" type="button" class="btn btn-primary"><%=Translation.translate("Delete")%></button>
	                </div>
	                <table id="approval-table">
		                <thead>
		                    <tr>
		                        <th>&nbsp</th>
		                        <th>Name</th>
		                        <th>Department</th>
		                        <th>Title</th>
		                        <th>Phone</th>
		                        <th>Email</th>
		                    </tr>
		                </thead>
		                <tbody>
		                </tbody>
	                </table>
                    <div class="content-group-footer">&nbsp</div>
                </div>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate("Create")%></button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " +  '<%=Translation.translate("New")%> <%=Translation.translate("Equipment")%>';
	    $("head").append("<link rel=\"stylesheet\" type=\"text/css\" href=\"media/approval-sheet.css\">");
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>