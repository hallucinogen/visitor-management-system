<%@page import="com.btj.vms.model.Visit"%>
<%@page import="com.btj.helper.translate.Translation"%>
<jsp:include page="../../pages/site/header.jsp"></jsp:include>
	<div id="body-wrapper">
        <div id="breadcrumb">
            <a href="administration/building"><%=Translation.translate("Home")%></a>
            &gt;
            <a href="administration/visit"><%=Translation.translate("Administration")%></a>
            &gt;
            <%=Translation.translate("Visit Log")%>
		</div>
		<div id="content">
            <form id="crowd" method="post" action="administration/visit">
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Date")%></div>
                    <div class="separator"></div>
                    <div><input name="begin-date" type="date"> to <input name="end-date" type="date"></div>
                </div>
                <div class="view-field">
                    <div class="question"><%=Translation.translate("Category")%></div>
                    <div class="separator"></div>
                    <div>
                        <input class="pos-radio" type="radio" name="activity" value="0">Out
                        <input class="pos-radio" type="radio" name="activity" value="1">In
                        <input class="pos-radio" type="radio" name="activity" value="2">All
                    </div>
                </div>
                <button type="submit" class="btn btn-inverse"><%=Translation.translate("Submit")%></button>
            </form>
		</div>
	</div>
	<script type="text/javascript">
		document.title = "Visitor Management System - " + '<%=Translation.translate("Visit Log")%>';
	</script>
<jsp:include page="../../pages/site/footer.jsp"></jsp:include>