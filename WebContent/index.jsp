<%@page import="com.btj.vms.model.User"%>
<%@page import="com.btj.helper.translate.Translation"%>
<%
	User user = (User) session.getAttribute("User");
	if(user != null){
		response.sendRedirect("home/building");
	}
%>

<jsp:include page="pages/site/header.jsp"></jsp:include>
<div id="body-wrapper">
	<div id="login-wrapper">
		<form name="login-form" method="post" action="login">
			<div>Username</div>
			<div><input name="username" type="text"></div>
			<div>Password</div>
			<div><input name="password" type="password"></div>
			<div id="addition">
				<input type="checkbox" name="remember"> <%=Translation.translate("Remember Me")%>
				<a id="forgot-link"><%=Translation.translate("Forgot Password")%></a>
			</div>
			<div id="button-wrapper">
			<button id="log-btn" class="btn btn-inverse" type="submit"><%=Translation.translate("Login")%></button>
            <a id="reg-btn" href="register.jsp">
                <button class="btn btn-inverse" type="button"><%=Translation.translate("Register")%></button>
            </a>
            </div>
		</form>
		<%if((request.getParameter("failed")!=null)&&(request.getParameter("failed").equals("true"))){%>
		<div id="warn"><%=Translation.translate("Incorrect Username or Password")%></div>
		<%}%>
	</div>
</div>
<jsp:include page="pages/site/footer.jsp"></jsp:include>
<div id="forgot-background"></div>
<div id="forgot-wrapper">
    <form name="forgot-form" method="post" action="ForgotHandler">
        <div>Username</div>
        <div><input type="text" name="username"></div>
        <button class="btn btn-inverse" type="submit"><%=Translation.translate("Send notification to email")%></button>
    </form>
</div>
<script type="text/javascript">
    document.title = "Visitor Management System";
    
    $("#forgot-link").click(function(){
        $("#forgot-background").show(100);
        $("#forgot-wrapper").show(100);
    });
    
    $("#forgot-background").click(function(){
        $("#forgot-wrapper").hide(100);
        $("#forgot-background").hide(100);
    });

    $("head").append("<link rel=\"stylesheet\" type=\"text/css\" href=\"media/index.css\">");
</script>   
