-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 26, 2013 at 06:33 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vms`
--

-- --------------------------------------------------------

--
-- Table structure for table `barrier`
--

CREATE TABLE IF NOT EXISTS `barrier` (
  `barrierID` varchar(25) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  `public` int(11) NOT NULL DEFAULT '0' COMMENT 'If public = 1 we will allow any card enter. Else, they should authenticate themselves',
  PRIMARY KEY (`barrierID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barrier`
--

INSERT INTO `barrier` (`barrierID`, `levelID`, `public`) VALUES
('BARSMG_001', 'SMG_002', 0),
('BARSMG_002', 'SMG_001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `buildingID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`buildingID`, `name`, `location`) VALUES
('SMG', 'Semanggi', 'Semanggi dekat Plaza Semanggi'),
('SRG', 'Sadang Serang', 'Jalan Sadang Serang 34');

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `cardID` varchar(25) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`cardID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`cardID`, `location`) VALUES
('CRDSMG_0001', 'Somewhere');

-- --------------------------------------------------------

--
-- Table structure for table `card_barrier`
--

CREATE TABLE IF NOT EXISTS `card_barrier` (
  `cardID` varchar(25) NOT NULL,
  `barrierID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`barrierID`),
  KEY `barrierID` (`barrierID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_barrier`
--

INSERT INTO `card_barrier` (`cardID`, `barrierID`) VALUES
('CRDSMG_0001', 'BARSMG_001'),
('CRDSMG_0001', 'BARSMG_002');

-- --------------------------------------------------------

--
-- Table structure for table `card_level`
--

CREATE TABLE IF NOT EXISTS `card_level` (
  `cardID` varchar(25) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`levelID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_level`
--

INSERT INTO `card_level` (`cardID`, `levelID`) VALUES
('CRDSMG_0001', 'SMG_001');

-- --------------------------------------------------------

--
-- Table structure for table `card_user`
--

CREATE TABLE IF NOT EXISTS `card_user` (
  `cardID` varchar(25) NOT NULL,
  `userID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`userID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `levelID` varchar(25) NOT NULL,
  `buildingID` varchar(25) NOT NULL,
  `physicalLevel` int(11) NOT NULL,
  PRIMARY KEY (`levelID`),
  KEY `buildingID` (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`levelID`, `buildingID`, `physicalLevel`) VALUES
('SMG_001', 'SMG', 1),
('SMG_002', 'SMG', 2);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleID` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleID`, `name`) VALUES
('ROLCENTRAL00001', 'Administrator'),
('ROLCENTRAL00002', 'Manager'),
('ROLCENTRAL00003', 'Direksi'),
('ROLCENTRAL00004', 'Chief (Outlet Manager)'),
('ROLCENTRAL00005', 'Cashier');

-- --------------------------------------------------------

--
-- Table structure for table `role_page`
--

CREATE TABLE IF NOT EXISTS `role_page` (
  `roleID` varchar(25) NOT NULL,
  `pageURL` varchar(100) NOT NULL,
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_page`
--

INSERT INTO `role_page` (`roleID`, `pageURL`) VALUES
('ROLCENTRAL00001', '/visitor/home'),
('ROLCENTRAL00001', '/visitor/home?action=create'),
('ROLCENTRAL00001', '/administration/user'),
('ROLCENTRAL00001', '/administration/role'),
('ROLCENTRAL00001', '/administration/card'),
('ROLCENTRAL00001', '/administration/barrier'),
('ROLCENTRAL00001', '/administration/building'),
('ROLCENTRAL00001', '/auth/barrier'),
('ROLCENTRAL00001', '/auth/level');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `roleID` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `name`, `roleID`, `password`) VALUES
('1234', 'Listiarso Wastuargo', 'ROLCENTRAL00001', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barrier`
--
ALTER TABLE `barrier`
  ADD CONSTRAINT `barrier_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `card_barrier`
--
ALTER TABLE `card_barrier`
  ADD CONSTRAINT `card_barrier_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_barrier_ibfk_2` FOREIGN KEY (`barrierID`) REFERENCES `barrier` (`barrierID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `card_level`
--
ALTER TABLE `card_level`
  ADD CONSTRAINT `card_level_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_level_ibfk_2` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `card_user`
--
ALTER TABLE `card_user`
  ADD CONSTRAINT `card_user_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_user_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `level`
--
ALTER TABLE `level`
  ADD CONSTRAINT `level_ibfk_1` FOREIGN KEY (`buildingID`) REFERENCES `building` (`buildingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_page`
--
ALTER TABLE `role_page`
  ADD CONSTRAINT `role_page_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON UPDATE CASCADE;
