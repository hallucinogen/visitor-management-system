-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 13 Mar 2013 pada 09.31
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `vms`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `approval`
--

CREATE TABLE IF NOT EXISTS `approval` (
  `workingPermitID` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `approvedDate` date DEFAULT NULL,
  PRIMARY KEY (`username`,`workingPermitID`),
  KEY `approval_ibfk_1` (`username`),
  KEY `approval_ibfk_2` (`workingPermitID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `approvalsheet`
--

CREATE TABLE IF NOT EXISTS `approvalsheet` (
  `workingPermitID` varchar(25) NOT NULL,
  `vendor` varchar(40) NOT NULL,
  `project` varchar(40) NOT NULL,
  PRIMARY KEY (`workingPermitID`),
  KEY `approvalsheet_ibfk_1` (`workingPermitID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `approvalsheet_equipment`
--

CREATE TABLE IF NOT EXISTS `approvalsheet_equipment` (
  `workingPermitID` varchar(25) NOT NULL,
  `equipmentID` varchar(25) NOT NULL,
  `inout` varchar(3) NOT NULL,
  PRIMARY KEY (`workingPermitID`,`equipmentID`),
  KEY `approvalsheet_equipment_ibfk_1` (`workingPermitID`),
  KEY `approvalsheet_equipment_ibfk_2` (`equipmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barrier`
--

CREATE TABLE IF NOT EXISTS `barrier` (
  `barrierID` varchar(25) NOT NULL,
  `levelID` varchar(25) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'public = 0 -> should authenticate. public = 1 -> allow any card enter',
  PRIMARY KEY (`barrierID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barrier`
--

INSERT INTO `barrier` (`barrierID`, `levelID`, `public`) VALUES
('BARSMG_001', 'SMG_001', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `buildingID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `baseLevelID` varchar(25) NOT NULL,
  PRIMARY KEY (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `building`
--

INSERT INTO `building` (`buildingID`, `name`, `location`, `baseLevelID`) VALUES
('SMG', 'Semanggi', 'Semanggi dekat Plaza Semanggi', 'SMG_001'),
('SRG', 'Sadang Serang', 'Jalan Sadang Serang 34', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `cardID` varchar(25) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`cardID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card`
--

INSERT INTO `card` (`cardID`, `location`) VALUES
('CRDSMG_0001', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card_level`
--

CREATE TABLE IF NOT EXISTS `card_level` (
  `cardID` varchar(25) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  `visitorID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`levelID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card_level`
--

INSERT INTO `card_level` (`cardID`, `levelID`, `visitorID`) VALUES
('CRDSMG_0001', 'SMG_001', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card_room`
--

CREATE TABLE IF NOT EXISTS `card_room` (
  `cardID` varchar(25) NOT NULL,
  `roomID` varchar(25) NOT NULL,
  `visitorID` varchar(25) NOT NULL,
  PRIMARY KEY (`cardID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card_room`
--

INSERT INTO `card_room` (`cardID`, `roomID`, `visitorID`) VALUES
('CRDSMG_002', 'ROMSMG_001', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
  `equipmentID` varchar(25) NOT NULL,
  `rackID` varchar(25) NOT NULL,
  `current` tinyint(1) NOT NULL COMMENT 'current = 0 -> AC. current = 1 -> DC',
  `phase` int(11) NOT NULL,
  `heat` double NOT NULL,
  `power` double NOT NULL,
  `weight` double NOT NULL,
  `height` double NOT NULL,
  `width` double NOT NULL,
  `depth` double NOT NULL,
  `redundant` tinyint(1) NOT NULL COMMENT 'redundant = 0 -> no. redundant = 1 -> yes',
  `description` varchar(100) NOT NULL,
  `isInstalled` tinyint(1) NOT NULL COMMENT 'isInstalled = 0 -> not yet installed. isInstalled = 1 -> installed',
  PRIMARY KEY (`equipmentID`),
  KEY `equipment_ibfk_1` (`rackID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `levelID` varchar(25) NOT NULL,
  `buildingID` varchar(25) NOT NULL,
  `physicalLevel` int(11) NOT NULL,
  PRIMARY KEY (`levelID`),
  KEY `buildingID` (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`levelID`, `buildingID`, `physicalLevel`) VALUES
('SMG_001', 'SMG', 1),
('SMG_002', 'SMG', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rack`
--

CREATE TABLE IF NOT EXISTS `rack` (
  `rackID` varchar(25) NOT NULL,
  `roomID` varchar(25) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`rackID`),
  KEY `rack_ibfk_1` (`roomID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleID` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`roleID`, `name`) VALUES
('ROLCENTRAL00000', 'User'),
('ROLCENTRAL00001', 'Administrator'),
('ROLCENTRAL00002', 'Manager'),
('ROLCENTRAL00003', 'Direksi'),
('ROLCENTRAL00004', 'Chief (Outlet Manager)'),
('ROLCENTRAL00005', 'Cashier');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_page`
--

CREATE TABLE IF NOT EXISTS `role_page` (
  `roleID` varchar(25) NOT NULL,
  `pageURL` varchar(100) NOT NULL,
  KEY `role_page_ibfk_1` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role_page`
--

INSERT INTO `role_page` (`roleID`, `pageURL`) VALUES
('ROLCENTRAL00000', '/administration/equipment'),
('ROLCENTRAL00000', '/administration/rack'),
('ROLCENTRAL00000', '/administration/room'),
('ROLCENTRAL00000', '/administration/barrier'),
('ROLCENTRAL00000', '/administration/level'),
('ROLCENTRAL00000', '/administration/building'),
('ROLCENTRAL00001', '/administration/equipment'),
('ROLCENTRAL00001', '/administration/rack'),
('ROLCENTRAL00001', '/administration/room'),
('ROLCENTRAL00001', '/administration/barrier'),
('ROLCENTRAL00001', '/administration/level'),
('ROLCENTRAL00001', '/administration/building'),
('ROLCENTRAL00001', '/home/visitor'),
('ROLCENTRAL00001', '/home/exchange'),
('ROLCENTRAL00001', '/home/security'),
('ROLCENTRAL00001', '/administration/user'),
('ROLCENTRAL00001', '/administration/role'),
('ROLCENTRAL00001', '/administration/card'),
('ROLCENTRAL00001', '/administration/visit'),
('ROLCENTRAL00001', 'administration-rack'),
('ROLCENTRAL00001', 'administration-room'),
('ROLCENTRAL00001', 'administration-barrier'),
('ROLCENTRAL00001', 'administration-level'),
('ROLCENTRAL00001', 'administration-building');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `roomID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  `power` double NOT NULL,
  PRIMARY KEY (`roomID`),
  KEY `level_ibfk_1` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `room`
--

INSERT INTO `room` (`roomID`, `name`, `levelID`, `power`) VALUES
('ROMSMG_001', 'XL Data Center', 'SMG_001', 100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `department` varchar(40) NOT NULL,
  `title` varchar(40) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `roleID` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`username`, `name`, `department`, `title`, `mobile`, `email`, `roleID`, `password`) VALUES
('123', '321', '123', '123', '123', '123', 'ROLCENTRAL00000', '202cb962ac59075b964b07152d234b70'),
('1234', 'Listiarso Wastuargo', 'IIT', 'CTO', '0812345679', '13508xxx@std.stei.itb.ac.id', 'ROLCENTRAL00001', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visit`
--

CREATE TABLE IF NOT EXISTS `visit` (
  `visitID` varchar(25) NOT NULL,
  `time` datetime NOT NULL,
  `visitorID` varchar(25) NOT NULL,
  `activity` tinyint(1) NOT NULL COMMENT 'activity = 0 -> out. activity = 1 -> in',
  `roomID` varchar(25) NOT NULL,
  PRIMARY KEY (`visitID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitor`
--

CREATE TABLE IF NOT EXISTS `visitor` (
  `visitorID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `identifierType` varchar(25) NOT NULL,
  PRIMARY KEY (`visitorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `visitor`
--

INSERT INTO `visitor` (`visitorID`, `name`, `identifierType`) VALUES
('12', '12', 'Kartu Pelajar'),
('123', '321', 'username'),
('1234', 'Listiarso Wastuargo', 'username');

-- --------------------------------------------------------

--
-- Struktur dari tabel `workingpermit`
--

CREATE TABLE IF NOT EXISTS `workingpermit` (
  `workingPermitID` varchar(25) NOT NULL,
  `visitorID` varchar(25) NOT NULL,
  `datefrom` date NOT NULL,
  `dateto` date NOT NULL,
  PRIMARY KEY (`workingPermitID`),
  KEY `workingpermit_ibfk_1` (`visitorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `approval`
--
ALTER TABLE `approval`
  ADD CONSTRAINT `approval_ibfk_1` FOREIGN KEY (`workingPermitID`) REFERENCES `workingpermit` (`workingPermitID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `approval_ibfk_2` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `approvalsheet`
--
ALTER TABLE `approvalsheet`
  ADD CONSTRAINT `approvalsheet_ibfk_1` FOREIGN KEY (`workingPermitID`) REFERENCES `workingpermit` (`workingPermitID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `approvalsheet_equipment`
--
ALTER TABLE `approvalsheet_equipment`
  ADD CONSTRAINT `approvalsheet_equipment_ibfk_1` FOREIGN KEY (`workingPermitID`) REFERENCES `workingpermit` (`workingPermitID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `approvalsheet_equipment_ibfk_2` FOREIGN KEY (`equipmentID`) REFERENCES `equipment` (`equipmentID`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `barrier`
--
ALTER TABLE `barrier`
  ADD CONSTRAINT `barrier_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `card_level`
--
ALTER TABLE `card_level`
  ADD CONSTRAINT `card_level_ibfk_2` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_level_ibfk_3` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`rackID`) REFERENCES `rack` (`rackID`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `level`
--
ALTER TABLE `level`
  ADD CONSTRAINT `level_ibfk_1` FOREIGN KEY (`buildingID`) REFERENCES `building` (`buildingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `rack`
--
ALTER TABLE `rack`
  ADD CONSTRAINT `rack_ibfk_1` FOREIGN KEY (`roomID`) REFERENCES `room` (`roomID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_page`
--
ALTER TABLE `role_page`
  ADD CONSTRAINT `role_page_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `workingpermit`
--
ALTER TABLE `workingpermit`
  ADD CONSTRAINT `workingpermit_ibfk_1` FOREIGN KEY (`visitorID`) REFERENCES `visitor` (`visitorID`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
