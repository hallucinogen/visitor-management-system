-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 04. Februari 2013 jam 23:21
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vms`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barrier`
--

CREATE TABLE IF NOT EXISTS `barrier` (
  `barrierID` varchar(25) NOT NULL,
  `levelID` varchar(25) DEFAULT NULL,
  `public` int(11) NOT NULL DEFAULT '0' COMMENT 'If public = 1 we will allow any card enter. Else, they should authenticate themselves',
  PRIMARY KEY (`barrierID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barrier`
--

INSERT INTO `barrier` (`barrierID`, `levelID`, `public`) VALUES
('BARSMG_001', 'SMG_001', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `buildingID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `building`
--

INSERT INTO `building` (`buildingID`, `name`, `location`) VALUES
('SMG', 'Semanggi', 'Semanggi dekat Plaza Semanggi'),
('SRG', 'Sadang Serang', 'Jalan Sadang Serang 34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `cardID` varchar(25) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`cardID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card`
--

INSERT INTO `card` (`cardID`, `location`) VALUES
('CRDSMG_0001', 'Somewhere');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card_barrier`
--

CREATE TABLE IF NOT EXISTS `card_barrier` (
  `cardID` varchar(25) NOT NULL,
  `barrierID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`barrierID`),
  KEY `barrierID` (`barrierID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card_barrier`
--

INSERT INTO `card_barrier` (`cardID`, `barrierID`) VALUES
('CRDSMG_0001', 'BARSMG_001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card_level`
--

CREATE TABLE IF NOT EXISTS `card_level` (
  `cardID` varchar(25) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`levelID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card_level`
--

INSERT INTO `card_level` (`cardID`, `levelID`) VALUES
('CRDSMG_0001', 'SMG_001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `card_user`
--

CREATE TABLE IF NOT EXISTS `card_user` (
  `cardID` varchar(25) NOT NULL,
  `userID` varchar(25) NOT NULL,
  KEY `cardID` (`cardID`,`userID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `card_user`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `levelID` varchar(25) NOT NULL,
  `buildingID` varchar(25) NOT NULL,
  `physicalLevel` int(11) NOT NULL,
  PRIMARY KEY (`levelID`),
  KEY `buildingID` (`buildingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`levelID`, `buildingID`, `physicalLevel`) VALUES
('SMG_001', 'SMG', 1),
('SMG_002', 'SMG', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rack`
--

CREATE TABLE IF NOT EXISTS `rack` (
  `rackID` varchar(25) NOT NULL,
  `roomID` varchar(25) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`rackID`),
  KEY `roomID` (`roomID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rack`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleID` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`roleID`, `name`) VALUES
('ROLCENTRAL00001', 'Administrator'),
('ROLCENTRAL00002', 'Manager'),
('ROLCENTRAL00003', 'Direksi'),
('ROLCENTRAL00004', 'Chief (Outlet Manager)'),
('ROLCENTRAL00005', 'Cashier');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_page`
--

CREATE TABLE IF NOT EXISTS `role_page` (
  `roleID` varchar(25) NOT NULL,
  `pageURL` varchar(100) NOT NULL,
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role_page`
--

INSERT INTO `role_page` (`roleID`, `pageURL`) VALUES
('ROLCENTRAL00001', '/visitor/home'),
('ROLCENTRAL00001', '/visitor/home?action=create'),
('ROLCENTRAL00001', '/administration/user'),
('ROLCENTRAL00001', '/administration/role'),
('ROLCENTRAL00001', '/administration/card'),
('ROLCENTRAL00001', '/administration/rack'),
('ROLCENTRAL00001', '/administration/room'),
('ROLCENTRAL00001', '/administration/barrier'),
('ROLCENTRAL00001', '/administration/level'),
('ROLCENTRAL00001', '/administration/building'),
('ROLCENTRAL00001', '/auth/barrier'),
('ROLCENTRAL00001', '/auth/level');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `roomID` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `levelID` varchar(25) NOT NULL,
  `power` double NOT NULL,
  PRIMARY KEY (`roomID`),
  KEY `levelID` (`levelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `room`
--

INSERT INTO `room` (`roomID`, `name`, `levelID`, `power`) VALUES
('ROMSMG_001', 'XL Data Center', 'SMG_001', 100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `roleID` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `roleID` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`username`, `name`, `roleID`, `password`) VALUES
('1234', 'Listiarso Wastuargo', 'ROLCENTRAL00001', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barrier`
--
ALTER TABLE `barrier`
  ADD CONSTRAINT `barrier_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `card_barrier`
--
ALTER TABLE `card_barrier`
  ADD CONSTRAINT `card_barrier_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_barrier_ibfk_2` FOREIGN KEY (`barrierID`) REFERENCES `barrier` (`barrierID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `card_level`
--
ALTER TABLE `card_level`
  ADD CONSTRAINT `card_level_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_level_ibfk_2` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `card_user`
--
ALTER TABLE `card_user`
  ADD CONSTRAINT `card_user_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `card_user_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `level`
--
ALTER TABLE `level`
  ADD CONSTRAINT `level_ibfk_1` FOREIGN KEY (`buildingID`) REFERENCES `building` (`buildingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `rack`
--
ALTER TABLE `rack`
  ADD CONSTRAINT `rack_ibfk_1` FOREIGN KEY (`roomID`) REFERENCES `room` (`roomID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_page`
--
ALTER TABLE `role_page`
  ADD CONSTRAINT `role_page_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `level` (`levelID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleID`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
